# NetSwitch PHP library

PHP classes library for L2 networks switches basic management. Use at your own risk.

## Requirements

* PHP 7.0 or higher
* GMP extension for PHP
* Net-SNMP and SPL SNMP

While using some options it'd be necessary to have access to system functions. For example to allow usage of exec() and system().
Also there're options to connect to perl wrappers of Net::Telnet for using CLI commands, so it'd be necessary to have Perl in the particular system setup.

## Supported models

* D-Link 
    * DES-3018
    * DES-3026
    * DES-3200-18 (A1/B1/C1)
    * DES-3200-26 (A1/B1)
    * DES-3200-28 (A1/B1/C1)
    * DES-3200-28F (A1/B1/C1)
    * DES-3200-52 (A1/B1/C1)
    * DES-3256
    * DES-3550
    * DGS-3120-24/SC (A1/B1)
* Zyxel
    * GS-2210-8
    * GS-2210-24
    * GS-2210-48
    * MES-3500-24F
    * MGS-3520-28F
    * MGS-3520-28
* Cisco
    * Catalyst 2960

## Supported methods

* [All information available in Wiki](https://bitbucket.org/dautkom/php.netswitch/wiki/)

## Warning and disclaimer

Library usage entails **great danger** - with it's help you can drop trunk VLAN, down transit port and enable STP on client's port. It is not intended to be foolproof - methods grant access to certain management features and provide no checks if invoker is going to shoot off his own leg. Use it with all possible care.

## License

Copyright (c) 2012-2016 Olegs Capligins and respective contributors under the [MIT License](http://opensource.org/licenses/MIT).
