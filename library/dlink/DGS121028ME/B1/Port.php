<?php

namespace dautkom\netswitch\library\dlink\DGS121028ME\B1;

use dautkom\netswitch\library\dlink\{
    MediumType, PortMeInheritor
};

/**
 * @package dautkom\netswitch\library\dlink\DGS121028ME\B1
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DGS-1210-28/ME B1
     * This trait modifies Core::addPortNumberToOID() method
     */
    use MediumType;

    /**
     * Trait utilizes getFromPort and setToPort methods
     */
    use PortMeInheritor;

    /**
     * Retrieve interface operating mode
     *
     * @return bool|int
     * @see    \dautkom\netswitch\library\Port::getLink()
     */
    public function getLink()
    {
        if (is_null(self::getPortNumber())) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original sysPortCtrlOperStatus return value list:
         * down (1)
         * rate1000M-Full (2)
         * rate100M-Full (3)
         * rate100M-Half (4)
         * rate10M-Full (5)
         * rate10M-Half (6)
         * rate10G-Full (7)
         */
        $link = $this->getFromPort("1.3.6.1.4.1.171.10.76.28.2.1.13.1.4");

        // Queried combo-port
        if (is_array($link)) {
            $mode = intval(array_sum($link));
        } // Queried regular port
        else {
            $mode = intval($link);
        }

        // Possible link status matrix
        $convert_link_status = [
            1 => '0', // no-link [down (1)]
            2 => '6', // full-1Gbps [rate1000M-Full (2)]
            3 => '4', // full-100Mbps [rate100M-Full (3)]
            4 => '3', // half-100Mbps [rate100M-Half (4)]
            5 => '2', // full-10Mbps [rate10M-Full (5)]
            6 => '1', // half-10Mbps [rate10M-Half (6)]
            7 => '8' // full-10Gbps [rate10G-Full (7)]
        ];

        return intval($convert_link_status[$mode]);
    }


    /**
     * Retrieves learning state ( enabled/disabled ) on current port
     * Return NULL if snmp response is empty or switch returned state other (1)
     *
     * @return int|null
     * @see PortInheritor::getFromPort()
     *
     */
    public function getLearningState()
    {
        $port = self::$port;
        $raw = $this->get(self::$device['snmp']['swL2PortControlLockState']);
        $bin = $this->parseHexMask($raw)['bin'];
        return ($bin[$port - 1] % 2) ? 1 : 2;
    }


    /**
     * Set learning state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {
        $mode = intval($mode);

        if ($mode < 1 || $mode > 2) {
            trigger_error('Wrong parameter specified for ' . __METHOD__ . '()', E_USER_WARNING);
            return false;
        }

        $raw = $this->get(self::$device['snmp']['swL2PortControlLockState']);
        $bin = $this->parseHexMask($raw)['bin'];

        $bin[self::$port - 1] = ($mode % 2) ? 1 : 0;
        $hex_mask = self::$_vlan->getHexMaskFromBin($bin);

        @$this->set(self::$device['snmp']['swL2PortControlLockState'], 'x', $hex_mask);

        /* In hope that static auto learning list was updated */
        return true;
    }


    /**
     * Clears all statistic counters on all ports
     *
     * @return bool|null
     */
    public function clearStats()
    {
        return $this->set(self::$device['snmp']['swL2DevCtrlCleanAllStatistic'], 'i', 1);
    }

}
