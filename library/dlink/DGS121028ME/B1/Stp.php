<?php

namespace dautkom\netswitch\library\dlink\DGS121028ME\B1;


use dautkom\netswitch\library\dlink\StpInheritor;

/**
 * @package namespace dautkom\netswitch\library\dlink\DGS121028ME\B1;
 */
class Stp extends \dautkom\netswitch\library\dlink\Stp
{
    use StpInheritor;
}
