<?php

namespace dautkom\netswitch\library\dlink\DGS121028ME\B1;

use dautkom\netswitch\library\dlink\FdbMeInheritor;

/**
 * @package dautkom\netswitch\library\dlink\DGS121028ME\B1
 */
class Fdb extends \dautkom\netswitch\library\Fdb
{

    /**
     * @var array
     */
    protected $fdb_filter = [
        '/SNMPv2-SMI::mib-2\.17\.7\.1\.3\.1\.1\.3\./',
        '/SNMPv2-SMI::enterprises\.171\.10\.76\.28\.2\.9\.3\.1\.4\./',
        "/Hex:/",
        "/Hex-/",
        "/x:/",
        "/x-/",
        "/ /",
        "/Hex-STRING:/"
    ];

    use FdbMeInheritor;

}
