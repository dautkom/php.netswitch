<?php

namespace dautkom\netswitch\library\dlink;


/**
 * Trait for library\dlink\Port, intended to be used for the sake of DRY
 * Contains private wrappers for retrieving and setting data from/to port
 *
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait PortInheritor
{

    /**
     * Get data from specific oid
     *
     * @param  $oid
     * @return int|null
     */
    protected function traitGetFromPort( $oid )
    {

        /** @noinspection PhpUndefinedMethodInspection */
        $snmp = parent::getFromPort( $oid );

        if( !empty( $snmp ) ) {

            // Combo-port
            if( $this->isCombo() ) {

                // Get rid of OID's in array keys
                $snmp = array_values( $snmp );

                // Unificate retuned values
                $snmp = $this->dlink_values[$snmp[0]][$snmp[1]];

                // If value equals 0 return null - error or unknown state
                return ( $snmp != 0 ) ? intval( $snmp ) : null;

            }

            // Regular port
            else {
                // Transformation to ifAdminStatus
                if( $snmp != 1 ) return ( $snmp % 2 ) ? 1 : 2;
            }

        }

        return null;

    }


    /**
     * Set data to specific oid with specific mode
     *
     * @param  $oid
     * @param  int $mode
     * @return bool
     */
    protected function traitSetToPort( $oid, $mode ): bool
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        // Transformation to swL2MgmtMIB
        $mode = ( $mode % 2 ) ? 3 : 2;

        // Method snmpSet() requires that oid count are equals to data count [3, 3]
        if( $this->isCombo() ) {
            $mode = ( $mode % 2 ) ? [3, 3] : [2, 2];
        }

        /** @noinspection PhpUndefinedMethodInspection */
        return parent::setToPort( $oid, 'i', $mode );

    }

}
