<?php

namespace dautkom\netswitch\library\dlink;
use dautkom\netswitch\library\{
    Telnet, TelnetException
};


/**
 * @package dautkom\netswitch\library\dlink
 */
abstract class Common extends \dautkom\netswitch\library\Common
{

    /**
     * Retrieve device serial number
     *
     * @return null|string
     */
    public function getSerialNumber()
    {
        return $this->get('.1.3.6.1.4.1.171.12.1.1.12.0');
    }


    /**
     * Save configuration via SNMP
     *
     * @return bool
     */
    protected function saveSnmp()
    {

        // waiting time in seconds
        $wait = 15;

        // Suppress error "No response from switch"
        @$this->set( self::$device['snmp']['agentSaveCfg'], 'i', self::$device['snmp']['agentSaveCfgValue'] );

        // Wait half of estimated time
        sleep( ceil($wait/2) );

        // Get comparator value from ini file, if not found set default value for majority of models
        $save_cfg_value = intval(self::$device['snmp']['agentStatusSaveCfgValue'] ?? 3);

        // Check status in 1 second interval in the second half of estimated switch saving time
        for( $i = 0; $i < floor($wait/2); $i++ ) {
            // Suppress error "No response from switch"
            if( @$this->get( self::$device['snmp']['agentStatusSaveCfg'] ) == $save_cfg_value ) {
                return true;
            }
            sleep(1);
        }

        // Return false if switch not saved
        return false;

    }


    /**
     * Save configuration via telnet using PHP wrapper
     *
     * @uses   Telnet
     * @return bool|null
     */
    protected function savePhpTelnet()
    {

        // Telnet class initialization
        $telnet = new Telnet();

        // Try connect to switch via Telnet throw Exception if can't connect
        try {
            $telnet->setTimeout(3)->connect( self::$ip );
            $telnet->login( self::$telnet[0], self::$telnet[1] );
        }
        catch ( TelnetException $e ) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return null;
        }

        // Send save command
        $telnet->waitfor('/#/', true)->send( "save" );

        return true;

    }

}
