<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait FdbInheritor
{

    /**
     * Create HEX mask for VLAN based on port number
     * DES-30xx require reverse binary mask
     *
     * @param  int $port
     * @return string
     */
    protected function getHexMaskFromPortNumber( int $port ): string
    {

        /** Retrieve data about device ports */
        $port      = intval( $port );
        $max_ports = count( self::$device['ports'] );

        // Return null if wrong port specified
        if( $port < 1 || $port > $max_ports ) {
            trigger_error( 'Wrong port specified', E_USER_WARNING );
            return '';
        }

        // Form binary mask
        $binary = '';
        for ( $i = 0; $i < $max_ports; $i++ ) {
            $binary.= ( $i == ( $port - 1 ) ) ? 1 : 0;
        }

        $binary = strrev( str_pad( $binary, 32, '0', STR_PAD_RIGHT ) );
        $binary = str_split( $binary, 4 );

        // Form hex mask
        $result = '';
        array_walk( $binary, function( $octet ) use ( &$result ) {
            $octet   = str_pad( $octet, 4, '0', STR_PAD_RIGHT );
            $result .= dechex( bindec( $octet ) );
        });

        /** Return port hex mask */
        return str_pad( $result, self::$device['vlan']['outHexLength'], '0', STR_PAD_RIGHT );

    }


    /**
     * Add static (permanent) MAC address to FDB table on current port
     * DES-30xx are freezing after adding MAC address and it's necessary to sleep() for a while.
     *
     * @param  string $mac MAC-address
     * @param  int    $vid VLAN ID
     * @return bool
     */
    public function addPermanent( string $mac, int $vid ): bool
    {

        /** Filter and check data */
        $port = self::$port;
        $vid  = ( self::$_vlan->validateVID( $vid ) ) ? $vid : null;
        $mac  = $this->filterMac( $mac );

        if( $port === false || is_null( $vid ) ) {
            trigger_error( 'Wrong data specified', E_USER_WARNING );
            return false;
        }

        /** Retrieve FDB table and check if mac already exists in fdb table */
        $fdb = $this->showAllPermanent();
        foreach( $fdb as $checked_port => $entries ) {
            foreach( $entries as $entry ) {
                if ( in_array( $mac, $entry ) ) {
                    trigger_error( 'Duplicated FDB entry - MAC already exists on port ' . $checked_port, E_USER_WARNING );
                    return false;
                }
            }
        }

        /** Convert data */
        $port_mask = $this->getHexMaskFromPortNumber( $port );
        $mac       = $this->convertMacHexDec( $mac );

        /** Check Vlan on port                                                      */
        $vlans     = self::$_vlan->getAll();
        if( empty( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Vlan $vid not exist on this switch or error occurred!", E_USER_WARNING );
        }

        if( $vlans[$vid]['egress']['bin']{ $port-1 } != 1 &&  $vlans[$vid]['untagged']['bin']{ $port-1 } != 1 ) {
            trigger_error("Port must be in $vid VLAN to add permanent MAC-address in it", E_USER_WARNING);
            return false;
        }

        $prepare_oid = self::$device['snmp']['dot1qStaticUnicastAllowedToGoTo'] . '.' . $vid . '.' . $mac;
        $writing_oid = self::$device['snmp']['dot1qStaticUnicastStatus'] . '.' . $vid . '.' . $mac;

        /** Write MAC and suppress not writable warning */
        $this->set($prepare_oid, 'x', $port_mask);

        /** Write status */
        $snmp = $this->set($writing_oid, 'i', 3);

        sleep(2);

        return ( $snmp === false ) ? false : true;

    }

}
