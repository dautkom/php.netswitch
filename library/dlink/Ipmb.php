<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @package dautkom\netswitch\library\dlink
 */
class Ipmb extends \dautkom\netswitch\library\Ipmb
{

    /**
     * Retrieve port IP-MAC binding state.
     *
     * @return bool|null
     */
    public function getState()
    {

        /*
         * Retrieve state of IP-MAC binding on current port
         *
         * Return values:
         * 1: other
         * 2: enabled
         * 3: disabled
         */
        $state = $this->getFromPort( self::$device['snmp']['swL2IpMacBindingPortState'] );

        return ($state == 2) ? true : false;

    }

}
