<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @package dautkom\netswitch\library\dlink
 */
class Port extends \dautkom\netswitch\library\Port
{

    /**
     * Trait utilizes getFromPort and setToPort methods
     * @see \dautkom\netswitch\library\dlink\Port::getFlowControlState()
     * @see PortInheritor::getFromPort()
     */
    use PortInheritor;

    /**
     * Unificate return values from d'link
     *
     * @var array
     */
    protected $dlink_values = [
        1 => [
            1 => '0',
            2 => '0',
            3 => '0',
        ],
        2 => [
            1 => '0',
            2 => '2',
            3 => '2',
        ],
        3 => [
            1 => '0',
            2 => '2',
            3 => '1',
        ],
    ];


    /**
     * Retrieve port medium type
     * Returns NULL if:
     *  1) port was not set,
     *  2) switch is not supporting medium type,
     *  3) combo port status is link-fail (3),
     *  4) trying to check medium type on combo port via compile() method.
     *
     * This method is retrieving medium type from ini file if port is not combo.
     * If switch is connected, method is retrieving active medium type via SNMP only on combo port.
     * Method is not retrieving medium type on combo port from ini file.
     *
     * Value list:
     *  0 : copper
     *  1 : fiber
     *
     * @param  int|null $port Port number, optional. Uses self::$port value if no argument is used
     * @return int|null
     */
    public function getMediumType( $port = null)
    {

        if( is_null($port) ) {
            $port = self::$port;
        }

        if( !$this->validatePortNumber( $port ) ) {
            trigger_error( 'Wrong port specified for ' . __METHOD__ . '()', E_USER_WARNING );
            return null;
        }

        /**
         * @var array Medium type value unification
         */
        $medium_convert = array( 1 => '0', 100 => '0', 2 => '1', 101 => '1' );

        // Get data from ini file for non-combo ports
        if( !$this->isCombo( $port ) ) {
            $medium_type = self::$device['ports'][$port][0];
            return ( !empty( $medium_type ) ) ? intval( $medium_convert[$medium_type] ) : null;
        }

        /**
         * Try to retrieve active medium type. Only if connected and if port is combo
         *
         * If both mediums are not connected, returns null.
         * Wrap $obj->Port->getMediumType() with if(isCombo()) check
         */
        if( self::$connected && $this->isCombo() ) {

            $save = $this->getPortNumber();  // Save previous port number
            $this->setPortNumber($port);     // Set temporary new port

            // Get data
            $snmp = $this->getFromPort( self::$device['snmp']['swL2PortInformationLinkStatus'] );

            // Restore port number property
            if ( $this->validatePortNumber($save) ) {
                $this->setPortNumber($save);
            }

            foreach( $snmp as $medium => $link ){

                // Get medium type from oid
                preg_match('/([^.]*)$/', $medium, $matches);

                // If link is up
                if( $link == 2 && array_key_exists(0, $matches) ) {
                    return intval( $medium_convert[$matches[0]] );
                }
            }

        }

        return null;

    }


    /**
     * Retrieve port admin state
     * Return 0 if snmp response is empty or switch returned state other (1)
     *
     * @see PortInheritor::getFromPort()
     *
     * Value list:
     *  0 : error
     *  1 : enabled
     *  2 : disabled
     *
     * @return int
     */
    public function getAdminState(): int
    {
        $res = $this->traitGetFromPort( self::$device['snmp']['swL2PortControlAdminState'] );
        return intval($res);
    }


    /**
     * Changes port admin state ( enabled/disabled )
     *
     * @see PortInheritor::setToPort()
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool
     */
    public function setAdminState( int $mode ): bool
    {
        return $this->traitSetToPort( self::$device['snmp']['swL2PortControlAdminState'], $mode );
    }


    /**
     * Retrieves flow control state on current port
     * Return NULL if snmp response is empty or switch returned state other (1)
     *
     * @see PortInheritor::getFromPort()
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getFlowControlState()
    {
        return $this->traitGetFromPort( self::$device['snmp']['swL2PortControlFlowCtrlState'] );
    }


    /**
     * Changes flow control state on current port
     *
     * @see PortInheritor::setToPort()
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setFlowControlState(int $mode)
    {
        return $this->traitSetToPort( self::$device['snmp']['swL2PortControlFlowCtrlState'], $mode );
    }


    /**
     * Retrieves learning state ( enabled/disabled ) on current port
     * Return NULL if snmp response is empty or switch returned state other (1)
     *
     * @see PortInheritor::getFromPort()
     *
     * @return int|null
     */
    public function getLearningState()
    {
        return $this->traitGetFromPort( self::$device['snmp']['swL2PortControlLockState'] );
    }


    /**
     * Set learning state on current port
     *
     * @see PortInheritor::setToPort()
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {
        return $this->traitSetToPort( self::$device['snmp']['swL2PortControlLockState'], $mode );
    }


    /**
     * Clears all statistic counters on all ports
     *
     * @return bool|null
     */
    public function clearStats()
    {
        return $this->set( self::$device['snmp']['swL2DevCtrlCleanAllStatistic'], 'i', 2 );
    }


    /**
     * @param  string $raw
     * @return array
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    protected function parseHexMask($raw)
    {
        $hex = self::$_vlan->getHexMaskFromRaw($raw);

        return [
            'raw' => $raw,
            'hex' => $hex,
            'bin' => self::$_vlan->getBinMaskFromHex($hex),
        ];
    }

}
