<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait IpmbMeInheritor
{

    /**
     * Emulates swIpMacBindingPortState retrieving.
     *
     * In dlink-common-mgmt swIpMacBindingPortState returns an empty value and IP-MAC binding entry
     * consists of two settings: ARP inspection and IP inspection. We check both and aggregate'em
     * into one unified value.
     *
     * @return bool
     */
    public function getState()
    {

        $arp_state = $this->getArpInspectionState();
        $ip_state  = $this->getIpInspectionState();

        return $ip_state === true || $arp_state === true;

    }

    /**
     * Retrieve IP inspection state
     *
     * Return values:
     * 0: disabled
     * 1: enabled
     *
     * @return int
     */
    private function getIpInspectionState()
    {

        $state = $this->getFromPort(self::$device['snmp']['impbPortIpInspectionState']);
        $state = intval($state);

        return $state == 1;

    }

    /**
     * Retrieve ARP inspection mode
     *
     * Return values:
     * 0: disabled
     * 1: strict
     * 2: loose
     *
     * @return int
     */
    private function getArpInspectionState()
    {

        $state = $this->getFromPort(self::$device['snmp']['impbPortArpInspectionState']);
        $state = intval($state);

        return $state == 1 || $state == 2;

    }

}
