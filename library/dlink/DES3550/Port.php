<?php

namespace dautkom\netswitch\library\dlink\DES3550;
use dautkom\netswitch\library\dlink\MediumType;


/**
 * @package dautkom\netswitch\library\dlink\DES3550
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DES-3550
     * This trait modifies Core::addPortNumberToOID() method
     */
    use MediumType;


    /**
     * No duplex mode could be retrieved via SNMP on DES-3550.
     * OID doesn't exist for this device.
     *
     * @see    Port::getDuplex()
     * @return null
     */
    public function getDuplex()
    {
        return $this->notImplemented();
    }


    /**
     * Retrieve interface operating mode
     *
     * @see    dautkom\netswitch\library\Port::getLink()
     * @return bool|int
     */
    public function getLink()
    {

        if( is_null(self::getPortNumber()) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         *  other(1),
         *  auto(2),
         *  half-10Mbps(3),
         *  full-10Mbps(4),
         *  half-100Mbps(5),
         *  full-100Mbps(6),
         *  half-1Gbps(7),
         *  full-1Gbps(8)
         */
        $link = $this->getFromPort(".1.3.6.1.4.1.171.11.64.2.2.4.4.1.6");

        // Combo port workaround
        if(is_array($link)) {
            $mode = intval(array_sum($link)) - 2;
            $mode = ($mode < 2) ? 2 : $mode;
        }
        else {
            $mode = intval($link);
        }

        if($mode > 8) $mode = 2;
        $mode = $mode - 2;

        // Unification
        return ($mode < 0) ? 0 : $mode;

    }

    /**
     * Set data to specific oid with specific mode
     * It is impossible to set learning state on Fiber port, learning state can be set only on
     * copper port. That's why setting learning state on combo port is forbidden.
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return null;
        }

        // Transformation to swL2MgmtMIB
        $mode = ( $mode % 2 ) ? 3 : 2;

        // Forbid set learning state on combo port
        if( $this->isCombo() ) {
            trigger_error( 'Learning state can not be set on combo ports', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['swL2PortControlLockState'], 'i', $mode );

    }


    /**
     * Clears all statistic counters on all ports
     * Switch SNMP engine freezes after clearing statistic thats why error suppressing is needed
     *
     * @return bool|null
     */
    public function clearStats()
    {

        // Suppress error "No response from switch"
        @$this->set( self::$device['snmp']['swL2DevCtrlCleanAllStatistic'], 'i', 2 );

        // Switch start correctly reponse only after 5 seconds
        sleep(5);

        // In hope that statistic was cleared
        return true;

    }

}
