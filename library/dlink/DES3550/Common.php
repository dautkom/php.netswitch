<?php

namespace dautkom\netswitch\library\dlink\DES3550;
use dautkom\netswitch\library\{
    Fdb, 
    Qos, 
    Vlan,
    dlink\CableDiagnostics, 
    dlink\Ipmb, 
    dlink\PortSecurity, 
    dlink\Stp
};


/**
 * @package dautkom\netswitch\library\dlink\DES3550
 */
class Common extends \dautkom\netswitch\library\dlink\Common
{

    /**
     * Changes default save method to save via Telnet.
     * Saving DES-3550 conf via SNMP causes its' SNMP engine's freeze for 3+ minutes.
     *
     * ---------------------------------------------------------------
     * It's strictly recommended to save 3550 configuration via Telnet
     * ---------------------------------------------------------------
     *
     * But switch also can be saved via SNMP, for those who love hardcore.
     * @var string
     */
    protected $mode = 'telnet';

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }


    /**
     * No serial number could be retrieved via SNMP on DES-3550
     *
     * @return null
     */
    public function getSerialNumber()
    {
        return $this->notImplemented();
    }

}
