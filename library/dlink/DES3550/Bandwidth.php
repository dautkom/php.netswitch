<?php

namespace dautkom\netswitch\library\dlink\DES3550;


/**
 * @package dautkom\netswitch\library\dlink\DES3550
 */
class Bandwidth extends \dautkom\netswitch\library\Bandwidth
{


    /**
     * Set Rx bandwidth limit.
     * Argument must be in kbps.
     *
     * @param  int $value speed limit value
     * @return bool
     */
    public function setRxRate(int $value): bool
    {

        $this->setRate( self::$device['snmp']['BandwidthRxRate'], $value, 'Rx' );

        // The lowest possible wait is 5 seconds
        sleep(5);

        // Get Rx from switch
        $result = $this->getRate( self::$device['snmp']['BandwidthRxRate'] );

        // Perform check for result and return appropriate bool
        return ( round( $result / 1024, 0 ) == round( $value / 1024, 0 ) ) ? true : false;

    }


    /**
     * Set Tx bandwidth limit.
     * Argument must be in kbps.
     *
     * @param  int $value speed limit value
     * @return bool
     */
    public function setTxRate(int $value): bool
    {

        $this->setRate( self::$device['snmp']['BandwidthTxRate'], $value, 'Tx' );

        // The lowest possible wait is 5 seconds
        sleep(5);

        // Get Tx from switch
        $result = $this->getRate( self::$device['snmp']['BandwidthTxRate'] );

        // Perform check for result and return appropriate bool
        return ( round( $result / 1024, 0 ) == round( $value / 1024, 0 ) ) ? true : false;

    }


    /**
     * Get data from specific oid
     *
     * @param  string   $oid   SNMP object identifier
     * @param  int|null $state egress state for zyxels, left for inheritance compatibility
     * @return int
     */
    protected function getRate(string $oid, $state=null): int
    {
        $snmp = $this->getFromPort( $oid );
        return intval( $snmp * 1024 );

    }


    /**
     * Set data to specific oid with specific mode
     *
     * @param  string $oid
     * @param  int    $value
     * @param  string $type
     * @return bool
     */
    protected function setRate( string $oid, int $value, string $type = ''): bool
    {

        $value = abs( intval( $value ) );

        // Check if port is combo
        $isCombo = (new Port)->isCombo();

        // Set max bandwidth if value equals 0
        $value = ( $value === 0 ) ?  1000 : round( $value / 1024, 0 );

        // On Gigabit ports value must be multiple of 8. Possible values: 8, 16, 24, 32, 40, etc.
        if( $isCombo && $value % 8 != 0 ) {
            trigger_error( 'Gigabit port rate limit must be multiple of 8192 Kbits/s( 8 Mbits/sec )', E_USER_WARNING );
            return false;
        }

        if( $value < 1 ) {
            trigger_error( $type . ' must be >1024 kbit/s for DES3550', E_USER_WARNING );
            return false;
        }

        // DES-3550 stops responding after setting Rx/TX rate via SNMP
        // So it's necessary to suppress errors...
        // Error must be suppressed only in snmpSetToPort method, without suppressing incorrect input warnings
        // that's why error suppressing is here but not in setRxRate or setTxRate methods.
        return @$this->setToPort( $oid, 'i', $value );

    }

}
