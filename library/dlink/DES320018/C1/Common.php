<?php

namespace dautkom\netswitch\library\dlink\DES320018\C1;
use dautkom\netswitch\library\{
    Bandwidth,
    Fdb,
    Qos,
    Vlan,
    dlink\CableDiagnostics,
    dlink\SaveInheritor,
    dlink\Stp
};


/**
 * @package netswitch\library\dlink\DES320018\C1
 */
class Common extends \dautkom\netswitch\library\dlink\DES320018\Common
{

    /**
     * Trait utilizes saveSnmp() method
     *
     * @see SaveInheritor::saveSnmp()
     */
    use SaveInheritor;


    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }

}
