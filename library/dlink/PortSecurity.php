<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @package dautkom\netswitch\library\dlink
 */
class PortSecurity extends \dautkom\netswitch\library\PortSecurity
{

    /**
     * Retrieve port security admin state.
     * Return NULL if snmp response is empty or switch returned state other (1)
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getState()
    {

        //Get data
        $snmp = $this->getFromPort( self::$device['snmp']['swL2PortSecurityAdmState'] );

        /**
         * @var array State value unification
         */
        $state_convert = [1 => '0', 2 => '1', 3 => '2'];

        return ( !empty($snmp) && $snmp != 1 ) ? intval( $state_convert[$snmp] ) : null;

    }


    /**
     * Changes port security admin state.
     *
     * Mode argument values:
     * 1 : enabled
     * 2 : disabled
     *
     * @param  int $mode
     * @return bool
     */
    public function setState(int $mode): bool
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        /**
         * @var array State value unification
         */
        $mode_convert = [1 => '2', 2 => '3'];

        return $this->setToPort( self::$device['snmp']['swL2PortSecurityAdmState'], 'i', $mode_convert[$mode] );

    }


    /**
     * Retrieves port security mode.
     *
     * Return values:
     *  0 : error
     *  1 : other
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @return int|null
     */
    public function getMode()
    {
        //Get Data
        $snmp = $this->getFromPort(self::$device['snmp']['swL2PortSecurityMode']);
        return ( $snmp !== false ) ? intval( $snmp ) : null;
    }


    /**
     * Changes port security mode.
     * Return null if wrong parameter specified
     *
     * Mode value list:
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setMode( $mode )
    {

        $mode = intval($mode);

        if( $mode < 2 || $mode > 4 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return null;
        }

        return $this->setToPort( self::$device['snmp']['swL2PortSecurityMode'], 'i', $mode );

    }

}
