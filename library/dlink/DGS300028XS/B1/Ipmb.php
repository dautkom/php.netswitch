<?php

namespace dautkom\netswitch\library\dlink\DGS300028XS\B1;
use dautkom\netswitch\library\dlink\IpmbInheritor;


/**
 * @package dautkom\netswitch\library\dlink\DGS300028XS\B1
 */
class Ipmb extends \dautkom\netswitch\library\dlink\Ipmb
{
    use IpmbInheritor;
}
