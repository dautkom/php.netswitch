<?php

namespace dautkom\netswitch\library\dlink\DGS300028XS\B1;

use dautkom\netswitch\library\dlink\{
    MediumType, PortInheritor
};


/**
 * @package dautkom\netswitch\library\dlink\DGS300028XS\B1
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DGS-312024SC
     * This trait modifies @see \dautkom\netswitch\library\Port::addPortNumberToOID() method
     */
    use MediumType;


    /**
     * Trait utilizes traitGetFromPort and traitSetToPort methods
     */
    use PortInheritor;


    /**
     * Retrieve interface operating mode
     *
     * @see    \dautkom\netswitch\library\Port::getLink()
     * @return bool|int
     */
    public function getLink()
    {

        if( is_null( self::getPortNumber() ) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         * other (0) ok
         * empty (1) ok
         * link-down (2) ok
         * half-10Mbps (3) ok
         * full-10Mbps (4) ok
         * half-100Mbps (5) ok
         * full-100Mbps (6) ok
         * half-1Gigabps (7) ok
         * full-1Gigabps (8)
         * full-10Gigabps (9)
         * empty (17) ok
         * err-disabled (18) ok
         */
        $link = $this->getFromPort( ".1.3.6.1.4.1.171.10.133.12.2.2.3.1.1.6" );

        // Queried combo-port
        if( is_array( $link ) ) {
            $mode = intval( array_sum( $link ) );
        }

        // Queried regular port
        else {
            $mode = intval($link);
        }

        // Possible link status matrix
        $convert_link_status = [
            3 => '1',  # half-10Mbps
            4 => '2',  # full-10Mbps
            5 => '3',  # half-100Mbps
            6 => '4',  # full-100Mbps
            7 => '5',  # half-1Gbps
            8 => '6',  # full-1Gbps
            9 => '8',  # full-10Gbps
            0 => '0', 1 => '0',             # no-link or empty or err-disabled
            2 => '0', 17 => '0',  18 => '0' # no-link or empty or err-disabled
        ];

        return intval( $convert_link_status[$mode] );

    }


    /**
     * Set learning state on current port
     *
     * @see PortInheritor::traitSetToPort()
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {

        // Get data
        $mode       = intval( $mode );
        $portsec    = (new PortSecurity())->getState();
        $learnstate = (new Port())->getLearningState();

        // Check port security state
        if( $mode == 2 && $portsec == 1 ) {
            trigger_error( "Learning must be enabled when Port Security is enabled", E_USER_WARNING );
            return false;
        }

        // Check if learning is already enabled, return true if enabled, if don't do it
        // snmpSetToPort will trigger error "SNMP::set(): Error in packet at 0 object_id: undoFailed" and return false,
        // error is triggered only when port security is enabled otherwise error is no triggered.
        if( $learnstate == 1 && $mode == 1 && $portsec == 1 ) {
            return true;
        }

        return $this->traitSetToPort( self::$device['snmp']['swL2PortControlLockState'], $mode );

    }

}
