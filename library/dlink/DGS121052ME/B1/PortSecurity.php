<?php

namespace dautkom\netswitch\library\dlink\DGS121052ME\B1;


/**
 * @package dautkom\netswitch\library\dlink\DGS121052ME
 */
class PortSecurity extends \dautkom\netswitch\library\dlink\PortSecurity
{

    /**
     * Retrieve port security admin state.
     * Return NULL if snmp response is empty
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getState()
    {
        $snmp = $this->getFromPort(self::$device['snmp']['swL2PortSecurityAdmState']);
        return (!empty($snmp)) ? intval($snmp) : null;
    }


    /**
     * Changes port security admin state.
     *
     * Mode argument values:
     * 1 : enabled
     * 2 : disabled
     *
     * @param int $mode
     * @return bool
     */
    public function setState(int $mode): bool
    {
        $mode = intval($mode);
        $learn_state = (new Port())->getLearningState();

        if ($learn_state == 2 && $mode == 1) {
            trigger_error("Learning must be enabled to enable Port Security", E_USER_WARNING);
            return false;
        }

        if ($mode < 1 || $mode > 2) {
            trigger_error('Wrong parameter specified for ' . __METHOD__ . '()', E_USER_WARNING);
            return false;
        }

        return $this->setToPort(self::$device['snmp']['swL2PortSecurityAdmState'], 'i', $mode);
    }


    /**
     * Retrieves port security mode.
     *
     * Return values:
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @return int|null
     */
    public function getMode()
    {
        $snmp = $this->getFromPort(self::$device['snmp']['swL2PortSecurityMode']);

        /**
         * Original swPortSecPortLockAddrMode return value list:
         * permanent(1)
         * deleteOnTimeout(2)
         * deleteOnReset(3)
         */

        /**
         * @var array State value unification
         */
        $mode_convert = [1 => '4', 2 => '3', 3 => '2'];

        return ($snmp !== false) ? intval($mode_convert[$snmp]) : null;
    }


    /**
     * Changes port security mode.
     * Return null if wrong parameter specified
     *
     * Mode value list:
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @param int $mode
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function setMode($mode)
    {
        $mode = intval($mode);

        if ($mode < 2 || $mode > 4) {
            trigger_error('Wrong parameter specified for ' . __METHOD__ . '()', E_USER_WARNING);
            return null;
        }

        /**
         * @var array State value unification
         */
        $mode_convert = [4 => '1', 3 => '2', 2 => '3'];

        return $this->setToPort(self::$device['snmp']['swL2PortSecurityMode'], 'i', $mode_convert[$mode]);
    }

}
