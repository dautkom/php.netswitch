<?php

namespace dautkom\netswitch\library\dlink;


use RuntimeException;

/**
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait StpInheritor
{

    /**
     * Retrieve switch instance stp priority
     *
     * @return int
     */
    public function getPriority()
    {
        $priority = $this->get(self::$device['snmp']['stpBridgePriority']);
        return intval($priority);
    }


    /**
     * Set priority of instance.
     * The priority must be dividable by 4096
     *
     * @param int $priority
     * @return bool|null
     * @throws RuntimeException
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function setPriority($priority)
    {

        if ($priority % 4096 != 0) {
            throw new RuntimeException("Wrong priority value! Must be dividable by 4096");
        }

        if ($this->getPriority() == $priority) {
            return true;
        }

        return $this->set(self::$device['snmp']['stpBridgePriority'], 'i', $priority);

    }


    /**
     * The port number of the port which offers the lowest cost path
     * from this bridge to the root bridge.
     *
     * @return int
     */
    public function getRootPort()
    {

        $this->checkStpStatus();
        return intval($this->get(self::$device['snmp']['stpRootPort']));

    }


    /**
     * Retrieve switch stp status
     *
     * Value List
     * enabled (1)
     * disabled (2)
     *
     * @return int
     */
    public function getStpStatus()
    {
        return intval($this->get(self::$device['snmp']['stpModuleStatus']));
    }


    /**
     * Retrieve root switch mac-address
     *
     * @return string|null
     */
    public function getRootMac()
    {
        $this->checkStpStatus();

        $rootPort = $this->getRootPort();

        if ($rootPort == 0) {
            return null;
        }

        // Returns octet string 4 symbols - hex priority, 12 symbols - mac
        // Example: 7000ffffffd8fffffffeffffffe3ffffff9233ffffff80
        $rootMac = $this->get(self::$device['snmp']['stpRootBridge']);

        return $this->filterMac($rootMac);

    }


    /**
     * Retrieve designated bridge mac
     *
     * @return string|null
     */
    public function getDesignatedBridgeMac()
    {
        $this->checkStpStatus();

        $rootPort = $this->getRootPort();

        if ($rootPort == 0) {
            return null;
        }

        // Returns octet string 4 symbols - hex priority, 12 symbols - mac
        // Example: 7000ffffffd8fffffffeffffffe3ffffff9233ffffff80
        $rootBridgeMac = $this->get(self::$device['snmp']['mstCistPortDesignatedBridge'] . ".$rootPort");
        $rootBridgeMac = $this->filterMac($rootBridgeMac);

        return ($this->getRootMac() == $rootBridgeMac) ? null : $rootBridgeMac;
    }


    /**
     * Retrieve switch mac
     *
     * @return string
     * @throws RuntimeException
     */
    public function getSwitchMac()
    {

        $switchMac = $this->get('.1.3.6.1.2.1.17.1.1.0');

        $switchMac = strtolower(preg_replace($this->fdb_filter, '', $switchMac));

        if (strlen($switchMac) != 12) {
            throw new RuntimeException("Wrong MAC address specified.");
        }

        return $switchMac;

    }


    /**
     * Retrieve stp status on current port
     *
     * Value List:
     * enabled (1)
     * disabled (2)
     *
     * @return int
     */
    public function getStateOnPort()
    {
        $status = intval($this->getFromPort(self::$device['snmp']['stpPortStatus']));
        return ($status % 2) ? 1 : 2;
    }


    /**
     * Set stp status (enabled/disabled) on current port
     *
     * Value List:
     * enabled (1)
     * disabled (2)
     *
     * @param $set
     * @return bool|null
     *
     * @throws RuntimeException
     */
    public function setStateOnPort($set)
    {

        if ($set != 1 && $set != 2) {
            throw new RuntimeException("Wrong stp state value.");
        }

        if ($this->getStateOnPort() == $set) {
            return true;
        }

        return ($this->setToPort(self::$device['snmp']['stpPortStatus'], 'i', ($set % 2) ? 1 : 0));

    }

}
