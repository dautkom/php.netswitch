<?php

namespace dautkom\netswitch\library\dlink;


/**
 * Trait for devices with `medium type` in OIDs
 * 
 * @traitUses NetSwitch
 * @package   dautkom\netswitch\library\dlink
 */
trait MediumType
{

    /**
     * List of invokers who should ignore medium type adding
     * @var array
     */
    private $skip = array(
        'getFromPort', 'setToPort', 'getDescription', 'setDescription', 'getDataFlow', 'getErrors', 'getOperatingStatus', 'getOperatingSpeed', 'getDuplex'
    );


    /**
     * Adds to OID port number AND medium type.
     * Does it with looking over to exclusions.
     *
     * @see    Core::snmpGetFromPort()
     * @param  string|array $oid
     * @return array
     */
    protected function addPortNumberToOID($oid)
    {

        /** @noinspection PhpUndefinedMethodInspection */
        $oid     = parent::addPortNumberToOID($oid);
        $invoker = debug_backtrace();
        $invoker = $invoker[2]['function'];

        // If medium type should not be added
        if( in_array($invoker, $this->skip) ) {
            return $oid;
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $ports = parent::getIni();
        $ports = $ports['ports'];
        $query = array();

        if( !is_array($oid) ) {
            $oid = array($oid);
        }

        // Add medium type to each OID
        array_map(

            /**
             * @var string $value   given array element
             * @use array  $ports   device ports data
             * @use array  $query   resultset with added medium type
             */
            function ($value) use ($ports, &$query) {

                for ($i = 0; $i < count($ports[self::$port]); $i++) {
                    $query[] = $value . '.' . $ports[self::$port][$i];
                }

            }, $oid
        );

        return $query;

    }

}
