<?php

namespace dautkom\netswitch\library\dlink\DES320028F\C1;
use dautkom\netswitch\library\dlink\{
    MediumType, PortInheritor
};


/**
 * @package dautkom\netswitch\library\dlink\DES320028F\C1
 */
class Port extends \dautkom\netswitch\library\dlink\DES320028F\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DES-320028F C1
     * This trait modifies @see dautkom\netswitch\library\Port::addPortNumberToOID() method
     */
    use MediumType;


    /**
     * Trait utilizes traitGetFromPort and traitSetToPort methods
     */
    use PortInheritor;


    /**
     * Retrieve interface operating mode
     *
     * @see    dautkom\netswitch\library\Port::getLink()
     * @return bool|int
     */
    public function getLink()
    {

        if( is_null( self::getPortNumber() ) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         * other(0)
         * empty(1)
         * link-down(2)
         * half-10Mbps(3)
         * full-10Mbps(4)
         * half-100Mbps(5)
         * full-100Mbps(6)
         * half-1Gigabps(7)
         * full-1Gigabps(8)
         * full-10Gigabps(9)
         */
        $link = $this->getFromPort( ".1.3.6.1.4.1.171.11.113.6.1.2.3.1.1.6" );

        // Combo port workaround
        if(is_array($link)) {
            $mode = intval(array_sum($link)) - 2;
            $mode = ($mode < 2) ? 2 : $mode;
        }
        else {
            $mode = intval($link);
        }

        if($mode > 8) $mode = 2;
        $mode = $mode - 2;

        // Unification
        return ($mode < 0) ? 0 : $mode;

    }


    /**
     * Set learning state on current port
     *
     * @see PortInheritor::setToPort()
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {

        // Get data
        $mode       = intval( $mode );
        $portsec    = (new PortSecurity())->getState();
        $learnstate = (new Port())->getLearningState();

        // Check port security state
        if( $mode == 2 && $portsec == 1 ) {
            trigger_error( "Learning must be enabled when Port Security is enabled", E_USER_WARNING );
            return false;
        }

        // Check if learning is already enabled, return true if enabled, if don't do it
        // snmpSetToPort will trigger error "SNMP::set(): Error in packet at 0 object_id: undoFailed" and return false,
        // error is triggered only when port security is enabled otherwise error is no triggered.
        if( $learnstate == 1 && $mode == 1 && $portsec == 1 ) {
            return true;
        }

        return $this->traitSetToPort( self::$device['snmp']['swL2PortControlLockState'], $mode );

    }

}
