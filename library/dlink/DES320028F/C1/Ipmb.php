<?php

namespace dautkom\netswitch\library\dlink\DES320028F\C1;
use dautkom\netswitch\library\dlink\IpmbInheritor;


/**
 * @package dautkom\netswitch\library\dlink\DES320028F\C1
 */
class Ipmb extends \dautkom\netswitch\library\dlink\Ipmb
{
    use IpmbInheritor;
}
