<?php

namespace dautkom\netswitch\library\dlink\DES320028F;
use dautkom\netswitch\library\dlink\MediumType;


/**
 * @package dautkom\netswitch\library\dlink\DES320028F
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DES-320028F
     * This trait modifies Core::addPortNumberToOID() method
     */
    use MediumType;


    /**
     * Retrieve interface operating mode
     *
     * @see    dautkom\netswitch\library\Port::getLink()
     * @return bool|int
     */
    public function getLink()
    {

        if( is_null( self::getPortNumber() ) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         * auto(1)
         * half-10Mbps(2)
         * full-10Mbps(3)
         * half-100Mbps(4)
         * full-100Mbps(5)
         * full-1Gigabps(7)
         */

        // Default value
        $mode = '';

        // For SFP ports call original getLink() method
        if( self::$port < 25 ) {
            $mode = parent::getLink();
        }

        // For combo ports the standard request is sent
        else {

            // Query
            $link = $this->getFromPort(".1.3.6.1.4.1.171.11.113.1.4.2.2.1.1.5");

            // Combo port workaround
            if (is_array($link)) {
                $mode = intval(array_sum($link)) - 2;
            }
        }

        return ( $mode < 0 ) ? 0 : intval( $mode );

    }

}
