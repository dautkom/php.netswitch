<?php

namespace dautkom\netswitch\library\dlink\DGS121020ME\B1;

use dautkom\netswitch\library\dlink\VlanInheritor;

/**
 * @package dautkom\netswitch\library\dlink\DGS121020ME\B1
 */
class Vlan extends \dautkom\netswitch\library\Vlan
{
    use VlanInheritor;
}
