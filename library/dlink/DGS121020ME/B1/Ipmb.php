<?php

namespace dautkom\netswitch\library\dlink\DGS121020ME\B1;

use dautkom\netswitch\library\dlink\IpmbMeInheritor as IpmbInheritor;

/**
 * @package dautkom\netswitch\library\dlink\DGS121020ME\B1
 */
class Ipmb extends \dautkom\netswitch\library\dlink\Ipmb
{
    use IpmbInheritor;
}
