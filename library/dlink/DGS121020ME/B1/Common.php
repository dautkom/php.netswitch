<?php

namespace dautkom\netswitch\library\dlink\DGS121020ME\B1;

use dautkom\netswitch\library\{
    Bandwidth, Qos
};
use dautkom\netswitch\library\dlink\{
    CableDiagnostics
};

/**
 * @package dautkom\netswitch\library\dlink\DGS121020ME\B1
 */
class Common extends \dautkom\netswitch\library\dlink\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }


    /**
     * Retrieve device serial number
     *
     * @return null|string
     */
    public function getSerialNumber()
    {
        return $this->get('.1.3.6.1.4.1.171.10.76.31.2.1.30.0');
    }

}
