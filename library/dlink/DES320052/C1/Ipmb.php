<?php

namespace dautkom\netswitch\library\dlink\DES320052\C1;
use dautkom\netswitch\library\dlink\IpmbInheritor;


/**
 * @package dautkom\netswitch\library\dlink\DES320052\C1
 */
class Ipmb extends \dautkom\netswitch\library\dlink\Ipmb
{
    use IpmbInheritor;
}
