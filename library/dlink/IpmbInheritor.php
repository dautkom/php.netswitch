<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait IpmbInheritor
{

    /**
     * Emulates swIpMacBindingPortState retrieving.
     *
     * In dlink-common-mgmt swIpMacBindingPortState returns an empty value and IP-MAC binding entry
     * consists of two settings: ARP inspection and IP inspection. We check both and aggregate'em
     * into one unified value.
     *
     * @return bool
     */
    public function getState()
    {

        $arp_state = $this->getArpInspectionState();
        $ip_state  = $this->getIpInspectionState();

        return ($ip_state === true || $arp_state === true)? true : false;

    }

    /**
     *
     * Return values:
     * 0: error
     * 1: enabled
     * 2: disabled
     *
     * @return int
     */
    private function getIpInspectionState()
    {

        /** @noinspection PhpUndefinedMethodInspection */
        $state = $this->getFromPort( '.1.3.6.1.4.1.171.12.23.3.2.1.17' );
        $state = intval($state);

        return ( $state == 1 )? true : false;

    }

    /**
     * Retrieve ARP inspection mode
     *
     * Return values:
     * 1: error
     * 2: enabled-strict
     * 3: disabled
     * 4: enabled-loose
     *
     * @return int
     */
    private function getArpInspectionState()
    {

        /** @noinspection PhpUndefinedMethodInspection */
        $state = $this->getFromPort( '.1.3.6.1.4.1.171.12.23.3.2.1.16' );
        $state = intval($state);

        return ( $state == 2 || $state == 4)? true : false;

    }

}
