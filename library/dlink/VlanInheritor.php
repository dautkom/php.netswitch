<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait VlanInheritor
{
    /**
     * Create vlan on device
     *
     * @param int $vid
     * @param string $name
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function createVlan($vid, $name)
    {

        // Get data
        $vid = ($this->validateVID($vid)) ? $vid : null;
        $vlans = $this->getAll();

        // Filter name
        $name = preg_replace('/[^\w\-_]+/', '', $name);

        if (empty($vlans) || is_null($vid)) {
            trigger_error('Error in params for ' . __METHOD__ . '()', E_USER_WARNING);
            return null;
        }

        if (array_key_exists($vid, $vlans)) {
            trigger_error("VLAN with ID '{$vid}' already exists", E_USER_WARNING);
            return null;
        }

        if (in_array($name, array_column($vlans, 'name'))) {
            trigger_error("VLAN with name '{$name}' already exists", E_USER_WARNING);
            return null;
        }

        // Array of oids + vid which will be set via snmpSet
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.5.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.1.$vid"
        ];

        // Create new vlan on switch
        $create_vlan = $this->set($oids, ['i', 's'], [5, $name]);

        // Mark Vlan as active
        $mark_vlan_active = ($create_vlan) ? $this->set(".1.3.6.1.2.1.17.7.1.4.3.1.5.$vid", 'i', 1) : false;

        return $create_vlan && $mark_vlan_active;

    }


    /**
     * Sets current port as non-member in a $vid VLAN
     *
     * @param int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function deleteVlanOnPort($vid)
    {

        // Get data
        $port = self::$port;
        $vid = ($this->validateVID($vid)) ? $vid : null;
        $vlans = $this->getAll();
        $fdb = self::$_fdb->showPortPermanent();

        // Check VID
        if (is_null($vid)) {
            trigger_error('Wrong VLAN ID specified', E_USER_WARNING);
            return null;
        }

        // Check Vlan's
        if (empty($vlans) || !is_array($vlans) || !array_key_exists($vid, $vlans)) {
            trigger_error("Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING);
            return null;
        }

        // Check FDB table
        if (!empty($fdb)) {
            trigger_error('Remove all static MAC addresses from VLAN before deleting it', E_USER_WARNING);
            return null;
        }

        $vlans[$vid]['egress']['bin']{$port - 1} = 0;
        $vlans[$vid]['untagged']['bin']{$port - 1} = 0;

        $mask_egress = $this->getHexMaskFromBin($vlans[$vid]['egress']['bin']);
        $mask_untagged = $this->getHexMaskFromBin($vlans[$vid]['untagged']['bin']);

        $set_egress = $this->set(".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid", 'x', $mask_untagged);
        $set_untagged = ($set_egress) ? $this->set(".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid", 'x', $mask_egress) : false;

        return ($set_egress && $set_untagged);

    }


    /**
     * Add $port on current port as untagged to a $vid VLAN
     *
     * @param int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function addUntaggedVlanOnPort($vid)
    {

        // Get data
        $port = self::$port;
        $vid = ($this->validateVID($vid)) ? $vid : null;
        $vlans = $this->getAll();

        // Check vid
        if (is_null($vid)) {
            trigger_error('Wrong VLAN ID specified', E_USER_WARNING);
            return null;
        }

        // Check VLANs
        if (empty($vlans) || !is_array($vlans) || !array_key_exists($vid, $vlans)) {
            trigger_error("Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING);
            return null;
        }

        // Prevent untagged ports overlapping
        if ($this->allow_untagged_overlap === false) {
            foreach ($vlans as $v_id => $vlan) {
                /**
                 * Ignore default vlan?
                 * @see Vlan::ignore_default_vlan
                 */
                if ($this->ignore_default_vlan) {
                    if ($v_id == 1) continue; /* skip default vlan */
                }

                if ($vlan['untagged']['bin']{$port - 1} == 1) {
                    trigger_error("Untagged ports overlapped in VLAN with ID '$v_id' ( {$vlan['name']} )", E_USER_WARNING);
                    return null;
                }
            }
        }

        $vlans[$vid]['egress']['bin']{$port - 1} = 1;
        $vlans[$vid]['untagged']['bin']{$port - 1} = 1;

        $mask_egress = $this->getHexMaskFromBin($vlans[$vid]['egress']['bin']);
        $mask_untagged = $this->getHexMaskFromBin($vlans[$vid]['untagged']['bin']);

        $set_egress = $this->set(".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid", 'x', $mask_egress);
        $set_untagged = ($set_egress) ? $this->set(".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid", 'x', $mask_untagged) : false;

        return ($set_egress && $set_untagged);

    }


    /**
     * Add current port as tagged (egress) to a $vid VLAN
     *
     * @param int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function addTaggedVlanOnPort($vid)
    {

        // Get data
        $port = self::$port;
        $vid = ($this->validateVID($vid)) ? $vid : null;
        $vlans = $this->getAll();

        // Check vid
        if (is_null($vid)) {
            trigger_error('Wrong VLAN ID specified', E_USER_WARNING);
            return null;
        }

        // Check Vlan's
        if (empty($vlans) || !is_array($vlans) || !array_key_exists($vid, $vlans)) {
            trigger_error("Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING);
            return null;
        }

        // Check if port is transit
        if ($this->isTransit()) {
            trigger_error("Port $port is set up as transit", E_USER_WARNING);
            return null;
        }

        // Check if port is untagged
        foreach ($vlans as $v_id => $vlan) {
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if ($this->ignore_default_vlan) {
                if ($v_id == 1) continue; /* skip default vlan */
            }

            if ($vlan['untagged']['bin']{$port - 1} == 1) {
                trigger_error("Port $port is marked as untagged in VLAN '$v_id' ( {$vlan['name']} )", E_USER_WARNING);
                return null;
            }
        }

        $vlans[$vid]['egress']['bin']{$port - 1} = 1;

        $mask_egress = $this->getHexMaskFromBin($vlans[$vid]['egress']['bin']);

        return $this->set(".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid", 'x', $mask_egress);

    }


    /**
     * Changes $vid VLAN setting by two masks - egress and untagged.
     * Both masks must be in hex format.
     *
     * @param $vid int
     * @param $mask_egress string Hex-string
     * @param $mask_untagged string Hex-string
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection, PhpUnused
     */
    public function setVlan($vid, $mask_egress, $mask_untagged)
    {

        // Get data
        $vid = ($this->validateVID($vid)) ? $vid : null;
        $vlans = $this->getAll();

        // Check VLANs
        if (empty($vlans)) {
            trigger_error("Couldn't retrieve VLANs from device", E_USER_WARNING);
            return null;
        } elseif (!array_key_exists($vid, $vlans)) {
            trigger_error("Specified VLAN ID was not found on device", E_USER_WARNING);
            return null;
        }

        // Check hex masks
        if (strlen($mask_egress) != self::$device['vlan']['outHexLength'] || strlen($mask_untagged) != self::$device['vlan']['outHexLength']) {
            trigger_error("Mask error", E_USER_WARNING);
            return null;
        }

        // Prevent untagged ports overlapping
        if ($this->allow_untagged_overlap === false) {

            $binary_mask = substr($mask_untagged, 0, self::$device['vlan']['inHexLength']);
            $binary_mask = self::$_vlan->getBinMaskFromHex($binary_mask);
            $binary_mask = gmp_init($binary_mask, 2);

            foreach ($vlans as $vlan_id => $vlan) {
                /**
                 * Ignore default vlan?
                 * @see Vlan::ignore_default_vlan
                 */
                if ($this->ignore_default_vlan) {
                    if ($vlan_id == 1) continue; /* skip default vlan */
                }

                if ($vlan_id != $vid) {

                    $untagged = null;
                    $untagged = gmp_init($vlan['untagged']['bin'], 2);
                    $check = gmp_and($binary_mask, $untagged);
                    $check = gmp_strval($check, 2);

                    if ($check != '0' && stripos($check, '1') !== false) {
                        trigger_error("Untagged ports overlapped in VLAN with ID $vlan_id ({$vlan['name']})", E_USER_WARNING);
                        return null;
                    }

                }

            }

        }

        $arguments = [
            [".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid", $mask_egress],
            [".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid", $mask_untagged]
        ];

        if (preg_match('/^0+$/is', $mask_egress) && preg_match('/^0+$/is', $mask_untagged)) {
            $arguments = array_reverse($arguments);
        }

        $set_command_1 = $this->set($arguments[0][0], 'x', $arguments[0][1]);
        $set_command_2 = ($set_command_1) ? $this->set($arguments[1][0], 'x', $arguments[1][1]) : false;

        return ($set_command_1 && $set_command_2);

    }


    /**
     * Configures GVRP PVID of current port.
     * To reset setting to default value, use setGvrpPvid(1).
     *
     * @param int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function setGvrpPvid($vid)
    {
        return $this->notImplemented();
    }

}
