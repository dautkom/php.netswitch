<?php

namespace dautkom\netswitch\library\dlink;

/**
 * @package dautkom\netswitch\library\dlink
 */
class CableDiagnostics extends \dautkom\netswitch\library\CableDiagnostics
{

    /**
     * Start cable diagnostics.
     * Before using any getters you should start diagnostics with this method.
     *
     * @param int $cycles
     * @return bool
     */
    public function init( int $cycles = 10 ): bool
    {

        $current_port = new Port;

        /**
         * @var array Medium type value unification
         */
        $medium_convert = array( 1 => '0', 100 => '0', 2 => '1', 101 => '1' );

        /*
         * No cable-diagnostics on optic ports
         */
        if( !$current_port->isCombo() ) {
            $ini_type = self::$device['ports'][self::$port][0];
            $medium_type =  (!empty( $ini_type ) ) ? intval( $medium_convert[$ini_type] ) : null;

            if($medium_type == 1 || is_null($medium_type)) {
                $this->init = false;
                return false;
            }
        }

        /*
         * Initialize Cable Diagnostic check
         */
        $this->setToPort( self::$device['snmp']['swEtherCableDiagAction'], 'i', 1 );

        /**
         * Wait for response status
         * 2 - processing
         * 3 - finished, OK
         */
        for( $i=0; $i<$cycles; $i++ ) {

            // Perform query if check is finished
            $status = null;

             // Suppress no link port error
            $status = @$this->getFromPort( self::$device['snmp']['swEtherCableDiagAction'] );

            // Set flag and return 'true' if check successfully finished
            if( $status == 3 ) {
                $this->init = true;
                return true;
            }

            // wait 0.1 second for next loop step
            usleep( 100000 );

        }

        $this->init = false;
        return false;
    }


    /**
     * Retrieves pairs statuses, taking into account port type.
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * @return null|array
     */
    public function getState()
    {
        if( !$this->init ) {
            return null;
        }

        $result = array();

        for($i = 1; $i <= 4; $i++) {
            $result[$i] = $this->getFromPort(self::$device['snmp']["swEtherCableDiagPair{$i}Status"]);
        }

        return $result;

    }

    /**
     * Retrieve pairs length in meters.
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * @return null|array
     */
    public function getLength()
    {

        if( !$this->init ) {
            return null;
        }

        $result    = array();

        for($i = 1; $i <= 4; $i++) {
            $result[$i] = $this->getFromPort(self::$device['snmp']["swEtherCableDiagPair{$i}Length"]);
        }

        return $result;

    }

}
