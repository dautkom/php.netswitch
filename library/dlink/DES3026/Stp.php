<?php

namespace dautkom\netswitch\library\dlink\DES3026;


/**
 * @package dautkom\netswitch\library\dlink\DES3026
 */
class Stp extends \dautkom\netswitch\library\dlink\DES3018\Stp
{

    /**
     * Retrieve switch stp status
     *
     * @return int
     */
    public function getStpStatus()
    {
        /*
         * Value List
         * other (1)
         * disabled (2)
         * enabled (3)
         */
        $status = $this->get( '.1.3.6.1.4.1.171.11.63.3.2.1.2.6.0' );

        // Value unification
        return ( intval( $status ) == 3 ) ? 1 : 2;
        
    }

}
