<?php

namespace dautkom\netswitch\library\dlink\DES3026;


/**
 * @package dautkom\netswitch\library\dlink\DES3026
 */
class CableDiagnostics extends \dautkom\netswitch\library\dlink\CableDiagnostics
{

    /**
     * No cable diagnostic on DES-3026.
     *
     * @param  int $cycles
     * @return bool
     */
    public function init(int $cycles = 10): bool
    {
        return false;
    }

}
