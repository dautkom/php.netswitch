<?php

namespace dautkom\netswitch\library\dlink\DES3026;
use dautkom\netswitch\library\dlink\FdbInheritor;

/**
 * @package dautkom\netswitch\library\dlink\DES3026
 */
class Fdb extends \dautkom\netswitch\library\Fdb
{

    /**
     * This trait overrides methods:
     * @see dautkom\netswitch\library\Fdb::addPermanent
     * @see dautkom\netswitch\library\Fdb::getHexMaskFromPortNumber
     */
    use FdbInheritor;

}
