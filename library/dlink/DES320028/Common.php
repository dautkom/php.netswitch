<?php

namespace dautkom\netswitch\library\dlink\DES320028;
use dautkom\netswitch\library\{
    Bandwidth, 
    Fdb, 
    Qos, 
    Vlan, 
    dlink\CableDiagnostics, 
    dlink\Ipmb, 
    dlink\PortSecurity, 
    dlink\Stp
};


/**
 * @package dautkom\netswitch\library\dlink\DES320028
 */
class Common extends \dautkom\netswitch\library\dlink\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }

}
