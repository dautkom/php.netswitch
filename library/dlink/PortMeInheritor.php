<?php

namespace dautkom\netswitch\library\dlink;


/**
 * Trait for library\dlink\Port, intended to be used for the sake of DRY
 * Contains private wrappers for retrieving and setting data from/to port
 *
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait PortMeInheritor
{

    /**
     * Get data from specific oid
     *
     * @param  $oid
     * @return int|null
     */
    protected function traitGetFromPort($oid)
    {
        $snmp = parent::getFromPort($oid);
        $res  = preg_replace('/[^\d]/', '', $snmp);
        return intval($res);
    }

    /**
     * Set data to specific oid with specific mode
     *
     * @param string $oid
     * @param int $mode
     * @return bool
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    protected function traitSetToPort($oid, $mode): bool
    {
        $mode = intval($mode);

        if ($mode < 1 || $mode > 2) {
            trigger_error('Wrong parameter specified for ' . __METHOD__ . '()', E_USER_WARNING);
            return false;
        }

        return parent::setToPort($oid, 'i', $mode);
    }

}
