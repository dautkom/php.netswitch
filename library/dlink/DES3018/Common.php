<?php

namespace dautkom\netswitch\library\dlink\DES3018;
use dautkom\netswitch\library\{
    Bandwidth, 
    Qos, 
    dlink\Ipmb, 
    dlink\SaveInheritor
};


/**
 * @package dautkom\netswitch\library\dlink\DES3018
 */
class Common extends \dautkom\netswitch\library\dlink\Common
{

    /**
     * Trait utilizes saveSnmp() method
     *
     * @see SaveInheritor::saveSnmp()
     */
    use SaveInheritor;

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }


    /**
     * No software version could be retrieved via SNMP on DES-3018.
     * If it's necessary - telnet and retrieve it from parsing initial output.
     *
     * @return null
     */
    public function getSoftwareRevision()
    {
        return $this->notImplemented();
    }


    /**
     * No hardware revision could be retrieved via SNMP on DES-3018.
     *
     * @return null
     */
    public function getHardwareRevision()
    {
        return $this->notImplemented();
    }


    /**
     * No serial number could be retrieved via SNMP on DES-3018
     *
     * @return null
     */
    public function getSerialNumber()
    {
        return $this->notImplemented();
    }

}
