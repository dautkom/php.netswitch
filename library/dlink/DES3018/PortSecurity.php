<?php

namespace dautkom\netswitch\library\dlink\DES3018;


/**
 * @package dautkom\netswitch\library\dlink\DES3018
 */
class PortSecurity extends \dautkom\netswitch\library\dlink\PortSecurity
{

    /**
     * Retrieve port security admin state.
     * Return NULL if snmp response is empty or switch returned state other (1)
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getState()
    {

        // Port security can't be changed on SFP modules ports, that's why it's always return 1 (enabled (1))
        if( self::$port > 16 ) {
            return 1;
        }

        //Get data
        $snmp = $this->getFromPort( self::$device['snmp']['swL2PortSecurityAdmState'] );

        /**
         * @var array State value unification
         */
        $state_convert = [1 => '0', 2 => '1', 3 => '2'];

        return ( !empty($snmp) && $snmp != 1 ) ? intval( $state_convert[$snmp] ) : null;

    }


    /**
     * Changes port security admin state.
     *
     * Mode argument values:
     * 1 : enabled
     * 2 : disabled
     *
     * @param  int $mode
     * @return bool
     */
    public function setState(int $mode): bool
    {

        // Port security can't be changed on SFP modules ports
        if( self::$port > 16 ) {
            trigger_error( 'Port security settings can\'t be changed on SFP modules ports', E_USER_WARNING );
            return false;
        }

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()', E_USER_WARNING );
            return false;
        }

        /**
         * @var array State value unification
         */
        $mode_convert = [1 => '2', 2 => '3'];

        return $this->setToPort( self::$device['snmp']['swL2PortSecurityAdmState'], 'i', $mode_convert[$mode] );

    }


    /**
     * Retrieve maximum amount of learned addresses on port
     *
     * @return int|null
     */
    public function getMaxAddresses()
    {

        // Port security can't be changed on SFP modules ports
        if( self::$port > 16 ) {
            trigger_error( 'Port security settings can\'t be changed on SFP modules ports', E_USER_WARNING );
            return null;
        }

        //Get data
        $snmp = $this->getFromPort( self::$device['snmp']['PortSecurityMaxLernAddr'] );
        return ( $snmp !== false ? intval( $snmp ) : null );

    }


    /**
     * Changes amount of learned addresses on port.
     * DES-3018 Max.Learning Addr 0 - 10
     * Will trigger error and return null if:
     *  1. trying to change max learned adress amount on SFP module port
     *  2. trying to set max learned address < 0 or > 10
     *
     * @param  int $value
     * @return bool
     */
    public function setMaxAddresses(int $value): bool
    {

        // Port security can't be changed on SFP modules ports
        if( self::$port > 16 ) {
            trigger_error( 'Port security settings can\'t be changed on SFP modules ports', E_USER_WARNING );
            return false;
        }

        $value = intval( $value );

        if( $value < 0 || $value > 10 ) {
            trigger_error( 'Invalid amount of max learned addresses. Must be between 0 and 10 inclusive.',  E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['PortSecurityMaxLernAddr'], 'i', $value );

    }


    /**
     * Retrieves port security mode.
     *
     * Return values:
     *  0 : error
     *  1 : other
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @return int|null
     */
    public function getMode()
    {

        if( self::$port > 16 ) {
            trigger_error( 'No port security modes on SFP modules ports', E_USER_WARNING );
            return 1;
        }

        //Get Data
        $snmp = $this->getFromPort(self::$device['snmp']['swL2PortSecurityMode']);

        // SNMP value unification
        $snmp = ( $snmp == 1 ) ? 2 : $snmp;

        return ( $snmp !== false ) ? intval( $snmp ) : null;
    }


    /**
     * Changes port security mode.
     * Return null if wrong parameter specified
     *
     * Mode value list:
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setMode( $mode )
    {

        // Port security can't be changed on SFP modules ports
        if( self::$port > 16 ) {
            trigger_error( 'Port security settings can\'t be changed on SFP modules ports', E_USER_WARNING );
            return null;
        }

        $mode = intval($mode);

        if( $mode < 2 || $mode > 4 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return null;
        }

        // Mode value unification
        $mode = ( $mode == 2 ) ? 1 : $mode;

        return $this->setToPort( self::$device['snmp']['swL2PortSecurityMode'], 'i', $mode );

    }

}
