<?php

namespace dautkom\netswitch\library\dlink\DES3018;


/**
 * @package dautkom\netswitch\library\dlink\DES3018
 */
class Stp extends \dautkom\netswitch\library\dlink\Stp
{
    /**
     * @var array
     */
    protected $fdb_filter = [
        "/Hex:/",
        "/Hex-/",
        "/x:/",
        "/x-/",
        "/ /",
        "/Hex-STRING:/"
    ];

    /**
     * Retrieve switch instance stp priority
     *
     * @return int
     */
    public function getPriority()
    {

        $priority = $this->get( ".1.3.6.1.2.1.17.2.2.0" );
        return intval($priority);

    }

    /**
     * Set priority of instance.
     * The priority must be dividable by 4096
     *
     * @param int $priority
     * @throws \RuntimeException
     * @return bool|null
     */
    public function setPriority( $priority )
    {

        if( $priority%4096 != 0 ) {
            throw new \RuntimeException("Wrong priority value! Must be dividable by 4096.");
        }

        if( $this->getPriority() == $priority ) {
            return true;
        }

        return $this->set( ".1.3.6.1.2.1.17.2.2.0", 'i', $priority );

    }

    /**
     * The port number of the port which offers the lowest cost path
     * from this bridge to the root bridge.
     *
     * @return int
     */
    public function getRootPort()
    {

        $this->checkStpStatus();

        return intval( $this->get( '.1.3.6.1.2.1.17.2.7.0' ) );

    }

    /**
     * Retrieve switch stp status
     *
     * @return int
     */
    public function getStpStatus()
    {
        /*
         * Value List
         * other (1)
         * disabled (2)
         * enabled (3)
         */
        $status = $this->get( '.1.3.6.1.4.1.171.11.63.2.2.1.2.6.0' );

        //return value unification
        return ( intval( $status ) == 3 )? 1 : 2;
    }

    /**
     * Retreive root switch mac-address
     *
     * @return string|null
     */
    public function getRootMac()
    {
        $this->checkStpStatus();

        $rootPort = $this->getRootPort();

        if( $rootPort == 0 ) {
            return null;
        }

        // Returns octet string 4 symbols - hex priority, 12 symbols - mac
        // Example: 7000ffffffd8fffffffeffffffe3ffffff9233ffffff80
        $rootMac = $this->get( '.1.3.6.1.2.1.17.2.5.0' );

        return $this->filterMac($rootMac);

    }

    /**
     * Retreive desgnated bridge mac
     *
     * @return string|null
     */
    public function getDesignatedBridgeMac()
    {
        $this->checkStpStatus();

        $rootPort = $this->getRootPort();

        if( $rootPort == 0 ) {
            return null;
        }

        // Returns octet string 4 symbols - hex priority, 12 symbols - mac
        // Example: 7000ffffffd8fffffffeffffffe3ffffff9233ffffff80
        $rootBridgeMac = $this->get( ".1.3.6.1.2.1.17.2.15.1.8.$rootPort" );
        $rootBridgeMac = $this->filterMac( $rootBridgeMac );

        return ( $this->getRootMac() == $rootBridgeMac )? null : $rootBridgeMac;
    }

    /**
     * Retreive switch mac
     *
     * @throws \RuntimeException
     * @return string
     */
    public function getSwitchMac()
    {
        $switchMac = $this->get( '.1.3.6.1.2.1.17.1.1.0' );

        $switchMac = strtolower( preg_replace( $this->fdb_filter, '', $switchMac ) );

        if( strlen( $switchMac ) != 12 ) {
            throw new \RuntimeException( "Wrong MAC address specified." );
        }

        return $switchMac;
    }

    /**
     * Retreive stp status on current port
     *
     * Value List:
     * enabled (1)
     * disabled (2)
     *
     * @return int
     */
    public function getStateOnPort() {

        $status = $this->getFromPort( '.1.3.6.1.2.1.17.2.15.1.4' );

        return intval( $status );
    }


    /**
     * Set stp status (enabled/disabled) on current port
     *
     * Value List:
     * enabled (1)
     * disabled (2)
     *
     * @throws \RuntimeException
     * @param $set
     * @return bool|null
     */
    public function setStateOnPort( $set ) {

        if( $set != 1 && $set != 2 ) {
            throw new \RuntimeException("Wrong stp state value.");
        }

        if( $this->getStateOnPort() == $set ) {
            return true;
        }

        return ( $this->setToPort( '.1.3.6.1.2.1.17.2.15.1.4', 'i', $set ) );

    }

    /**
     * Mac value filtering
     *
     * @throws \RuntimeException
     * @param string $mac
     * @return string
     */
    protected function filterMac( $mac ) {

        $mac = strtolower( preg_replace( $this->fdb_filter, '', $mac ) );

        $mac = substr( $mac, 4 );

        if( strlen( $mac ) != 12 ) {
            throw new \RuntimeException( "Wrong MAC address specified." );
        }

        return $mac;
    }

    /**
     * Stp global status check
     *
     * @throws \RuntimeException
     * @return bool
     */
    protected function checkStpStatus() {

        if( $this->getStpStatus() == 2 ) {
            throw new \RuntimeException("Stp is disabled.");
        }

        return true;

    }


}
