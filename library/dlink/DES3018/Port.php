<?php

namespace dautkom\netswitch\library\dlink\DES3018;


/**
 * @package dautkom\netswitch\library\dlink\DES3018
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * No duplex mode could be retrieved via SNMP on DES-3018.
     * OID doesn't exist for this device.
     *
     * @see    Port::getDuplex()
     * @return null
     */
    public function getDuplex()
    {
        return $this->notImplemented();
    }


    /**
     * Retrieve interface operating mode
     *
     * @see    Port::getLink()
     * @return int
     */
    public function getLink()
    {

        if( is_null(self::getPortNumber()) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         *  1: auto
         *  2: half-10Mbps
         *  3: full-10Mbps
         *  4: half-100Mbps
         *  5: full-100Mbps
         *  6: half-1Gigabps
         *  7: full-1Gigabps
         */
        $link = $this->getFromPort(".1.3.6.1.4.1.171.11.63.2.2.2.1.1.5");
        $link = intval($link) - 1;

        // Unification
        return ($link < 0) ? 0 : $link;

    }


    /**
     * Retrieve port amount.
     *
     * Instead of default configuration value reading, it checks real port amount
     * on the device if active connection exists. Depending on expansion SFP module
     * there can be 16, 17 or 18 ports. Writing to 17 port if no SFP installed will
     * cause SNMP error of inexistant OID, that's why we check real port amount.
     *
     * @return int
     */
    public function count(): int
    {

        if( !self::$connected ) {
            return parent::count();
        }
        else {

            // swL2PortCtrlPortIndex
            $snmp = count($this->walk('.1.3.6.1.4.1.171.11.63.2.2.2.2.1.1'));

            if( !empty($snmp) ) {
                return $snmp;
            }
            else {
                trigger_error("Unable to retrieve port amount via SNMP, using default value", E_USER_NOTICE);
                return parent::count();
            }

        }

    }


    /**
     * Set current port number and save it in Core::$port property
     *
     * Overrides dautkom\netswitch\library\Port::setPortNumber() method to avoid
     * setting current port if no SFP module is installed in that port.
     *
     * @throws \RuntimeException
     * @param  int $port
     * @return bool
     */
    public function setPortNumber(int $port): bool
    {

        $port = intval($port);

        if( !$this->validatePortNumber($port) ) {
            throw new \RuntimeException("Wrong port specified");
        }

        return parent::setPortNumber($port);

    }


    /**
     * Validate port number
     *
     * @param  int $port
     * @return bool
     */
    public function validatePortNumber(int $port): bool
    {

        $port = intval($port);

        if( self::$connected && $port > $this->count() ) {
            return false;
        }

        return parent::validatePortNumber($port);
    }


    /**
     * Retrieves learning state ( enabled/disabled ) on current port
     * Learning is always enabled on DES-30xx models
     *
     * @return int
     */
    public function getLearningState()
    {
        return 1;
    }


    /**
     * Set learning state on current port
     * Learning state can not be set via SNMP on DES-3018.
     *
     * @param  int $mode
     * @return null
     */
    public function setLearningState(int $mode)
    {
        return $this->notImplemented();
    }

}
