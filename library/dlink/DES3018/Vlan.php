<?php

namespace dautkom\netswitch\library\dlink\DES3018;


/**
 * @package dautkom\netswitch\library\dlink\DES3018
 */
class Vlan extends \dautkom\netswitch\library\Vlan
{

    /**
     * @param  string $hex
     * @return string
     */
    public function getBinMaskFromHex($hex)
    {
        return strrev(parent::getBinMaskFromHex($hex));
    }


    /**
     * @param  int $bin
     * @return string
     */
    public function getHexMaskFromBin( $bin )
    {

        // Split bin mask to octets
        $bin = strrev( $bin );
        $bin = str_split( $bin, 8 );

        // Form hex mask
        $result = '';
        array_walk( $bin, function( $octet ) use ( &$result ) {
            $result .= str_pad( dechex( bindec( $octet ) ), 2, "0", STR_PAD_LEFT );
        });

        // Return hex mask
        return str_pad( $result, self::$device['vlan']['outHexLength'], '0', STR_PAD_RIGHT );

    }


    /**
     * GVRP is not supported on DES-3018
     *
     * @param int $vid
     * @return null
     */
    public function setGvrpPvid( $vid )
    {
        return $this->notImplemented();
    }

}
