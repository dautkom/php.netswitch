<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\dlink
 */
trait FdbMeInheritor
{

    /**
     * Retrieves all permanent MAC-addresses from FDB table via SNMP
     * Result array (
     *     int portNumber => array(
     *         array(0 => string MAC, 1 => string VID),
     *         ...
     *     )
     *     ...
     * )
     *
     * @return array
     */
    public function showAllPermanent(): array
    {

        $fdb = [];
        $snmp = array_keys(@$this->walk(self::$device['snmp']['fdbStaticEntries']));

        array_walk($snmp, function (&$value) {
            $value = explode('.', preg_replace($this->fdb_filter, '', $value));
        });

        foreach ($snmp as $key => $octet_list) {
            $port = array_pop($octet_list);
            $vlan = array_shift($octet_list);

            $mac = implode('', array_map(function ($value) {
                return str_pad(dechex($value), 2, '0', STR_PAD_LEFT);
            }, $octet_list));

            $fdb[$port][] = [$mac, $vlan];
        }

        ksort($fdb);
        return $fdb;

    }


    /**
     * Add static (permanent) MAC address to FDB table on current port
     *
     * @param string $mac MAC-address
     * @param int $vid VLAN ID
     * @return bool
     */
    public function addPermanent(string $mac, int $vid): bool
    {

        $port = self::$port;
        $mac = $this->filterMac($mac);

        if (empty($mac) || !$port || !self::$_vlan->validateVID($vid)) {
            trigger_error('Wrong data specified', E_USER_WARNING);
            return false;
        }

        // Retrieve FDB table and check if mac already exists in fdb table
        $fdb = $this->showAllPermanent();
        foreach ($fdb as $used_port => $value) {
            if (in_array($mac, array_column($value, 0))) {
                trigger_error("Duplicated FDB entry - MAC already exists on port {$used_port}", E_USER_WARNING);
                return false;
            }
        }

        // Check Vlan on port
        $vlans = self::$_vlan->getAll();
        if (empty($vlans) || !array_key_exists($vid, $vlans)) {
            trigger_error("Vlan {$vid} not exist on this switch or error occurred!", E_USER_WARNING);
            return false;
        }

        if ($vlans[$vid]['egress']['bin'][$port - 1] != 1 && $vlans[$vid]['untagged']['bin'][$port - 1] != 1) {
            trigger_error("Port must be in $vid VLAN to add permanent MAC-address in it", E_USER_WARNING);
            return false;
        }

        $oid = self::$device['snmp']['fdbStaticEntries'] . ".{$vid}.{$this->convertMacHexDec($mac, false)}";
        return $this->setToPort($oid, 'i', 4);

    }


    /**
     * Delete unicast MAC-address from FDB table.
     *
     * CLI analogue: delete fdb from <vlan_name> <mac_addr>
     *
     * @param string $mac MAC-address
     * @param int $vid VLAN ID
     * @return bool
     */
    public function deletePermanent(string $mac, int $vid): bool
    {
        // Check and filter input data
        $mac = $this->filterMac($mac);

        if (empty($mac) || !self::$_vlan->validateVID($vid)) {
            trigger_error("Error input data for " . __METHOD__, E_USER_WARNING);
            return false;
        }

        // Get permanent FDB table
        $fdb = $this->showAllPermanent();

        if (empty($fdb)) {
            trigger_error("Empty FDB table " . __METHOD__, E_USER_WARNING);
            return false;
        }

        $fdb = array_reduce(array_merge(...array_values($fdb)), function ($accumulator, $item) {
            if (!array_key_exists($item['1'], $accumulator)) {
                $accumulator[$item['1']] = [];
            }
            array_push($accumulator[$item['1']], $item[0]);
            return $accumulator;
        }, []);


        if (!array_key_exists($vid, $fdb) || !in_array($mac, $fdb[$vid])) {
            trigger_error("Such combination of VID-MAC({$vid}-{$mac}) does does not exist in FDB table", E_USER_WARNING);
            return false;
        }

        $oid = self::$device['snmp']['fdbStaticEntries'] . ".{$vid}.{$this->convertMacHexDec($mac, false)}";
        return $this->setToPort($oid, 'i', 6);

    }

}
