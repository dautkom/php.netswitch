<?php

namespace dautkom\netswitch\library\dlink\DES3526;


/**
 * @package dautkom\netswitch\library\dlink\DES3526
 */
class Bandwidth extends \dautkom\netswitch\library\Bandwidth
{

    /**
     * Get data from specific oid
     *
     * @param  string   $oid   SNMP object identifier
     * @param  int|null $state egress state for zyxels, left for inheritance compatibility
     * @return int
     */
    protected function getRate(string $oid, $state=null): int
    {
        $snmp = $this->getFromPort( $oid );
        return intval( $snmp * 1024 );

    }


    /**
     * Set data to specific oid with specific mode
     *
     * @param  string $oid    SNMP object identifier
     * @param  int    $value  speed limit value
     * @param  string $type   Rx or Tx
     * @return bool
     */
    protected function setRate( string $oid, int $value, string $type = ''): bool
    {

        $value = abs( intval( $value ) );

        // Check if port is combo
        $isCombo = (new Port)->isCombo();

        // Set max bandwidth if value equals 0
        $value = ( $value === 0 ) ?  1000 : round( $value / 1024, 0 );

        // On Gigabit ports value must be multiple of 8. Possible values: 8, 16, 24, 32, 40, etc.
        if( $isCombo && $value % 8 != 0 ) {
            trigger_error( 'Gigabit port rate limit must be multiple of 8192 Kbits/s( 8 Mbits/sec )', E_USER_WARNING );
            return false;
        }

        if( $value < 1 ) {
            trigger_error( $type . ' must be >1024 kbit/s for DES35xx', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( $oid, 'i', $value );

    }

}
