<?php

namespace dautkom\netswitch\library\dlink\DES3526;
use dautkom\netswitch\library\{
    Fdb, 
    Qos, 
    Vlan, 
    dlink\CableDiagnostics,
    dlink\Ipmb, 
    dlink\PortSecurity, 
    dlink\Stp
};


/**
 * @package dautkom\netswitch\library\dlink\DES3526
 */
class Common extends \dautkom\netswitch\library\dlink\Common
{

    /**
     * Changes default save method to save via Telnet.
     * Saving DES-3526 conf via SNMP could cause its' SNMP engine's freeze for 2-3 minutes.
     * But switch also can be saved via SNMP, for those who love hardcore.
     * @var string
     */
    protected $mode = 'telnet';

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }


    /**
     * No serial number could be retrieved via SNMP on DES-3526
     *
     * @return null
     */
    public function getSerialNumber()
    {
        return $this->notImplemented();
    }

}
