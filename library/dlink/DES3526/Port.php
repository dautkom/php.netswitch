<?php

namespace dautkom\netswitch\library\dlink\DES3526;
use dautkom\netswitch\library\dlink\MediumType;


/**
 * @package dautkom\netswitch\library\dlink\DES3526
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DES-3526
     * This trait modifies Core::addPortNumberToOID() method
     */
    use MediumType;


    /**
     * No duplex mode could be retrieved via SNMP on DES-3526.
     * OID doesn't exist for this device.
     *
     * @see    Port::getDuplex()
     * @return null
     */
    public function getDuplex()
    {
        return $this->notImplemented();
    }


    /**
     * Retrieve interface operating mode
     *
     * @return bool|int
     */
    public function getLink()
    {

        if( is_null(self::getPortNumber()) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         *  other(1),
         *  auto(2),
         *  half-10Mbps(3),
         *  full-10Mbps(4),
         *  half-100Mbps(5),
         *  full-100Mbps(6),
         *  half-1Gbps(7),
         *  full-1Gbps(8)
         */
        $link = $this->getFromPort(".1.3.6.1.4.1.171.11.64.1.2.4.4.1.6");

        // Combo port workaround
        if(is_array($link)) {
            $mode = intval(array_sum($link)) - 2;
            $mode = ($mode < 2) ? 2 : $mode;
        }
        else {
            $mode = intval($link);
        }

        if($mode > 8) $mode = 2;
        $mode = $mode - 2;

        // Unification
        return ($mode < 0) ? 0 : $mode;

    }

}
