<?php

namespace dautkom\netswitch\library\dlink;


/**
 * @traitUses NetSwitch
 * @package   dautkom\netswitch\library\dlink
 */
trait SaveInheritor
{

    /**
     * Save configuration via SNMP
     * DES-30xx and switches with C1 hardware revision doesn't have proper agentStatusSaveCfg and it's impossible
     * to check if configuration was successfuly saved via SNMP.
     *
     * @return bool
     */
    protected function saveSnmp()
    {

        /** Suppress error "No response from switch"   */
        /** @noinspection PhpUndefinedMethodInspection, @noinspection PhpUndefinedFieldInspection */
        @$this->set( self::$device['snmp']['agentSaveCfg'], 'i', self::$device['snmp']['agentSaveCfgValue'] );

        // In hope that switch saved successfully
        return true;

    }

}
