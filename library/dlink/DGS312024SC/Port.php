<?php

namespace dautkom\netswitch\library\dlink\DGS312024SC;
use dautkom\netswitch\library\dlink\{
    MediumType, PortInheritor
};


/**
 * @package dautkom\netswitch\library\dlink\DGS312024SC
 */
class Port extends \dautkom\netswitch\library\dlink\Port
{

    /**
     * We have `combo port` and `medium type` concepts for DGS-312024SC
     * This trait modifies @see dautkom\netswitch\library\Port::addPortNumberToOID() method
     */
    use MediumType;


    /**
     * Trait utilizes traitGetFromPort and traitSetToPort methods
     */
    use PortInheritor;


    /**
     * Retrieve interface operating mode
     *
     * @see    dautkom\netswitch\library\Port::getLink()
     * @return bool|int
     */
    public function getLink()
    {

        if( is_null( self::getPortNumber() ) ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        /**
         * Original swL2PortInfoNwayStatus return value list:
         * link-down(0)
         * full-10Mbps-8023x(1)
         * full-10Mbps-none(2)
         * half-10Mbps-backp(3)
         * half-10Mbps-none(4)
         * full-100Mbps-8023x(5)
         * full-100Mbps-none(6)
         * half-100Mbps-backp(7)
         * half-100Mbps-none(8)
         * full-1Gigabps-8023x(9)
         * full-1Gigabps-none(10)
         * half-1Gigabps-backp(11)
         * half-1Gigabps-none(12)
         * full-10Gigabps-8023x(13)
         * full-10Gigabps-none(14)
         * half-10Gigabps-8023x(15)
         * half-10Gigabps-none(16)
         * empty (17)
         * err-disabled (18)
         */
        $link = $this->getFromPort( ".1.3.6.1.4.1.171.11.117.1.3.2.3.1.1.6" );

        // Queried combo-port
        if( is_array( $link ) ) {
            $mode = intval( array_sum( $link ) );
        }

        // Queried regular port
        else {
            $mode = intval($link);
        }

        // Possible link status matrix
        $convert_link_status = [
            1  => '2', 2  => '2', # full-10Mbps
            3  => '1', 4  => '1', # half-10Mbps
            5  => '4', 6  => '4', # full-100Mbps
            7  => '3', 8  => '3', # half-100Mbps
            9  => '6', 10 => '6', # full-1Gbps
            11 => '5', 12 => '5', # half-1Gbps
            13 => '8', 14 => '8', # full-10Gbps
            15 => '7', 16 => '7', # half-10Gbps
            0  => '0', 17 => '0', 18 => '0', # no-link or empty or err-disabled
        ];

        return intval( $convert_link_status[$mode] );

    }


    /**
     * Set learning state on current port
     *
     * @see PortInheritor::traitSetToPort()
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {

        // Get data
        $mode       = intval( $mode );
        $portsec    = (new PortSecurity())->getState();
        $learnstate = (new Port())->getLearningState();

        // Check port security state
        if( $mode == 2 && $portsec == 1 ) {
            trigger_error( "Learning must be enabled when Port Security is enabled", E_USER_WARNING );
            return false;
        }

        // Check if learning is already enabled, return true if enabled, if don't do it
        // snmpSetToPort will trigger error "SNMP::set(): Error in packet at 0 object_id: undoFailed" and return false,
        // error is triggered only when port security is enabled otherwise error is no triggered.
        if( $learnstate == 1 && $mode == 1 && $portsec == 1 ) {
            return true;
        }

        return $this->traitSetToPort( self::$device['snmp']['swL2PortControlLockState'], $mode );

    }

}
