<?php

namespace dautkom\netswitch\library\dlink\DGS312024SC;
use dautkom\netswitch\library\dlink\IpmbInheritor;


/**
 * @package dautkom\netswitch\library\dlink\DGS312024SC
 */
class Ipmb extends \dautkom\netswitch\library\dlink\Ipmb
{
    use IpmbInheritor;
}
