<?php

namespace dautkom\netswitch\library\dlink\DGS312024SC\B1;
use dautkom\netswitch\library\{
    Bandwidth,
    Fdb,
    Qos,
    Vlan,
    dlink\CableDiagnostics,
    dlink\SaveInheritor,
    dlink\Stp,
    dlink\DGS312024SC\Ipmb,
    dlink\DGS312024SC\PortSecurity
};


/**
 * @package dautkom\netswitch\library\dlink\DGS312024SC\B1
 */
class Common extends \dautkom\netswitch\library\dlink\DGS312024SC\Common
{

    /**
     * Trait utilizes saveSnmp() method
     *
     * @see SaveInheritor::saveSnmp()
     */
    use SaveInheritor;


    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }

}
