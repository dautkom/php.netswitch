<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * Uses \NetSwitch::$_vlan property, which stores current model Vlan object.
 * This self::$_vlan property is being registered upon initialization.
 *
 * @package dautkom\netswitch\library
 */
class Fdb extends NetSwitch
{

    /**
     * @var array
     */
    protected $fdb_filter = [
        '/SNMPv2-SMI::mib-2\.17\.7\.1\.3\.1\.1\.3\./',
        "/Hex:/",
        "/Hex-/",
        "/x:/",
        "/x-/",
        "/ /",
        "/Hex-STRING:/"
    ];


    /**
     * Retrieves all permanent MAC-addresses from FDB table via SNMP
     * Result array (
     *     int portNumber => array(
     *         array(0 => string MAC, 1 => string VID),
     *         ...
     *     )
     *     ...
     * )
     *
     * @return array
     */
    public function showAllPermanent(): array
    {

        // Get data
        // Suppresses warning "no such oid exists"
        // for the case when FDB table is empty.
        $snmp   = @$this->walk( self::$device['snmp']['dot1qStaticUnicastAllowedToGoTo'] );
        $result = [];

        if( !empty($snmp) && is_array( $snmp ) ) {

            array_walk( $snmp, function ( $value, $key ) use ( &$result ) {

                // Filter data
                $value = substr( preg_replace( $this->fdb_filter, '', $value ), 0, self::$device['vlan']['inHexLength'] );
                $key   = preg_replace( $this->fdb_filter, '', $key );

                // Convert hex mask to binary mask and define port number from binary mask
                $port = strpos( self::$_vlan->getBinMaskFromHex( $value ), '1' ) + 1;

                // Make an array of decimal octets
                $key = explode( '.', $key );

                // Get vid from key (first elemet in array)
                $vid = array_shift( $key );

                // Remove last element from array (0)
                array_pop( $key );

                // Convert to hex-representation
                $mac = '';
                array_walk( $key, function ($octet) use (&$mac) {
                    $mac .= str_pad( dechex( $octet ), 2, '0', STR_PAD_LEFT );
                });

                // Push all data to result array
                $result[$port][] = [$mac, $vid];

            });
        }

        ksort($result);
        return $result;

    }


    /**
     * Retrieves all MAC-addresses from specific port via PHP Telnet
     *
     * @see \dautkom\netswitch\library\Fdb::getFdbFromPhpTelnet()
     * @return array
     */
    public function showPortAll(): array
    {
        // Return data via telnet
        return $this->getFdbFromPhpTelnet( self::$port );
    }


    /**
     * Retrieve all permanent MAC-addresses from FDB table for current $port
     *
     * Return array {
     *          array {
     *              0 => string MAC, 1 => string VID
     *          }
     *      ...
     * )
     *
     * @return array
     */
    public function showPortPermanent(): array
    {
        // Retrieve port number
        $port = self::$port;

        // Retrieve Fdb table
        $fdb  = $this->showAllPermanent();

        // Return empty array if Fdb table is empty or array key not exists
        return (  empty( $fdb ) || !array_key_exists( $port, $fdb ) ) ?  [] : $fdb[$port];
    }


    /**
     * Add static (permanent) MAC address to FDB table on current port
     *
     * @param  string $mac MAC-address
     * @param  int    $vid VLAN ID
     * @return bool
     */
    public function addPermanent( string $mac, int $vid ): bool
    {

        // Filter and check data
        $port = self::$port;
        $vid  = ( self::$_vlan->validateVID( $vid ) ) ? $vid : null;
        $mac  = $this->filterMac( $mac );

        if( empty($mac) || $port === false || is_null( $vid ) ) {
            trigger_error( 'Wrong data specified', E_USER_WARNING );
            return false;
        }

        // Retrieve FDB table and check if mac already exists in fdb table
        $fdb = $this->showAllPermanent();
        foreach( $fdb as $checked_port => $entries ) {
            foreach( $entries as $entry ) {
                if ( in_array( $mac, $entry ) ) {
                    trigger_error( 'Duplicated FDB entry - MAC already exists on port ' . $checked_port, E_USER_WARNING );
                    return false;
                }
            }
        }

        // Convert data
        $port_mask = $this->getHexMaskFromPortNumber( $port );
        $mac       = $this->convertMacHexDec( $mac );

        // Check Vlan on port
        $vlans     = self::$_vlan->getAll();
        if( empty( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Vlan $vid not exist on this switch or error occurred!", E_USER_WARNING );
            return false;
        }

        if( $vlans[$vid]['egress']['bin']{ $port-1 } != 1 &&  $vlans[$vid]['untagged']['bin']{ $port-1 } != 1 ) {
            trigger_error("Port must be in $vid VLAN to add permanent MAC-address in it", E_USER_WARNING);
            return false;
        }

        // OID's
        $prepare_oid = self::$device['snmp']['dot1qStaticUnicastAllowedToGoTo'] . ".$vid.$mac";
        $writing_oid = self::$device['snmp']['dot1qStaticUnicastStatus'] . ".$vid.$mac";

        // Write MAC
        // Suppress oid not writable warning
        @$this->set($prepare_oid, 'x', $port_mask);

        // Write status
        return $this->set($writing_oid, 'i', 3);

    }


    /**
     * Delete unicast MAC-address from FDB table.
     *
     * CLI analogue: delete fdb from <vlan_name> <mac_addr>
     *
     * @param  string $mac MAC-address
     * @param  int    $vid VLAN ID
     * @return bool
     */
    public function deletePermanent( string $mac, int $vid ): bool
    {

        // Check and filter input data
        $mac  = $this->filterMac($mac);
        $vid  = ( self::$_vlan->validateVID( $vid ) ) ? $vid : null;
        $fdb  = $this->showAllPermanent();
        $res = [];

        if( empty($mac) || !$vid ) {
            trigger_error("Error input data for ".__METHOD__, E_USER_WARNING);
            return false;
        }

        // Transform FDB table to array with VID as key
        array_walk($fdb, function($value) use (&$res) {
            array_walk($value, function($value) use (&$res) {
                $res[$value[1]][] = $value[0];
            });
        });

        if( !array_key_exists($vid, $res) || !in_array($mac, $res[$vid]) ) {
            trigger_error("MAC address $mac for VLAN ID $vid does not exist in FDB table", E_USER_WARNING);
            return false;
        }

        // Prepare OID
        $oid = self::$device['snmp']['dot1qStaticUnicastStatus']. ".$vid." . $this->convertMacHexDec( $mac );

        // Sent query to switch
        return $this->set( $oid , 'i', 2 );

    }


    /**
     * Deletes all static FDB entries from current port
     *
     * @return bool
     */
    public function deleteAllPermanent(): bool
    {

        // Get data
        $fdb = $this->showPortPermanent();

        if( empty($fdb) ) {
            trigger_error("Don't have any MAC to delete on this port", E_USER_NOTICE);
            return false;
        }

        // Return false if deletePermanent() returns false
        foreach( $fdb as $entry ) {

            $delete = $this->deletePermanent( $entry[0], $entry[1] );

            if( $delete === false ) {
                trigger_error("Error deleting MAC {$entry[0]} in VLAN {$entry[1]}", E_USER_WARNING);
                return false;
            }

        }

        return true;

    }


    /**
     * Remove all invalid characters from mac-address and check if it is valid
     *
     * @param  string $mac hex-formatted MAC-address
     * @return string
     */
    public function filterMac( string $mac ): string
    {

        $mac = preg_replace('/[^A-F0-9]+/i', '', $mac);

        if( strlen( $mac ) != 12 ) {
            trigger_error( 'Wrong MAC address specified', E_USER_WARNING );
            return '';
        }

        return $mac;

    }


    /**
     * MAC-address converting from hexadecimal to decimal
     *
     * @param string $mac 12 symbols <b>valid</b> MAC-address without delimeters
     * @param bool $append_trailing_zero
     * @return string
     */
    public function convertMacHexDec( string $mac, $append_trailing_zero = true): string
    {

        $mac    = str_split( $mac, 2 );
        $result = '';

        // Converting mac from hexadecimal to decimal
        array_walk( $mac, function( $octet ) use ( &$result ) {
            $result .= hexdec( $octet ) . '.';
        });

        return substr($result, 0, -1) . ($append_trailing_zero ? '.0' : '');

    }


    /**
     * Create HEX mask for VLAN based on port number
     *
     * @param  int $port
     * @return string
     */
    protected function getHexMaskFromPortNumber( int $port ): string
    {

        // Retrieve data about device ports
        $port       = intval( $port );
        $max_ports  = count( self::$device['ports'] );

        // Return null if wrong port specified
        if( $port < 1 || $port > $max_ports ) {
            trigger_error( 'Wrong port specified', E_USER_WARNING );
            return '';
        }

        // Form binary mask
        $binary = '';
        for ( $i = 0; $i < $max_ports; $i++ ) {
            $binary.= ( $i == ( $port - 1 ) ) ? 1 : 0;
        }

        $binary = str_split( $binary, 4 );

        // Form hex mask
        $result = '';
        array_walk( $binary, function( $octet ) use ( &$result ) {
            $octet   = str_pad( $octet, 4, '0', STR_PAD_RIGHT );
            $result .= dechex( bindec( $octet ) );
        });

        // Return port hex mask
        return str_pad( $result, self::$device['vlan']['outHexLength'], '0', STR_PAD_RIGHT );

    }


    /**
     * Retrieves all MAC-addresses from specific port via PHP Telnet
     *
     * Return array {
     *          array {
     *               vid => string VID, vlan => string Vlan, state => string State, mac => strig MAC
     *          }
     *      ...
     * )
     *
     * @uses   Telnet
     * @param  int $port
     * @return array
     */
    protected function getFdbFromPhpTelnet( int $port ): array
    {

        // Telnet class initialization
        $telnet =  new Telnet();
        $result = [];

        // Try connect to switch via Telnet throw Exception if can't connect
        try {
            $telnet->setTimeout(2)->connect( self::$ip );
            $telnet->login(self::$telnet[0], self::$telnet[1]);
        }
        catch ( TelnetException $e ) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return $result;
        }

        // Send command and get data from switch
        $telnet->waitfor('/#/', true)->send( "show fdb port $port" );
        $fdb = $telnet->waitfor('/#/')->getData();

        // Parse data and push it to result array
        if( preg_match_all('/(\d+)\s+([\w-_]+)\s+([0-9a-f\-:]+)\s+(\d+)\s+(\w+)/i', $fdb, $matches) ) {

            // Rearrange array
            $count = count( $matches[0] );
            for ( $i = 0; $i < $count; $i++ ) {
                $result[] = [
                    'vid'   => $matches[1][$i],
                    'vlan'  => $matches[2][$i],
                    'state' => $matches[5][$i],
                    'mac'   => strtolower( preg_replace( '/[^a-f0-9]/i', '', $matches[3][$i] ) )
                ];
            }
        }

        // Return data array
        return $result;

    }

}
