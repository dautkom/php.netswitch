<?php

namespace dautkom\netswitch\library\zyxel;


/**
 * @package dautkom\netswitch\library\zyxel
 */
class Port extends \dautkom\netswitch\library\Port
{

    /**
     * Retrieves flow control state on current port
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getFlowControlState()
    {
        $snmp = $this->getFromPort( self::$device['snmp']['zyPortFlowControlState'] );
        return is_null($snmp) ? null : intval($snmp);
    }


    /**
     * Changes flow control state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setFlowControlState(int $mode)
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        // Write mode to port
        return $this->setToPort( self::$device['snmp']['zyPortFlowControlState'], 'i', $mode );

    }


    /**
     * Retrieves learning state ( enabled/disabled ) on current port
     *
     * @return int|null
     */
    public function getLearningState()
    {
        $snmp = $this->getFromPort( self::$device['snmp']['zyPortSecurityPortLearnState'] );
        return is_null($snmp) ? null : intval($snmp);
    }


    /**
     * Set learning state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setLearningState(int $mode)
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        // Write mode to port
        return $this->setToPort( self::$device['snmp']['zyPortSecurityPortLearnState'], 'i', $mode );

    }


    /**
     * Clears all statistic counters on current port
     *
     * @return bool|null
     */
    public function clearStats()
    {
        return $this->setToPort( self::$device['snmp']['zyPortCounterReset'], 'i', 1 );
    }


    /**
     * @inheritdoc
     */
    public function getMediumType( /** @noinspection PhpUnusedParameterInspection */ $port = null)
    {

        if( is_null($port) ) {
            $port = self::$port;
        }

        if( !$this->validatePortNumber( $port ) ) {
            trigger_error( 'Wrong port specified for ' . __METHOD__ . '()', E_USER_WARNING );
            return null;
        }

        if( self::$connected ) {

            $save = $this->getPortNumber();  // Save previous port number
            $this->setPortNumber($port);     // Set temporary new port

            /**
             * OID zyPortLinkUpType returns the following data:
             *  0: down
             *  1: copper
             *  2: fiber
             *  3: xfp
             *  4: cx4
             */
            $snmp = $this->getFromPort( self::$device['snmp']['zyPortLinkUpType'] );

            // Restore port number property
            if ( $this->validatePortNumber($save) ) {
                $this->setPortNumber($save);
            }

            // Cast returned raw value to unified data copper/sfp
            $type = round(log(intval($snmp), 2.6));

            if($type >= 0 && $type < 2) {
                return intval($type);
            }

        }

        return null;

    }

}
