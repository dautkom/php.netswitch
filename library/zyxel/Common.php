<?php

namespace dautkom\netswitch\library\zyxel;
use dautkom\netswitch\library\{
    Telnet, TelnetException
};


/**
 * @package dautkom\netswitch\library\zyxel
 */
class Common extends \dautkom\netswitch\library\Common
{

    /**
     * Retrieve firmware version from zyxel.products.enterpriseSolution.esMgmt.esSysInfo
     *
     * @return bool|string
     */
    public function getSoftwareRevision()
    {
        return $this->get('.1.3.6.1.4.1.890.1.15.3.1.6.0');
    }


    /**
     * Retrieve hardware version from zyxel.products.enterpriseSolution.esMgmt.esSysInfo
     *
     * If esSysInfo.sysHwVersionString returns error, method tries to query es.sysInfo.sysHwMajorVersion
     * and esSysInfo.sysHwMinorVersion, joining it with dot
     *
     * @return bool|string
     */
    public function getHardwareRevision()
    {

        $hwVersionString = @$this->get('.1.3.6.1.4.1.890.1.15.3.1.15.0');

        if( !$hwVersionString ) {

            $hwVersionString = $this->get(array('.1.3.6.1.4.1.890.1.15.3.1.13.0', '.1.3.6.1.4.1.890.1.15.3.1.14.0'));

            if(is_array($hwVersionString)) {
                $hwVersionString = implode('.', $hwVersionString);
            }

        }

        return $hwVersionString;

    }


    /**
     * Retrieve device serial number
     *
     * @return bool|string
     */
    public function getSerialNumber()
    {
        return $this->get('.1.3.6.1.4.1.890.1.15.3.82.2.10.0');
    }


    /**
     * Save configuration via SNMP
     *
     * @return bool
     */
    protected function saveSnmp()
    {

        // Switch with revision V4.30(AAND.0) fully saves after ~11 seconds, but revision V4.50(AAND.2) save ~ 30 seconds. Wait time increased to 40 seconds
        $wait = 40;

        // Write config(1)
        $this->set( self::$device['snmp']['zySysMgmtConfigSave'], 'i', 1 );

        // Wait half of estimated time
        sleep( ceil($wait/2) );

        // Check status in 1 second interval in the second half of estimated switch saving time
        for( $i = 0; $i < floor($wait/2); $i++ ) {
            // Suppress error "No response from switch"
            if( @$this->get( self::$device['snmp']['zySysMgmtLastActionStatus'] ) == 1 ) {
                return true;
            }
            sleep(1);
        }

        // Return false if switch not saved
        return false;

    }


    /**
     * Save configuration to running-config via telnet using PHP wrapper
     *
     * @uses   Telnet
     * @return bool|null
     */
    protected function savePhpTelnet()
    {

        /**
         * Telnet class initialization
         * @var $telnet \dautkom\netswitch\library\Telnet
         */
        $telnet =  new Telnet;

        // Try connect to switch via Telnet throw Exception if can't connect
        try {
            $telnet->setTimeout(3)->connect( self::$ip );
            $telnet->login( self::$telnet[0], self::$telnet[1] );
        } 
        catch ( TelnetException $e ) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return null;
        }

        // Send save command
        $telnet->waitfor('/#/', true)->send( "w m" );

        return true;

    }

}
