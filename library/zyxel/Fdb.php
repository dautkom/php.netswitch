<?php

namespace dautkom\netswitch\library\zyxel;
use dautkom\netswitch\library\{
    Telnet, TelnetException
};


/**
 * @package dautkom\netswitch\library\zyxel
 */
class Fdb extends \dautkom\netswitch\library\Fdb
{

    /**
     * This trait override method:
     * @see NetSwitch::walk()
     */
    use SnmpInheritor;
    
    /**
     * Add static (permanent) MAC address to FDB table on current port
     *
     * @param  string $mac MAC-address
     * @param  int    $vid VLAN ID
     * @return bool
     */
    public function addPermanent( string $mac, int $vid ): bool
    {

        // Filter and check data
        $port = self::$port;
        $vid  = ( self::$_vlan->validateVID( $vid ) ) ? $vid : null;
        $mac  = $this->filterMac( $mac );

        if( $port === false || is_null( $vid ) ) {
            trigger_error( 'Wrong data specified', E_USER_WARNING );
            return false;
        }

        // Retrieve FDB table and check if mac already exists in fdb table
        $fdb = $this->showAllPermanent();
        foreach( $fdb as $checked_port => $entries ) {
            foreach( $entries as $entry ) {
                if ( in_array( $mac, $entry ) ) {
                    trigger_error( "Duplicated FDB entry - MAC already exists on port $checked_port", E_USER_WARNING );
                    return false;
                }
            }
        }

        // Convert data
        $port_mask = $this->getHexMaskFromPortNumber( $port );
        $mac       = $this->convertMacHexDec( $mac );

        // Check Vlan on port
        $vlans     = self::$_vlan->getAll();
        if( empty( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Vlan $vid not exist on this switch or error occurred!", E_USER_WARNING );
            return false;
        }

        if( $vlans[$vid]['egress']['bin']{ $port-1 } != 1 &&  $vlans[$vid]['untagged']['bin']{ $port-1 } != 1 ) {
            trigger_error("Port must be in $vid VLAN to add permanent MAC-address in it", E_USER_WARNING);
            return false;
        }

        // OID's
        $prepare_oid = self::$device['snmp']['dot1qStaticUnicastAllowedToGoTo'] . ".$vid.$mac";
        $writing_oid = self::$device['snmp']['dot1qStaticUnicastStatus'] . ".$vid.$mac";

        // First set other(1) for creating & Inactivating entry
        // and only then set permanent(3) for activating the entry.
        $this->set($writing_oid, 'i', 1);

        // Write MAC
        // Suppress oid not writable warning
        @$this->set($prepare_oid, 'x', $port_mask);

        //Write status
        return $this->set($writing_oid, 'i', 3);

    }


    /**
     * Retrieves all MAC-addresses from specific port via PHP Telnet
     *
     * @uses   Telnet
     * @param  int $port
     * @return array
     */
    protected function getFdbFromPhpTelnet(int $port): array
    {

        /**
         * Telnet class initialization
         * @var $telnet \dautkom\netswitch\library\Telnet
         */
        $telnet =  new Telnet;
        $result = [];

        // Get Vlan  list
        $vlans  = self::$_vlan->getAll();

        // Try connect to switch via Telnet throw Exception if can't connect
        try {
            $telnet->setTimeout(2)->connect( self::$ip );
            $telnet->login( self::$telnet[0], self::$telnet[1] );
        } 
        catch ( TelnetException $e ) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return $result;
        }

        // Send command and get data from switch
        $telnet->waitfor('/#/', true)->send( "show mac address-table port $port" );
        $fdb = $telnet->waitfor('/#/')->getData();

        // Parse data and push it to result array
        if( preg_match_all('/(\d+)\s+(\d+)\s+([0-9a-f\-:]+)\s+(\w+)/i', $fdb, $matches) ) {

            // Rearrange array
            $count = count( $matches[0] );
            for ( $i = 0; $i < $count; $i++ ) {
                $result[] = [
                    'vid'   => $matches[2][$i],
                    'vlan'  => $vlans[$matches[2][$i]]['name'],
                    'state' => $matches[4][$i],
                    'mac'   => strtolower(preg_replace('/[^a-f0-9]/i', '', $matches[3][$i]))
                ];
            }
        }

        // Return data array
        return $result;

    }

}
