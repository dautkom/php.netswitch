<?php

namespace dautkom\netswitch\library\zyxel;
use dautkom\netswitch\library\{
   Telnet, TelnetException
};


/**
 * @package dautkom\netswitch\library\zyxel
 */
class CableDiagnostics extends \dautkom\netswitch\library\CableDiagnostics
{
    /**
     * @var array
     */
    private $result = [];

    /**
     * Start telnet cable diagnostics.
     * Before using any getters you should start diagnostics with this method.
     * Zyxel cable-diagnostics is not working on disabled ports!
     *
     * @param  int $cycles
     * @return bool
     */
    public function init(int $cycles = 10): bool
    {

        /**
         * Telnet class initialization
         * @var $telnet \dautkom\netswitch\library\Telnet
         */
        $telnet = new Telnet;

        try {
            $telnet->setTimeout(2)->connect( self::$ip );
            $telnet->login( self::$telnet[0], self::$telnet[1] );
        } 
        catch ( TelnetException $e ) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return $this->init = false;
        }

        /*
         * Send command and get data from switch
         */
        $telnet->waitfor('/#/', true)->send( "cable-diagnostics ".self::$port );
        $telnetAnswer = $telnet->waitfor('/#/')->getData();

        /*
         * Parse data
         */
        if( preg_match_all('/pair.\s+(\w+)\s+(\S+)/i', $telnetAnswer, $matches) ) {
            for( $i = 0; $i < 4; $i++ ) {
                $this->result[$i+1]['state']  = $matches[1][$i];
                $this->result[$i+1]['length'] = $matches[2][$i];
            }
        }
        else {
            return $this->init = false;
        }

        return $this->init = true;

    }

    /**
     * Retrieves pairs statuses, taking into account port type.
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * @return null|array
     */
    public function getState()
    {
        if( !$this->init ) {
            return null;
        }

        $states = [];

        /*
         * Possible values (OK, Open, Short, Unknown, Unsupported) to strval(int)
         */
        for( $i = 1; $i <= 4; $i++ ) {
            switch( strtolower( $this->result[$i]['state'] ) ) {
                case 'ok':
                    $states[$i] = '0';
                    break;
                case 'open':
                    $states[$i] = '1';
                    break;
                case 'short':
                    $states[$i] = '2';
                    break;
                case 'unknown':
                    $states[$i] = '5';
                    break;
                default:
                    $states[$i] = '8';
            }
        }

        return $states;
    }

    /**
     * Retrieve pairs length in meters.
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * @return null|array
     */
    public function getLength()
    {
        if( !$this->init ) {
            return null;
        }

        $lengths = [];

        /*
         * Possible returned values (strval(float), N/A, Unsupported) to strval(int)
         */
        for( $i = 1; $i <= 4; $i++ ) {
            switch( strtolower( $this->result[$i]['length'] ) ) {
                case 'n/a':
                    $lengths[$i] = '0';
                    break;
                case 'unsupported':
                    $lengths[$i] = '-1';
                    break;
                default:
                    $lengths[$i] = strval(intval($this->result[$i]['length']));
            }
        }

        return $lengths;
    }

}
