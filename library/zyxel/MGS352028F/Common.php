<?php

namespace dautkom\netswitch\library\zyxel\MGS352028F;
use dautkom\netswitch\library\{
    PortSecurity, 
    Qos, 
    zyxel\Bandwidth, 
    zyxel\CableDiagnostics, 
    zyxel\Fdb, 
    zyxel\Ipmb, 
    zyxel\Port, 
    zyxel\Stp, 
    zyxel\Vlan
};


/**
 * @package dautkom\netswitch\library\zyxel\MGS352028F
 */
class Common extends \dautkom\netswitch\library\zyxel\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }

    /**
    * Save configuration via SNMP
    * Method is overridden because switch is saving for excessively long time.
    *
    * @return bool
    */
    protected function saveSnmp()
    {

        // Write config(1)
        $this->set( self::$device['snmp']['zySysMgmtConfigSave'], 'i', 1 );

        // In hope that switch saved successfully
        return true;

    }

}
