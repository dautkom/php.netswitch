<?php

namespace dautkom\netswitch\library\zyxel\MES350024F;
use dautkom\netswitch\library\{
    PortSecurity, 
    Qos, 
    zyxel\Bandwidth, 
    zyxel\CableDiagnostics,
    zyxel\Fdb, 
    zyxel\Ipmb,
    zyxel\Stp,
    zyxel\Vlan
};


/**
 * @package dautkom\netswitch\library\zyxel\MES350024F
 */
class Common extends \dautkom\netswitch\library\zyxel\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }


    /**
     * Retrieve firmware version
     *
     * Firmware version are split in 4 parts, thats why 4 SNMP request are send
     *
     * @return bool|string
     */
    public function getSoftwareRevision()
    {

        // Oid's array
        $sw_oid = [
            '.1.3.6.1.4.1.890.1.5.8.57.1.1.0',
            '.1.3.6.1.4.1.890.1.5.8.57.1.2.0',
            '.1.3.6.1.4.1.890.1.5.8.57.1.3.0',
            '.1.3.6.1.4.1.890.1.5.8.57.1.4.0',
        ];

        // Get data from switch
        $sw = $this->get($sw_oid);

        // Check if data was received from the switch
        if (is_array($sw)) {
            $sw = array_values($sw);
            $sw = 'V' . $sw[0] . '.' . $sw[1] . '(' . $sw[2] . '.' . $sw[3] . ')';
        }

        return $sw;

    }


    /**
     * Retrieve hardware version
     *
     * @return bool|string
     */
    public function getHardwareRevision()
    {

        $hwVersion = $this->get(['.1.3.6.1.4.1.890.1.5.8.57.1.8.0', '.1.3.6.1.4.1.890.1.5.8.57.1.9.0']);

        if (is_array($hwVersion)) {
            $hwVersion = implode('.', $hwVersion);
        }

        return $hwVersion;

    }


    /**
     * Retrieve device serial number
     *
     * @return bool|string
     */
    public function getSerialNumber()
    {
        return $this->get('.1.3.6.1.4.1.890.1.5.8.57.1.10.0');
    }


    /**
     * Save configuration via SNMP
     *
     * @return bool
     */
    protected function saveSnmp()
    {

        // Switch fully saves after ~21 seconds, added 1 extra second
        $wait = 22;

        // Write config(1)
        $this->set( self::$device['snmp']['zySysMgmtConfigSave'], 'i', 1 );

        // Wait half of estimated time
        sleep( ceil($wait/2) );

        // Check status in 1 second interval in the second half of estimated switch saving time
        for( $i = 0; $i < floor($wait/2); $i++ ) {
            // Suppress error "No response from switch"
            if( @$this->get( self::$device['snmp']['zySysMgmtLastActionStatus'] ) == 1 ) {
                return true;
            }
            sleep(1);
        }

        // Return false if switch not saved
        return false;

    }

}
