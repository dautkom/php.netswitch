<?php

namespace dautkom\netswitch\library\zyxel\MES350024F;


/**
 * @package dautkom\netswitch\library\zyxel\MES350024F
 */
class Port extends \dautkom\netswitch\library\zyxel\Port
{

    /**
     * Retrieves flow control state on current port
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getFlowControlState()
    {
        $flow_control = $this->getFromPort( self::$device['snmp']['zyPortFlowControlState'] );

        return ( $flow_control == 0 ) ? 2 : 1;
    }


    /**
     * Changes flow control state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setFlowControlState(int $mode)
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        $mode = ( $mode == 2 ) ? 0 : 1;

        // Write mode to port
        return $this->setToPort( self::$device['snmp']['zyPortFlowControlState'], 'i', $mode );

    }


    /**
     * @inheritdoc
     */
    public function getMediumType( /** @noinspection PhpUnusedParameterInspection */ $port = null)
    {

        if( is_null($port) ) {
            $port = self::$port;
        }

        if( !$this->validatePortNumber( $port ) ) {
            trigger_error( 'Wrong port specified for ' . __METHOD__ . '()', E_USER_WARNING );
            return null;
        }

        if( self::$connected ) {

            $save = $this->getPortNumber();  // Save previous port number
            $this->setPortNumber($port);     // Set temporary new port

            /**
             * OID zyPortLinkUpType returns the following data:
             *  0: down
             *  1: copper
             *  2: fiber
             *  3: xfp
             *  4: cx4
             */
            $snmp1 = $this->getFromPort( self::$device['snmp']['zyPortLinkUpType'] );

            /**
             * @important
             * Bugfix: MES3500-24F returns copper(1) for 100mbit SFP interfaces
             */
            $snmp2 = $this->getFromPort( self::$device['snmp']['portOpModePortModuleType'] );
            $snmp2 = intval(!intval($snmp2)); # flip 0 and 1
            $snmp1 = $snmp1 + $snmp2;         # sum LinkUpType and ModuleType, normalizing/fixing the result

            // Restore port number property
            if ( $this->validatePortNumber($save) ) {
                $this->setPortNumber($save);
            }

            // Cast returned raw value to unified data copper/sfp
            $type = round(log(intval($snmp1), 2.6));

            if($type >= 0 && $type < 2) {
                return intval($type);
            }

        }

        return null;

    }

}
