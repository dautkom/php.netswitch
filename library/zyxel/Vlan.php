<?php

namespace dautkom\netswitch\library\zyxel;


/**
 * @package dautkom\netswitch\library\zyxel
 */
class Vlan extends \dautkom\netswitch\library\Vlan
{

    /**
     * Create vlan on device
     *
     * @param  int $vid
     * @param  string $name
     * @return bool|null
     */
    public function createVlan( $vid, $name )
    {

        // Get data
        $vid    = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans  = $this->getAll();

        // Filter name
        $name   = preg_replace( '/[^\w\-_]+/', '', $name );

        if( empty( $vlans ) || is_null( $vid ) ) {
            trigger_error( 'Error in params for ' . __METHOD__ . '()', E_USER_WARNING );
            return null;
        }

        foreach( $vlans as $v_id => $v_name ) {
            if( $v_name['name'] == $name || $v_id == $vid ) {
                trigger_error( "VLAN with such name or ID already exists", E_USER_WARNING);
                return null;
            }
        }

        // Array of oids + vid which will be set via snmpSet
        // On zyxel first add entry dot1qVlanStaticRowStatus createAndGo(4) and then set name dot1qVlanStaticName
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.5.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.1.$vid",
        ];

        // Create new vlan on switch
        return $this->set( $oids, ['i','s'], [4, $name] );

    }


    /**
     * Sets current port as non-member in a $vid VLAN
     *
     * @param  int $vid
     * @return bool|null
     */
    public function deleteVlanOnPort( $vid )
    {

        // Set default Pvid on port, by default zyxel don't do it
        parent::setGvrpPvid( 1 );

        // Enables port ingress
        $this->setPortIngress();

        //Delete vlan from port
        return parent::deleteVlanOnPort( $vid );

    }


    /**
     * Add $port on current port as untagged to a $vid VLAN
     *
     * @param  int $vid
     * @return bool|null
     */
    public function addUntaggedVlanOnPort( $vid )
    {

        // Set Pvid on port, by default zyxel don't do it
        parent::setGvrpPvid( $vid );

        // Enables port ingress
        $this->setPortIngress();

        // add untagged vlan
        return parent::addUntaggedVlanOnPort( $vid );

    }


    /**
     * Add current port as tagged (egress) to a $vid VLAN
     *
     * @param  int $vid
     * @return bool|null
     */
    public function addTaggedVlanOnPort( $vid )
    {

        // Set default Pvid on port, by default zyxel don't do it
        parent::setGvrpPvid( 1 );

        // Enables port ingress
        $this->setPortIngress();

        // add tagged vlan
        return parent::addTaggedVlanOnPort( $vid );

    }


    /**
     * Enable port ingress filtering on current port
     *
     * @see dautkom\netswitch\library\zyxel\Vlan::addUntaggedVlanOnPort()
     * @see dautkom\netswitch\library\zyxel\Vlan::addTaggedVlanOnPort()
     *
     * @return bool|null
     */
    protected function setPortIngress()
    {
        return $this->setToPort( '1.3.6.1.2.1.17.7.1.4.5.1.3', 'i', 1);
    }

}
