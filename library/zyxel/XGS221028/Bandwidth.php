<?php

namespace dautkom\netswitch\library\zyxel\XGS221028;


/**
 * @package dautkom\netswitch\library\zyxel\XGS221028
 */
class Bandwidth extends \dautkom\netswitch\library\zyxel\Bandwidth
{

    /**
     * @inheritdoc
     */
    public function setRxRate(int $value): bool
    {

        $value = abs( intval( $value ) );

        // Enable ingress state
        if( $this->getIngressState() === 2 && $value >= 1 ) {
            $this->setIngressState(1);
        }

        // Disable ingress state and set default ingress rate
        if( $value === 0 ) {
            $this->setIngressState(2);
            $value = 1;
        }

        // Validate input data
        if( $value < 1 ) {
            trigger_error( 'Rx must be >1 kbit/s', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['zyRateLimitPortPeakRate'], 'i', $value );

    }


    /**
     * @inheritdoc
     */
    public function setTxRate(int $value): bool
    {

        $value = abs( intval( $value ) );


        // Enable egress state
        if( $this->getEgressState() === 2 && $value >= 1 ) {
            $this->setEgressState(1);
        }

        // Disable egress state and set default egress rate
        if( $value === 0 ) {
            $this->setEgressState(2);
            $value = 1;
        }

        // Validate input data
        if( $value < 1 ) {
            trigger_error( 'Tx must be >1 kbit/s', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['zyRateLimitPortEgressRate'], 'i', $value );

    }


    /**
     * @inheritdoc
     */
    protected function getRate(string $oid, $state=null): int
    {
        $snmp = $this->getFromPort( $oid );

        // Return 0 if peak rate is 1 and peak state is disabled
        return ( $snmp == 1 && $state == 2 ) ? 0 : $snmp;
    }

}