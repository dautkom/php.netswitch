<?php

namespace dautkom\netswitch\library\zyxel\XGS221028;
use dautkom\netswitch\library\{
    PortSecurity, 
    Qos, 
    zyxel\CableDiagnostics,
    zyxel\Fdb, 
    zyxel\Ipmb, 
    zyxel\Port, 
    zyxel\Stp, 
    zyxel\Vlan
};


/**
 * @package dautkom\netswitch\library\zyxel\XGS221028
 */
class Common extends \dautkom\netswitch\library\zyxel\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;
        $this->Stp              = new Stp;

        parent::init();

    }

}
