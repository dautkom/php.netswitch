<?php

namespace dautkom\netswitch\library\zyxel;


/**
 * @package dautkom\netswitch\library\zyxel
 */
class Bandwidth extends \dautkom\netswitch\library\Bandwidth
{

    /**
     * Retrieves Rx value.
     * Rx is a traffic, which is received from the client side. For client it will be 'UPLOAD".
     * Value is returned in kbps.
     * Unlimited bandwidth is returned as int(0)
     *
     * @return int|mixed
     */
    public function getRxRate(): int
    {
        return $this->getRate( self::$device['snmp']['zyRateLimitPortPeakRate'], $this->getIngressState() );
    }


    /**
     * Set Rx bandwidth limit.
     * Argument must be in kbps.
     *
     * @param  int $value speed limit value
     * @return bool
     */
    public function setRxRate(int $value): bool
    {

        $value = abs( intval( $value ) );

        // Enable ingress state
        if( $this->getIngressState() === 2 && $value >= 64 ) {
            $this->setIngressState(1);
        }

        // Disable ingress state and set default ingress rate
        if( $value === 0 ) {
            $this->setIngressState(2);
            $value = 64;
        }

        // Validate input data
        if( $value < 64 ) {
            trigger_error( 'Rx must be >64 kbit/s', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['zyRateLimitPortPeakRate'], 'i', $value );

    }


    /**
     * Retrieves Tx value.
     * Tx is a traffic, which is received from the uplink side. For client it will be 'DOWNLOAD".
     * Value is returned in kbps.
     * Unlimited bandwidth is returned as int(0)
     *
     * @return int
     */
    public function getTxRate(): int
    {
        return $this->getRate( self::$device['snmp']['zyRateLimitPortEgressRate'], $this->getEgressState() );
    }


    /**
     * Set Tx bandwidth limit.
     * Argument must be in kbps.
     *
     * @param  int $value speed limit value
     * @return bool
     */
    public function setTxRate(int $value): bool
    {

        $value = abs( intval( $value ) );

        // Enable egress state
        if( $this->getEgressState() === 2 && $value >= 64 ) {
            $this->setEgressState(1);
        }

        // Disable egress state and set default egress rate
        if( $value === 0 ) {
            $this->setEgressState(2);
            $value = 64;
        }

        // Validate input data
        if( $value < 64 ) {
            trigger_error( 'Tx must be >64 kbit/s', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['zyRateLimitPortEgressRate'], 'i', $value );

    }


    /**
     * Get data from specific oid
     *
     * @param  string $oid
     * @param  int    $state
     * @return int
     */
    protected function getRate(string $oid, $state=null): int
    {
        $snmp = $this->getFromPort( $oid );

        // Return 0 if peak rate is 64 and peak state is disabled
        return ( $snmp == 64 && $state == 2 ) ? 0 : $snmp;
    }


    /**
     * Retrieves ingress (UPLOAD) peak rate limiting on the specified port.
     *
     * Return values:
     * 1: enabled
     * 2: disabled
     *
     * @return int
     */
    protected function getIngressState(): int
    {
        return intval( $this->getFromPort( self::$device['snmp']['zyRateLimitPortPeakState'] ) );
    }


    /**
     * Change ingress (UPLOAD) peak rate limiting on the specified port.
     *
     * Mode values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool
     */
    protected function setIngressState( $mode ): bool
    {

        $mode = intval($mode);

        if( $mode < 1 || $mode > 2 ) {
            trigger_error('Wrong parameter for '. __METHOD__ . '()', E_USER_WARNING);
            return false;
        }

        return $this->setToPort( self::$device['snmp']['zyRateLimitPortPeakState'], 'i', $mode );

    }


    /**
     * Retrieves egress (DOWNLOAD) peak rate limiting on the specified port.
     *
     * Return values:
     * 1: enabled
     * 2: disabled
     *
     * @return int
     */
    protected function getEgressState(): int
    {
        return intval( $this->getFromPort( self::$device['snmp']['zyRateLimitPortEgressState'] ) );
    }


    /**
     * Change egress (DOWNLOAD) peak rate limiting on the specified port.
     *
     * Mode values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool
     */
    protected function setEgressState( $mode ): bool
    {

        $mode = intval($mode);

        if( $mode < 1 || $mode > 2 ) {
            trigger_error('Wrong parameter for '. __METHOD__ . '()', E_USER_WARNING);
            return false;
        }

        return $this->setToPort( self::$device['snmp']['zyRateLimitPortEgressState'], 'i', $mode );

    }

}
