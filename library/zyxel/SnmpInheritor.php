<?php

namespace dautkom\netswitch\library\Zyxel;
use dautkom\netsnmp\NetSNMP;


/**
 * Trait for NetSwitch::walk() method
 * Intended to be used for Zyxel oid_increasing_check bug
 *
 * @traitUses NetSwitch
 * @package dautkom\netswitch\library\Zyxel
 */
trait SnmpInheritor
{

    /**
     * Wrapper for SNMP::walk()
     *
     * @param  string $oid  OID string
     * @param  bool   $real preserve suffix as array keys
     * @return array
     */
    protected function walk(string $oid, bool $real=false): array
    {

        /** @noinspection PhpUndefinedFieldInspection */
        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
            return null;
        }

        /** @noinspection PhpUndefinedFieldInspection */
        /** @var NetSNMP $snmp                        */
        $snmp = self::$snmp;
        
        $snmp->oid_increasing_check = false;
        $res = $snmp->walk($oid, $real);
        $snmp->oid_increasing_check = true;
        
        return $res;

    }

}
