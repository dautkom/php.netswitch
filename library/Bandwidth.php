<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
class Bandwidth extends NetSwitch
{

    /**
     * Retrieves Rx value.
     * Rx is a traffic, which is received from the client side. For client it will be 'UPLOAD".
     * Value is returned in kbps.
     * Unlimited bandwidth is returned as int(0)
     *
     * @return int
     */
    public function getRxRate(): int
    {
       return $this->getRate( self::$device['snmp']['BandwidthRxRate'] );
    }


    /**
     * Set Rx bandwidth limit.
     * Argument must be in kbps.
     *
     * @param  int $value speed limit value
     * @return bool
     */
    public function setRxRate(int $value): bool
    {
        return $this->setRate( self::$device['snmp']['BandwidthRxRate'], $value, 'Rx' );
    }


    /**
     * Retrieves Tx value.
     * Tx is a traffic, which is received from the uplink side. For client it will be 'DOWNLOAD".
     * Value is returned in kbps.
     * Unlimited bandwidth is returned as int(0)
     *
     * @return int
     */
    public function getTxRate(): int
    {
        return $this->getRate( self::$device['snmp']['BandwidthTxRate'] );
    }


    /**
     * Set Tx bandwidth limit.
     * Argument must be in kbps.
     *
     * @param  int $value speed limit value
     * @return bool
     */
    public function setTxRate(int $value): bool
    {
        return $this->setRate( self::$device['snmp']['BandwidthTxRate'], $value, 'Tx' );
    }


    /**
     * Get data from specific OID
     *
     * @param  string   $oid   SNMP object identifier
     * @param  int|null $state egress state for zyxels, left for inheritance compatibility
     * @return int
     */
    protected function getRate(string $oid, $state=null): int
    {
        $snmp = $this->getFromPort( $oid );
        return ( $snmp == 1024000 ) ? 0 : intval( $snmp );
    }


    /**
     * Set data to specific OID with specific mode
     *
     * @param  string $oid    SNMP object identifier
     * @param  int    $value  speed limit value
     * @param  string $type   Rx or Tx
     * @return bool
     */
    protected function setRate( string $oid, int $value, string $type = ''): bool
    {

        $value = abs( intval( $value ) );

        // Set max bandwidth if value equals 0
        $value = ( $value === 0 ) ? 1024000 : $value;

        if( $value < 64 ) {
            trigger_error( $type . ' must be >64 kbit/s', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( $oid, 'i', $value );

    }

}
