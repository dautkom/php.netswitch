<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
abstract class CableDiagnostics extends NetSwitch
{

    /**
     * Initialization flag
     * @var bool
     */
    protected $init = false;


    /**
     * Start cable diagnostics.
     * Before using any getters you should start diagnostics with this method.
     *
     * @param  int $cycles
     * @return bool
     */
    abstract public function init(int $cycles = 10): bool;


    /**
     * Retrieves pairs statuses, taking into account port type.
     * Returns array of 4 elements(pairs)
     *
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * Pair status values:
     * 0: ok
     * 1: open
     * 2: short
     * 3: open-short
     * 4: crosstalk
     * 5: unknown
     * 6: count
     * 7: no-cable
     * 8: other
     *
     * @return null|array
     */
    abstract public function getState();


    /**
     * Retrieve pairs length in meters.
     * Returns array of 4 elements(pairs)
     *
     * No cable/pair results a -1 in response.
     *
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * @return null|array
     */
    abstract public function getLength();

}
