<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
class Qos extends NetSwitch
{

    /**
     * Retrieve port qos value.
     *
     * @return mixed
     */
    public function getQos()
    {
        return $this->getFromPort( self::$device['snmp']['dot1dPortDefaultUserPriority'] );
    }


    /**
     * Set port qos value.
     *
     * @param int $value
     * @return bool
     */
    public function setQos(int $value): bool 
    {
        $value = intval( $value );

        if( $value < 0 || $value > 7 ) {
            trigger_error( 'Qos value must be 0-7', E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['dot1dPortDefaultUserPriority'], 'i', $value );
    }

}

