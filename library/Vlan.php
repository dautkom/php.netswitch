<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
class Vlan extends NetSwitch
{

    /**
     * Should 'default' VLAN to be ignored while working with VLANs.
     * It's recommended to leave this option enabled
     *
     * @var bool
     */
    protected $ignore_default_vlan = true;


    /**
     * Set TRUE if you want to add multiple untagged VLANs on one port.
     * Multiple untagged VLANs on one port are supported only if there's 802.1v support
     *
     * @var bool
     */
    protected $allow_untagged_overlap = false;


    /**
     * @var array
     */
    private $vlan_filter = array(
        "/Hex:/",
        "/Hex-/",
        "/x:/",
        "/x-/",
        "/ /",
        "/\\./",
        "/Hex-STRING:/"
    );


    /**
     * Validates VLAN ID
     *
     * @param  int $vid
     * @return bool
     */
    public function validateVID(int $vid): bool
    {
        $vid = intval($vid);
        return ( $vid < 1 || $vid > 4095 ) ? false : true;
    }


    /**
     * Retrieve all VLANs from the device
     *
     * RESULT FORMAT:
     * array
     * (
     *     integer [$vid] => array
     *     (
     *          ['name'] => string
     *          ['egress'] => array
     *          (
     *              ['hex'] => string
     *              ['bin'] => string
     *          )
     *          ['untagged'] => array
     *          (
     *              ['hex'] => string
     *              ['bin'] => string
     *          )
     *     )
     *     ....
     * )
     *
     * @return array
     */
    public function getAll()
    {

        $result   = array();
        $raw_data = array(
            'name'     => $this->walk('.1.3.6.1.2.1.17.7.1.4.3.1.1'),
            'egress'   => $this->walk('.1.3.6.1.2.1.17.7.1.4.2.1.4'),
            'untagged' => $this->walk('.1.3.6.1.2.1.17.7.1.4.2.1.5')
        );

        // Processing result
        array_walk($raw_data, function($value, $key) use (&$result) {

            if( !is_array($value) ) {
                trigger_error("Unable to retrieve $key in ".__METHOD__, E_USER_ERROR);
            }

            // Current iteration storage variable
            // Stores temporary data for 'name', 'egress' and 'untagged' array_walk
            $sub = array();

            // Processing 'name', 'egress' and 'untagged'
            array_walk($value, function($data, $vid) use ($key, &$sub) {
                $vid             = intval(substr($vid, strrpos($vid, '.') + 1));            // cut VLAN ID from OID
                $sub[$vid][$key] = ($key != 'name') ? $this->parseVlanMask($data) : $data;  // parse data if it's not a VLAN name
            });

            // Merge interstitial result to main resultset
            $result = array_replace_recursive($result, $sub);

        });

        return $result;

    }


    /**
     * Create vlan on device
     *
     * @param  int $vid
     * @param  string $name
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function createVlan( $vid, $name )
    {

        // Get data
        $vid    = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans  = $this->getAll();

        // Filter name
        $name   = preg_replace( '/[^\w\-_]+/', '', $name );

        if( empty( $vlans ) || is_null( $vid ) ) {
            trigger_error( 'Error in params for ' . __METHOD__ . '()', E_USER_WARNING );
            return null;
        }

        foreach( $vlans as $v_id => $v_name ) {
            if( $v_name['name'] == $name || $v_id == $vid ) {
                trigger_error( "VLAN with such name or ID already exists", E_USER_WARNING);
                return null;
            }
        }

        // Array of oids + vid which will be set via snmpSet
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.1.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.5.$vid",
        ];

        // Create new vlan on switch
        return $this->set( $oids, ['s','i'], [$name, 4] );

    }


    /**
     * Delete vlan from device
     *
     * @param  int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function deleteVlan( $vid )
    {

        // Get data
        $vid    = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans  = $this->getAll();

        if( is_null( $vid ) ) {
            trigger_error( 'Wrong VLAN ID specified', E_USER_WARNING );
            return null;
        }

        if( empty( $vlans ) || !is_array( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING );
            return null;
        }

        // Deleting vlan from device
        return $this->set( ".1.3.6.1.2.1.17.7.1.4.3.1.5.$vid", 'i', 6 );

    }


    /**
     * Sets current port as non-member in a $vid VLAN
     *
     * @param  int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function deleteVlanOnPort( $vid )
    {

        // Get data
        $port   = self::$port;
        $vid    = ( $this->validateVID( $vid )) ? $vid : null;
        $vlans  = $this->getAll();
        $fdb    = self::$_fdb->showPortPermanent();

        // Check VID
        if( is_null( $vid ) ) {
            trigger_error( 'Wrong VLAN ID specified', E_USER_WARNING );
            return null;
        }

        // Check Vlan's
        if( empty( $vlans ) || !is_array( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING );
            return null;
        }

        // Check FDB table
        if( !empty( $fdb ) ) {
            trigger_error( 'Remove all static MAC addresses from VLAN before deleting it', E_USER_WARNING );
            return null;
        }

        $vlans[$vid]['egress']['bin']{ $port-1 }   = 0;
        $vlans[$vid]['untagged']['bin']{ $port-1 } = 0;

        $mask_egress   = $this->getHexMaskFromBin( $vlans[$vid]['egress']['bin'] );
        $mask_untagged = $this->getHexMaskFromBin( $vlans[$vid]['untagged']['bin'] );

        // Array of oids + vid which will be set via snmpSet
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid"
        ];

        // Delete Vlan on port
        return $this->set( $oids, 'x', [$mask_untagged, $mask_egress] );

    }


    /**
     * Marks port as non-member on all VLANs on the device
     *
     * @return bool|null
     */
    public function clearVlansOnPort()
    {

        // Get data
        $vlans  = $this->getAll();

        // Check Vlan's
        if( empty( $vlans ) || !is_array( $vlans ) ) {
            trigger_error( "Can't retrieve VLANs", E_USER_WARNING );
            return null;
        }

        // Default value
        $delete = null;

        // Loop through VLAN's
        foreach( $vlans as $v_id => $v_name ) {
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if( $this->ignore_default_vlan ) {
                // Default vlan id
                if ( $v_id != 1 ) {
                    $delete = $this->deleteVlanOnPort( $v_id );
                }
            } else {
                $delete = $this->deleteVlanOnPort( $v_id );
            }

            if( $delete === false ) {
                trigger_error( "Error occurred while deleting VLAN with ID '$v_id' ( {$v_name['name']} ) from port", E_USER_WARNING );
                return false;
            }
        }

        return true;

    }


    /**
     * Add $port on current port as untagged to a $vid VLAN
     *
     * @param  int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function addUntaggedVlanOnPort( $vid )
    {

        // Get data
        $port  = self::$port;
        $vid   = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans = $this->getAll();

        // Check vid
        if( is_null( $vid ) ) {
            trigger_error( 'Wrong VLAN ID specified', E_USER_WARNING );
            return null;
        }

        // Check VLANs
        if( empty( $vlans ) || !is_array( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING );
            return null;
        }

        // Prevent untagged ports overlapping
        if( $this->allow_untagged_overlap === false ) {
            foreach( $vlans as $v_id => $vlan ) {
                /**
                 * Ignore default vlan?
                 * @see Vlan::ignore_default_vlan
                 */
                if( $this->ignore_default_vlan ) {
                    if($v_id == 1) continue; /* skip default vlan */
                }

                if( $vlan['untagged']['bin']{ $port-1 } == 1 ) {
                    trigger_error( "Untagged ports overlapped in VLAN with ID '$v_id' ( {$vlan['name']} )", E_USER_WARNING );
                    return null;
                }
            }
        }

        $vlans[$vid]['egress']['bin']{ $port-1 }     = 1;
        $vlans[$vid]['untagged']['bin']{ $port-1 }   = 1;

        $mask_egress     = $this->getHexMaskFromBin( $vlans[$vid]['egress']['bin'] );
        $mask_untagged   = $this->getHexMaskFromBin( $vlans[$vid]['untagged']['bin'] );

        // Array of oids + vid which will be set via snmpSet
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid"
        ];

        // Make port untagged
        return $this->set( $oids, 'x', [$mask_egress, $mask_untagged] );

    }


    /**
     * Add current port as tagged (egress) to a $vid VLAN
     *
     * @param  int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function addTaggedVlanOnPort( $vid )
    {

        // Get data
        $port  = self::$port;
        $vid   = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans = $this->getAll();

        // Check vid
        if( is_null( $vid ) ) {
            trigger_error( 'Wrong VLAN ID specified', E_USER_WARNING );
            return null;
        }

        // Check Vlan's
        if( empty( $vlans ) || !is_array( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING );
            return null;
        }

        // Check if port is transit
        if( $this->isTransit() ) {
            trigger_error( "Port $port is set up as transit", E_USER_WARNING );
            return null;
        }

        // Check if port is untagged
        foreach ($vlans as $v_id => $vlan) {
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if( $this->ignore_default_vlan ) {
                if($v_id == 1) continue; /* skip default vlan */
            }

            if( $vlan['untagged']['bin']{ $port-1 } == 1 ) {
                trigger_error( "Port $port is marked as untagged in VLAN '$v_id' ( {$vlan['name']} )", E_USER_WARNING );
                return null;
            }
        }

        $vlans[$vid]['egress']['bin']{ $port-1 }    = 1;

        $mask_egress     = $this->getHexMaskFromBin( $vlans[$vid]['egress']['bin'] );
        $mask_untagged   = $this->getHexMaskFromBin( $vlans[$vid]['untagged']['bin'] );

        // Array of oids + vid which will be set via snmpSet
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid"
        ];

        // Make port tagged
        return $this->set( $oids, 'x', [$mask_untagged, $mask_egress] );

    }


    /**
     * Check if port is transit.
     * Port is considered as transit, when it's tagged in all VLANs on device.
     *
     * @return bool|null
     */
    public function isTransit()
    {

        // Get data
        $port   = self::$port;
        $vlans  = $this->getAll();

        // Check Vlan's
        if( empty( $vlans ) || !is_array( $vlans ) ) {
            trigger_error( "Can't retrieve VLANs from switch", E_USER_WARNING );
            return null;
        }

        foreach( $vlans as $v_id => $vlan ) {
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if( $this->ignore_default_vlan ) {
                // Default vlan id
                if( $v_id != 1 ) {
                    if( $vlan['egress']['bin']{ $port-1 } == 0 || ( $vlan['egress']['bin']{ $port-1 } == 1 && $vlan['untagged']['bin']{ $port-1 } != 0 ) ) {
                        return false;
                    }
                } else {
                    // Return false if there only default VLAN on a switch
                    if( count( $vlans ) == 1 ) {
                        return false;
                    }
                }
            } else {
                if( $vlan['egress']['bin']{ $port-1 } == 0 || ( $vlan['egress']['bin']{ $port-1 } == 1 && $vlan['untagged']['bin']{ $port-1 } != 0 ) ) {
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Checks whether $port (or current port) is marked as tagged in any VLAN
     *
     * @return bool|null
     */
    public function isTagged()
    {

        // Get data
        $port  = self::$port;
        $vlans = $this->getAll();

        // Check VLANs
        if ( empty( $vlans ) || !is_array( $vlans ) ) {
            trigger_error( "Can't retrieve VLANs from switch",  E_USER_WARNING );
            return null;
        }

        // Check if current port is tagged
        foreach( $vlans as $vid => $vlan ){
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if( $this->ignore_default_vlan ) {
                if($vid == 1) continue; /* skip default vlan */
            }

            if ($vlan['egress']['bin']{$port - 1} == 1 && $vlan['untagged']['bin']{$port - 1} == 0) {
                return true;
            }
        }

        return false;

    }


    /**
     * Set all VLANs as tagged on specified port.
     * Port must be clear from permanent MAC-addresses or there an error will be triggered
     *
     * Be sure to use 'GVRP PVID' to be configured on transit port
     * if it's required on the certain switch
     *
     * @return bool|null
     */
    public function setTransit()
    {

        // Get data
        $port   = self::$port;
        $vlans  = $this->getAll();

        // Check Vlan's
        if( empty( $vlans ) || !is_array( $vlans ) ) {
            trigger_error( "Can't retrieve VLANs from switch", E_USER_WARNING );
            return null;
        }

        // Check if port is already transit
        if( $this->isTransit() ) {
            trigger_error( "Port $port is already set as transit", E_USER_WARNING );
            return null;
        }

        // Check if port presents in any VLAN
        foreach( $vlans as $v_id => $vlan ) {
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if( $this->ignore_default_vlan ) {
                if($v_id == 1) continue; /* skip default vlan */
            }

            if( $vlan['egress']['bin']{ $port-1 } != 0 || $vlan['untagged']['bin']{ $port-1 } != 0 ) {
                trigger_error( "Remove $port port from VLAN '$v_id' ( {$vlan['name']} )", E_USER_WARNING );
                return null;
            }
        }

        // Default value
        $add = null;

        // Add port as tagged to every VLAN
        foreach( $vlans as $v_id => $vlan ) {
            /**
             * Ignore default vlan?
             * @see Vlan::ignore_default_vlan
             */
            if( $this->ignore_default_vlan ) {
                // Default vlan id
                if ( $v_id != 1 ) {
                    $add = $this->addTaggedVlanOnPort( $v_id );
                }
            } else {
                $add = $this->addTaggedVlanOnPort( $v_id );
            }

            if( $add === false ) {
                trigger_error( "Error occurred while setting up port as transit on VLAN with ID '$v_id' ( {$vlan['name']} )", E_USER_WARNING );
                return false;
            }
        }

        return true;

    }


    /**
     * Configures GVRP PVID of current port.
     * To reset setting to default value, use setGvrpPvid(1).
     *
     * @param  int $vid
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function setGvrpPvid( $vid )
    {

        // Get data
        $vid   = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans = $this->getAll();

        // Check vid
        if( is_null( $vid ) ) {
            trigger_error( 'Wrong VLAN ID specified', E_USER_WARNING );
            return null;
        }

        // Check Vlan's
        if( empty( $vlans ) || !is_array( $vlans ) || !array_key_exists( $vid, $vlans ) ) {
            trigger_error( "Can't retrieve VLANs or VLAN with ID \"$vid\" not found", E_USER_WARNING );
            return null;
        }

        return $this->setToPort( '1.3.6.1.2.1.17.7.1.4.5.1.1', 'u', $vid );

    }


    /**
     * Changes $vid VLAN setting by two masks - egress and untagged.
     * Both masks must be in hex format.
     *
     * @param $vid int
     * @param $mask_egress string Hex-string
     * @param $mask_untagged string Hex-string
     * @return bool|null
     *
     * @noinspection PhpMissingParamTypeInspection, PhpUnused
     */
    public function setVlan( $vid, $mask_egress, $mask_untagged )
    {

        // Get data
        $vid   = ( $this->validateVID( $vid ) ) ? $vid : null;
        $vlans = $this->getAll();

        // Check VLANs
        if( empty( $vlans ) ) {
            trigger_error("Couldn't retrieve VLANs from device", E_USER_WARNING);
            return null;
        }
        elseif( !array_key_exists( $vid, $vlans ) ) {
            trigger_error("Specified VLAN ID was not found on device", E_USER_WARNING);
            return null;
        }

        // Check hex masks
        if( strlen( $mask_egress ) != self::$device['vlan']['outHexLength'] || strlen( $mask_untagged ) != self::$device['vlan']['outHexLength']  ) {
            trigger_error("Mask error", E_USER_WARNING);
            return null;
        }

        // Prevent untagged ports overlapping
        if( $this->allow_untagged_overlap === false ) {

            $binary_mask   = substr( $mask_untagged, 0, self::$device['vlan']['inHexLength'] );
            $binary_mask   = self::$_vlan->getBinMaskFromHex( $binary_mask );
            $binary_mask   = gmp_init($binary_mask, 2);

            foreach($vlans as $vlan_id => $vlan) {
                /**
                 * Ignore default vlan?
                 * @see Vlan::ignore_default_vlan
                 */
                if( $this->ignore_default_vlan ) {
                    if($vlan_id == 1) continue; /* skip default vlan */
                }

                if( $vlan_id != $vid ) {

                    $untagged = null;
                    $untagged = gmp_init( $vlan['untagged']['bin'], 2 );
                    $check    = gmp_and( $binary_mask, $untagged );
                    $check    = gmp_strval( $check, 2 );

                    if( $check != '0' && stripos($check, '1') !== false ) {
                        trigger_error("Untagged ports overlapped in VLAN with ID $vlan_id ({$vlan['name']})", E_USER_WARNING);
                        return null;
                    }

                }

            }

        }

        // Array of oids + vid which will be set via snmpSet
        $oids = [
            ".1.3.6.1.2.1.17.7.1.4.3.1.2.$vid",
            ".1.3.6.1.2.1.17.7.1.4.3.1.4.$vid"
        ];

        return $this->set( $oids, 'x', [$mask_egress, $mask_untagged] );

    }


    /**
     * Convert hexadecimal mask to binary
     * Mask length matches the [vlan][binLength] setting from model.ini
     *
     * Method is made public to allow cross-object usage
     *
     * @param  string $hex
     * @return string
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function getBinMaskFromHex($hex)
    {
        $bin = base_convert($hex, 16, 2);
        return str_pad($bin, self::$device['vlan']['binLength'], "0", STR_PAD_LEFT);
    }


    /**
     * Convert VLAN binary mask to hex mask.
     *
     * Method is made public to allow cross-object usage
     *
     * @param  int $bin
     * @return string
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function getHexMaskFromBin( $bin )
    {

        // Split bin mask to octets
        $bin = str_split( $bin, 8 );

        // Form hex mask
        $result = '';
        array_walk( $bin, function( $octet ) use ( &$result ) {
            $result .= str_pad( dechex( bindec( $octet ) ), 2, "0", STR_PAD_LEFT );
        });

        // Return hex mask
        return str_pad( $result, self::$device['vlan']['outHexLength'], '0', STR_PAD_RIGHT );

    }


    /**
     * Convert raw value retrieved via SNMP to a hexadecimal mask
     * Mask length is cut to the [vlan][inHexLength] setting from model ini
     *
     * Method is made public to allow cross-object usage
     *
     * Has interception of SNMP bug, when value is interpreted as if
     * hex segments where passed through chr() function - weird characters
     * are returned instead of regular hexadecimal
     *
     * @param  string $raw
     * @return string
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function getHexMaskFromRaw($raw)
    {

        $tmp = '';
        $hex = preg_replace($this->vlan_filter, '', $raw);
        $hex = preg_replace('/[^0-9A-F]+/i', '', $hex);
        $hex = substr($hex, 0, self::$device['vlan']['inHexLength']);

        // Buggy behavior workaround in case if hex values
        // in result are interpreted as if they passed through chr()
        if( strlen($hex) < self::$device['vlan']['inHexLength'] ) {

            for( $i=0; $i<strlen($raw); $i++ ) {
                $tmp.= str_pad( dechex( ord( $raw{$i} ) ), 2, "0", STR_PAD_LEFT);
            }

            $hex = $tmp;

        }

        return $hex;

    }


    /**
     * Method to for raw result parsing
     *
     * @param  string $raw
     * @return array
     * @see Vlan::getAll()
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    private function parseVlanMask($raw)
    {

        $hex = $this->getHexMaskFromRaw($raw);

        return array(
            'raw' => $raw,
            'hex' => $hex,
            'bin' => $this->getBinMaskFromHex($hex),
        );

    }

}
