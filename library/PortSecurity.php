<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
class PortSecurity extends NetSwitch
{

    /**
     * Retrieve port security admin state.
     * Return NULL if snmp response is empty or switch returned state other (1)
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    public function getState()
    {
        $snmp = $this->getFromPort(self::$device['snmp']['zyPortSecurityPortState']);
        return is_null($snmp) ? null : intval($snmp);
    }


    /**
     * Changes port security admin state.
     *
     * Mode argument values:
     * 1 : enabled
     * 2 : disabled
     *
     * @param  int $mode
     * @return bool
     */
    public function setState(int $mode): bool
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        // Write mode to port
        return $this->setToPort( self::$device['snmp']['zyPortSecurityPortState'], 'i', $mode );

    }


    /**
     * Retrieve maximum amount of learned addresses on port
     *
     * @return int|null
     */
    public function getMaxAddresses()
    {
        //Get data
        $snmp = $this->getFromPort( self::$device['snmp']['PortSecurityMaxLernAddr'] );
        return ( $snmp !== false ? intval( $snmp ) : null );
    }


    /**
     * Changes amount of learned addresses on port.
     * Will trigger error and return null if trying to set max learned address < 0 or > 64
     *
     * @param  int $value
     * @return bool
     */
    public function setMaxAddresses(int $value): bool
    {

        $value = intval( $value );

        if( $value < 0 || $value > 64) {
            trigger_error( 'Invalid amount of max learned addresses. Must be between 0 and 64 inclusive.',  E_USER_WARNING );
            return false;
        }

        return $this->setToPort( self::$device['snmp']['PortSecurityMaxLernAddr'], 'i', $value );

    }


    /**
     * Retrieves port security mode.
     *
     * Return values:
     *  0 : error
     *  1 : other
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @return int|null
     */
    public function getMode()
    {
        return null;
    }


    /**
     * Changes port security mode.
     * Return null if wrong parameter specified
     *
     * Mode value list:
     *  2 : permanent
     *  3 : deleteOnTimeout
     *  4 : deleteOnReset
     *
     * @param  int $mode
     * @return bool|null
     */
    public function setMode( /** @noinspection PhpUnusedParameterInspection */ $mode )
    {
        return null;
    }

}
