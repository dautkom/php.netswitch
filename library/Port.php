<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
abstract class Port extends NetSwitch
{

    /**
     * Retrieves flow control state on current port
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return int|null
     */
    abstract public function getFlowControlState();


    /**
     * Changes flow control state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    abstract public function setFlowControlState(int $mode);


    /**
     * Retrieves learning state ( enabled/disabled ) on current port
     *
     * @return int|null
     */
    abstract public function getLearningState();


    /**
     * Set learning state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return bool|null
     */
    abstract public function setLearningState(int $mode);


    /**
     * Clears all statistic counters on all ports
     *
     * @return bool|null
     */
    abstract public function clearStats();


    /**
     * Set current port number and save it in Core::$port property
     *
     * @throws \RuntimeException
     * @param  int $port Port number
     * @return bool
     */
    public function setPortNumber(int $port): bool
    {

        $port = intval($port);

        if( !$this->validatePortNumber($port) ) {
            throw new \RuntimeException("Wrong port specified");
        }

        parent::$port = $port;
        return true;

    }


    /**
     * Retrieve current port number from NetSwitch::$port property
     *
     * @return int|null
     */
    public function getPortNumber()
    {
        return parent::$port;
    }


    /**
     * Retrieves amount of sent/recieved packets on port.
     * Triggers warning and returns 'false', if port number was not set.
     *
     * @return array
     */
    public function getDataFlow(): array
    {

        // Excessive check to avoid multiple warnings from snmpGetFromPort
        // in case if port number was not set
        if( !self::$port ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return [];
        }

        $data = $this->getFromPort([
            '1.3.6.1.2.1.2.2.1.11', '1.3.6.1.2.1.2.2.1.12',
            '1.3.6.1.2.1.2.2.1.17', '1.3.6.1.2.1.2.2.1.18'
        ]);

        if(!$data) {
            return [];
        }

        return [
            'in' => [
                'unicast'    => $data['1.3.6.1.2.1.2.2.1.11.'.parent::$port],
                'nonunicast' => $data['1.3.6.1.2.1.2.2.1.12.'.parent::$port]
            ],
            'out' => [
                'unicast'    => $data['1.3.6.1.2.1.2.2.1.17.'.parent::$port],
                'nonunicast' => $data['1.3.6.1.2.1.2.2.1.18.'.parent::$port]
            ],
        ];

    }


    /**
     * Retrieves amount of errors/discards on port.
     * Triggers warning and returns 'false', if port number was not set.
     *
     * @return array
     */
    public function getErrors(): array
    {

        // Excessive check to avoid multiple warnings from snmpGetFromPort
        // in case if port number was not set
        if( !self::$port ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return [];
        }

        $data = $this->getFromPort([
            '1.3.6.1.2.1.2.2.1.14', '1.3.6.1.2.1.2.2.1.13',
            '1.3.6.1.2.1.2.2.1.20', '1.3.6.1.2.1.2.2.1.19'
        ]);

        if(!$data) {
            return [];
        }

        return [
            'in' => [
                'errors'   => $data['1.3.6.1.2.1.2.2.1.14.'.parent::$port],
                'discards' => $data['1.3.6.1.2.1.2.2.1.13.'.parent::$port]
            ],
            'out' => [
                'errors'   => $data['1.3.6.1.2.1.2.2.1.20.'.parent::$port],
                'discards' => $data['1.3.6.1.2.1.2.2.1.19.'.parent::$port]
            ],
        ];

    }


    /**
     * The current operational state of the interface.
     *
     * Value list:
     *  1: up
     *  2: down
     *  3: testing
     *  4: unknown
     *  5: dormant
     *  6: notPresent
     *  7: lowerLayerDown
     *
     * @return int
     */
    public function getOperatingStatus(): int
    {
        $raw = $this->getFromPort('1.3.6.1.2.1.2.2.1.8');
        $res = preg_replace('/[^\d]/', '', $raw);
        return intval($res);
    }


    /**
     * An estimate of the interface's current bandwidth in bits per second.
     *
     * @return mixed
     */
    public function getOperatingSpeed()
    {
        return $this->getFromPort('1.3.6.1.2.1.2.2.1.5');
    }


    /**
     * Retrieve operating duplex status via dot3StatsDuplexStatus OID
     *
     * Value list:
     *  1: unknown
     *  2: halfDuplex
     *  3: fullDuplex
     *
     * Unknown(1) indicates that the current duplex mode could not be determined.
     * Most likely it's because current operating status is "down"
     *
     * @return mixed
     */
    public function getDuplex()
    {
        $raw = $this->getFromPort('1.3.6.1.2.1.10.7.2.1.19');
        return preg_replace('/[^\d]/', '', $raw);
    }


    /**
     * Retrieve interface operating mode
     *
     * Value list:
     *  0: link down
     *  1: half-10Mbps
     *  2: full-10Mbps
     *  3: half-100Mbps
     *  4: full-100Mbps
     *  5: half-1Gbps
     *  6: full-1Gbps
     *
     * @return int|null
     */
    public function getLink()
    {

        $opStatus = $this->getOperatingStatus();
        $opDuplex = $this->getDuplex();

        if( $opStatus == 0 || $opDuplex == 0 || is_null(self::getPortNumber()) ) {
            if(is_null(self::getPortNumber())) {
                trigger_error("Port number is not set", E_USER_WARNING);
            }
            return null;
        }

        // If link is down
        if( !((int)!($opStatus >> 1)) ) {
            return 0;
        }

        $opSpeed  = $this->getOperatingSpeed();
        $opSpeed /= 1000000;                        // Get operating speed in Mbit/s
        $opSpeed  = log10($opSpeed)*2;              // Prepare return value
        $opDuplex = intval(!floor(log($opDuplex))); // 0 if fullDuplex, 1 if halfDuplex
        $link     = ($opSpeed - $opDuplex);

        return ($link < 0) ? 0 : $link;

    }


    /**
     * Retrieve port description from ifMIB.ifMIBObjects.ifXTable.ifXEntry.ifAlias
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getFromPort('.1.3.6.1.2.1.31.1.1.1.18');
    }


    /**
     * Set port description
     *
     * Replaces current interface description, so if it's necessary to add some information to
     * existing description please retrieve information first by getDescription() method
     *
     * @param  string $description
     * @return bool
     */
    public function setDescription(string $description): bool
    {
        return $this->setToPort('.1.3.6.1.2.1.31.1.1.1.18', 's', $description);
    }


    /**
     * Retrieve port amount
     *
     * @return int
     */
    public function count(): int
    {
        return count(self::$device['ports']);
    }


    /**
     * Check if current port is a combo-port
     *
     * @param  int|null $port Port number, optional. Uses self::$port value if no argument is used
     * @return bool
     */
    public function isCombo($port = null): bool
    {

        if( is_null($port) ) {
            $port = self::$port;
        }

        if( !$this->validatePortNumber($port) ) {
            trigger_error('Wrong port specified for ' . __METHOD__ . '()', E_USER_WARNING);
            return false;
        }

        return ( count( self::$device['ports'][$port]) > 1 ) ? true : false;

    }


    /**
     * Validate port number
     *
     * @param  int $port
     * @return bool
     */
    public function validatePortNumber(int $port): bool
    {
        $port = intval($port);
        return ( empty($port) || !array_key_exists($port, self::$device['ports']) ) ? false : true;
    }


    /**
     * Retrieve port medium type
     *
     * This method is retrieving medium type from ini file if port is not combo.
     * If switch is connected, method is retrieving active medium type via SNMP only on combo port.
     * Method is not retrieving medium type on combo port from ini file.
     *
     * Value list:
     *  0 : copper
     *  1 : fiber
     *
     * @param  int|null $port Port number, optional. Uses self::$port value if no argument is used
     * @return int|null
     */
    public function getMediumType( /** @noinspection PhpUnusedParameterInspection */ $port = null)
    {
        return null;
    }


    /**
     * Retrieve port admin state
     * Return 0 if snmp response is empty or switch returned state other (1)
     *
     * Value list:
     *  0 : error
     *  1 : enabled
     *  2 : disabled
     *
     * @return int
     */
    public function getAdminState(): int
    {
        $snmp = $this->getFromPort( self::$device['snmp']['ifAdminStatus'] );
        $res  = preg_replace('/[^\d]/', '', $snmp);
        return intval($res);
    }


    /**
     * Changes port admin state ( enabled/disabled )
     *
     * Mode argument values:
     *  int(1): enabled
     *  int(2): disabled
     *
     * @param  int $mode new port admin state
     * @return bool
     */
    public function setAdminState( int $mode ): bool
    {

        $mode = intval( $mode );

        if( $mode < 1 || $mode > 2 ) {
            trigger_error( 'Wrong parameter specified for ' . __METHOD__ . '()',  E_USER_WARNING );
            return false;
        }

        // Write mode to port
        return $this->setToPort( self::$device['snmp']['ifAdminStatus'], 'i', $mode );

    }

    /**
     * Retreive port number by vlan + mac (0 - not found)
     *
     * @throws \RuntimeException
     * @param  int    $vid VLAN ID
     * @param  string $mac MAC address
     * @return int|null
     */
    public function getPortFromFdb( int $vid, string $mac )
    {

        if( !self::$_vlan->validateVID( $vid ) ) {
            throw new \RuntimeException( "Wrong VLAN specified." );
        }

        $mac = preg_replace( '/[^A-F0-9]+/i', '', $mac );

        if( strlen( $mac ) != 12 ) {
            throw new \RuntimeException( "Wrong MAC address specified." );
        }

        /*
         * converting mac to decimal
         */
        $mac = str_split( $mac, 2 );
        array_walk($mac, function(&$val){ $val = hexdec( $val ); });
        $mac = implode( '.', $mac );

        /*
         * oid + vlan + mac(decimal)
         * ( vlan 999 mac 000b82393fc4 converts to .999.0.11.130.57.63.196 )
         */
        $port = @$this->get( '.1.3.6.1.2.1.17.7.1.2.2.1.2'.".$vid.$mac" );

        return ( is_null( $port ) ) ? null : intval( $port );

    }

}
