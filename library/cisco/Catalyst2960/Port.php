<?php

namespace dautkom\netswitch\library\cisco\Catalyst2960;


/**
 * @package dautkom\netswitch\library\cisco\Catalyst2960
 */
class Port extends \dautkom\netswitch\library\cisco\Port
{

    /**
     * Retrieves amount of sent/recieved packets on port.
     * Triggers warning and returns 'false', if port number was not set.
     *
     * This is an override as long as Catalyst2960 doesn't have OID
     * for non-unicast packets
     *
     * @see dautkom\netswitch\library\Port::getDataFlow
     * @return array
     */
    public function getDataFlow(): array
    {

        // Excessive check to avoid multiple warnings from snmpGetFromPort
        // in case if port number was not set
        if( !self::$port ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return [];
        }

        $data = $this->getFromPort(['1.3.6.1.2.1.2.2.1.11', '1.3.6.1.2.1.2.2.1.17']);

        if(!$data) {
            return [];
        }

        return [
            'in' => [
                'unicast'    => $data['1.3.6.1.2.1.2.2.1.11'.self::$port],
                'nonunicast' => null
            ],
            'out' => [
                'unicast'    => $data['1.3.6.1.2.1.2.2.1.17'.self::$port],
                'nonunicast' => null
            ],
        ];

    }

} 
