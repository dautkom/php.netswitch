<?php

namespace dautkom\netswitch\library\cisco\Catalyst2960;
use dautkom\netswitch\library\{
    Bandwidth, 
    Fdb, 
    PortSecurity,
    Qos, 
    Vlan,
    cisco\CableDiagnostics,
    cisco\Ipmb
};


/**
 * @package dautkom\netswitch\library\cisco\Catalyst2960
 */
class Common extends \dautkom\netswitch\library\cisco\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->Bandwidth        = new Bandwidth;
        $this->CableDiagnostics = new CableDiagnostics;
        $this->Fdb              = new Fdb;
        $this->Port             = new Port;
        $this->PortSecurity     = new PortSecurity;
        $this->Qos              = new Qos;
        $this->Vlan             = new Vlan;
        $this->Ipmb             = new Ipmb;

        parent::init();

    }

}
