<?php

namespace dautkom\netswitch\library\cisco;


/**
 * @package dautkom\netswitch\library\cisco
 */
class Common extends \dautkom\netswitch\library\Common
{

    /**
     * Retrieve chassis serial number
     *
     * @return bool|string
     */
    public function getSerialNumber()
    {
        return $this->get('.1.3.6.1.4.1.9.5.1.2.19.0');
    }


    /**
     * Save configuration via SNMP
     *
     * @return bool
     */
    protected function saveSnmp()
    {
        /**
         * @todo Temp solution
         */
        return $this->notImplemented();
    }


    /**
     * Save configuration via telnet using PHP wrapper
     *
     * @return bool|null
     */
    protected function savePhpTelnet()
    {
        /**
         * @todo Temp solution
         */
        return $this->notImplemented();
    }

}
