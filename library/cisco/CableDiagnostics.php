<?php

namespace dautkom\netswitch\library\cisco;


/**
 * @package dautkom\netswitch\library\cisco
 */
class CableDiagnostics extends \dautkom\netswitch\library\CableDiagnostics
{

    /**
     * @param  int $cycles
     * @return bool
     */
    public function init(int $cycles = 10): bool
    {

        /*
         * Initialize Cable Diagnostic check
         *
         * first we set ccdTdrIfAction to 2 resetting ccdTdrIfResultValid if test was performed before,
         * next we set ccdTdrIfAction to 1, initializing cable diagnostics procedure
         */
        $set = $this->setToPort( ['.1.3.6.1.4.1.9.9.390.1.2.1.1.1', '.1.3.6.1.4.1.9.9.390.1.2.1.1.1'], 'i', [2, 1] );

        if( !$set ) {
            return false;
        }

        /**
         * Wait for response status
         * 2 - processing
         * 1 - finished, OK
         */
        for( $i=0; $i<$cycles; $i++ ) {

            // Perform query if check is finished
            $status = null;

            // Suppress no link port error
            $status = @$this->getFromPort( '.1.3.6.1.4.1.9.9.390.1.2.1.1.4' );

            // Set flag and return 'true' if check successfully finished
            if( $status == 1 ) {
                $this->init = true;
                return true;
            }

            // wait 0.1 second for next loop step
            usleep( 100000 );

        }

        return false;

    }


    /**
     * Retrieves pairs statuses.
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * Return values:
     *   1: unknown(1)
     *   2: terminated(2)
     *   3: notCompleted(3)
     *   4: notSupported(4)
     *   5: open(5)
     *   6: shorted(6)
     *   7: impedanceMismatch(7)
     *   8: broken(8)
     *   9: indeterminate(9)
     *
     * @return array|null
     */
    public function getState()
    {

        if( !$this->init ) {
            return null;
        }

        // ccdTdrIfResultPairStatus
        return $this->getPairData('1.3.6.1.4.1.9.9.390.1.2.2.1.8');

    }


    /**
     * Retrieve pairs length
     * Don't forget to call CableDiagnostics->init() method first.
     *
     * Maybe it'd be reasonable to combine this method with ccdTdrIfResultPairLengthUnit
     *
     * @return array|null
     */
    public function getLength()
    {

        if( !$this->init ) {
            return null;
        }

        // ccdTdrIfResultPairLength
        return $this->getPairData('1.3.6.1.4.1.9.9.390.1.2.2.1.3');

    }


    /**
     * @param  string $oid
     * @return array
     */
    private function getPairData($oid)
    {

        $oid = $this->addPortNumberToOID($oid);
        $raw = $this->walk($oid, true);
        $res = array();

        if (is_array($raw)) {

            array_walk($raw,
                function ($value, $key) use ($oid, &$res) {
                    $key       = intval(str_replace($oid, '', $key));
                    $res[$key] = $value;
                }
            );

        }

        return $res;

    }

}
