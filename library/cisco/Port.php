<?php

namespace dautkom\netswitch\library\cisco;


/**
 * @package dautkom\netswitch\library\cisco
 */
class Port extends \dautkom\netswitch\library\Port
{

    /**
     * Retrieves flow control state on current port
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return null
     */
    public function getFlowControlState()
    {
        // TODO: temporary solution
        return $this->notImplemented();
    }


    /**
     * Changes flow control state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return null
     */
    public function setFlowControlState(int $mode)
    {
        // TODO: temporary solution
        return $this->notImplemented();
    }


    /**
     * Retrieves learning state ( enabled/disabled ) on current port
     *
     * @return null
     */
    public function getLearningState()
    {
        // TODO: temporary solution
        return $this->notImplemented();
    }


    /**
     * Set learning state on current port
     *
     * Mode argument values:
     *  1: enabled
     *  2: disabled
     *
     * @param  int $mode
     * @return null
     */
    public function setLearningState(int $mode)
    {
        // TODO: temporary solution
        return $this->notImplemented();
    }


    /**
     * Clears all statistic counters on all ports
     *
     * @return null
     */
    public function clearStats()
    {
        // TODO: temporary solution
        return $this->notImplemented();
    }

}
