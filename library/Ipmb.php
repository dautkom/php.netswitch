<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
abstract class Ipmb extends NetSwitch
{

    /**
     * Retrieve port IP-MAC binding state.
     *
     * @return bool|null
     */
    abstract public function getState();

}
