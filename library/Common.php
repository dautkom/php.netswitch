<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 *
 * @property Bandwidth
 * @property CableDiagnostics
 * @property Fdb
 * @property Ipmb
 * @property Port
 * @property PortSecurity
 * @property Qos
 * @property Stp
 * @property Vlan
 */
abstract class Common extends NetSwitch
{

    /**
     * Object for physical interfaces interaction
     * @var Port
     */
    public $Port;

    /**
     * Object for interacting with settings related to port security technology
     * @var PortSecurity
     */
    public $PortSecurity;

    /**
     * Object for interacting with VLAN settings
     * @var Vlan
     */
    public $Vlan;

    /**
     * Object for work with bandwidth control management settings
     * @var Bandwidth
     */
    public $Bandwidth;

    /**
     * Object for interacting with settings related to FDB table management
     * @var Fdb
     */
    public $Fdb;

    /**
     * Object for interacting with qos settings
     * @var Qos
     */
    public $Qos;

    /**
     * Object for interacting with Cable Diagnostic
     * @var CableDiagnostics
     */
    public $CableDiagnostics;

    /**
     * Object for interacting with IP-MAC binding
     * @var Ipmb
     */
    public $Ipmb;

    /**
     * Object for interacting with Stp
     * @var Stp
     */
    public $Stp;

    /**
     * Default value of saving method.
     * Override this value in specific vendor or model Common file, if you need other saving method,
     * for specific switch.
     *
     * Possible values: telnet, snmp
     *
     * @var string
     */
    protected $mode = 'snmp';

    /**
     * @var array
     */
    private $common_filter = [
        "/Hex:/",
        "/Hex-/",
        "/x:/",
        "/x-/",
        "/ /",
        "/\\./",
        "/Hex-STRING:/"
    ];

    /**
     * Load ini file and clear static properties
     * @ignore
     */
    public function __construct()
    {
        self::$port = null;
        $this->loadIni();
    }


    /**
     * Cross-object instance handlers
     * @ignore
     */
    public function init()
    {
        self::$_vlan = $this->Vlan;
        self::$_fdb  = $this->Fdb;
    }


    /**
     * Retrieve model id string
     * Result looks like VENDOR\MODEL
     *
     * @return string
     */
    public function getModelId(): string
    {
        return self::$model_id;
    }


    /**
     * Retrieve model name
     *
     * @return string
     */
    public function getModel(): string
    {
        return substr( self::$model_id, (strpos(self::$model_id, '\\') + 1));
    }


    /**
     * Retrieve vendor name
     *
     * @return string
     */
    public function getVendor(): string
    {
        return ucfirst(substr(self::$model_id, 0, strpos(self::$model_id, '\\')));
    }


    /**
     * Retrieve firmware version from rmon.probeConfig.probeSoftwareRev
     *
     * @return null|string
     */
    public function getSoftwareRevision()
    {
        return $this->get('.1.3.6.1.2.1.16.19.2.0');
    }


    /**
     * Retrieve hardware revision from rmon.probeConfig.probeHardwareRev
     *
     * @return null|string
     */
    public function getHardwareRevision()
    {
        return $this->get('.1.3.6.1.2.1.16.19.3.0');
    }


    /**
     * Retrieve uptime.
     *
     * Return value consists of timeticks by default. 1 tTick = 1/10 sec.
     * Raw output can be retrieved if method is called as getUptime(true).
     *
     * If no timeticks where found in result string, the raw outupt will
     * be returned regardless of argument value
     *
     * @param  bool   $raw if true the raw string will be returned
     * @return null|string
     */
    public function getUptime(bool $raw = false)
    {

        $uptime = $this->get('.1.3.6.1.2.1.1.3.0');
        $tticks = [];

        preg_match( '/\((\d+)\)/', $uptime, $tticks );

        if( !$raw && array_key_exists(1, $tticks) && $tticks[1] > 0 ) {
            return $tticks[1];
        }

        return $uptime;

    }


    /**
     * Retrieve device name from sysName.0
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->get('.1.3.6.1.2.1.1.5.0');
    }


    /**
     * Set device name to sysName.0
     *
     * Note, that it's impossible to write a zero-length (empty) value on some switches and firmwares.
     * If it's necessary to reset the name property - use space instead, but beware that 0x20 string
     * will be stored in OID.
     *
     * @param  string $name
     * @return bool
     */
    public function setName(string $name): bool
    {
        return $this->set('.1.3.6.1.2.1.1.5.0', 's', $name);
    }


    /**
     * Retrieve device location from sysLocation.0
     *
     * @return string|null
     */
    public function getLocation()
    {
        return $this->get('.1.3.6.1.2.1.1.6.0');
    }


    /**
     * Set device location to sysLocation.0
     *
     * @param  string $location
     * @return bool
     */
    public function setLocation(string $location): bool
    {
        return $this->set('.1.3.6.1.2.1.1.6.0', 's', $location);
    }


    /**
     * Retrieve device contact information from sysContact.0
     *
     * @return string|null
     */
    public function getContact()
    {
        return $this->get('.1.3.6.1.2.1.1.4.0');
    }


    /**
     * Set device contact information to sysContact.0
     *
     * @param  string $contact
     * @return bool
     */
    public function setContact(string $contact): bool
    {
        return $this->set('.1.3.6.1.2.1.1.4.0', 's', $contact);
    }


    /**
     * Save configuration to NVRAM
     * Mode can be changed from class variable @see Common::mode or
     * from method variable $mode, prefered is class variable.
     *
     * @param  string $mode
     * @return mixed
     */
    public function save( string $mode = '' )
    {

        // Check input data
        if( empty( $mode ) ) $mode = $this->mode;

        // Save method
        switch( $mode ) {

            case 'telnet':
                return $this->savePhpTelnet();
            break;

            default:
                return $this->saveSnmp();
            break;

        }

    }


    /**
     * Retrieve device MAC address from dot1dBaseBridgeAddress.0
     *
     * @return string
     */
    public function getDeviceMacAddress(): string
    {
        $snmp = preg_replace( $this->common_filter, '', $this->get('.1.3.6.1.2.1.17.1.1.0') );
        return strval($snmp);
    }


    /**
     * Save configuration via SNMP
     *
     * @return bool|null
     */
    abstract protected function saveSnmp();


    /**
     * Save configuration via telnet using PHP wrapper
     *
     * @uses   Telnet
     * @return bool|null
     */
    abstract protected function savePhpTelnet();


    /**
     * Retrieve device serial number
     *
     * @return null|string
     */
    abstract public function getSerialNumber();

}
