<?php

namespace dautkom\netswitch\library;
use dautkom\netswitch\NetSwitch;


/**
 * @package dautkom\netswitch\library
 */
abstract class Stp extends NetSwitch
{

    /**
     * Retrieve switch instance stp priority
     *
     * @return int
     */
    abstract public function getPriority();

    /**
     * Set priority of instance.
     * The priority must be dividable by 4096
     *
     * @param int $priority
     * @throws \RuntimeException
     * @return bool|null
     */
    abstract public function setPriority( $priority );

    /**
     * The port number of the port which offers the lowest cost path
     * from this bridge to the root bridge.
     *
     * @throws \RuntimeException
     * @return int
     */
    abstract public function getRootPort();

    /**
     * Retrieve switch stp status
     *
     * Value List
     * enabled (1)
     * disabled (2)
     *
     * @return int
     */
    abstract public function getStpStatus();

    /**
     * Retreive root switch mac-address
     *
     * @throws \RuntimeException
     * @return string
     */
    abstract public function getRootMac();

    /**
     * Retreive desgnated bridge mac
     *
     * @throws \RuntimeException
     * @return string
     */
    abstract public function getDesignatedBridgeMac();

    /**
     * Retreive switch mac
     *
     * @throws \RuntimeException
     * @return string
     */
    abstract public function getSwitchMac();

    /**
     * Retreive stp status on current port
     *
     * Value List:
     * enabled (1)
     * disabled (2)
     *
     * @throws \RuntimeException
     * @return int
     */
    abstract public function getStateOnPort();

    /**
     * Set stp status (enabled/disabled) on current port
     *
     * Value List:
     * enabled (1)
     * disabled (2)
     *
     * @throws \RuntimeException
     * @param $set
     * @return bool|null
     */
    abstract public function setStateOnPort( $set );

}
