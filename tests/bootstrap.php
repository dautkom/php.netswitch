<?php

namespace tests;
use dautkom\netswitch\NetSwitch;
use PHPUnit\Framework\TestCase;

/**
 * @package tests
 */
class NetSwitch_PHPUnit_Framework_TestCase extends TestCase
{

    /**
     * @var array
     */
    protected $dlinks;

    /**
     * @var array
     */
    protected $zyxels;

    /**
     * @var array
     */
    protected $ciscos;

    /**
     * @var array
     */
    protected $snmp;

    /**
     * @var array
     */
    protected $devices;

    /**
     * @var \dautkom\netswitch\library\Common
     */
    protected $obj;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $vendor;

    /**
     * @ignore
     */
    public function __construct()
    {
        
        parent::__construct();

        // Register properties
        $this->dlinks = json_decode($GLOBALS['dlinks'], true);
        $this->zyxels = json_decode($GLOBALS['zyxels'], true);
        $this->ciscos = json_decode($GLOBALS['ciscos'], true);
        $this->snmp   = json_decode($GLOBALS['snmp'], true);

        // Register all supported namespaces
        $switches = array_merge($this->dlinks, $this->zyxels, $this->ciscos);
        foreach( $switches as $ip => $switch ) {
            $this->devices[$switch[0]] = array($ip => $switch);
        }
        
    }

    /**
     * @ignore
     */
    public function connect()
    {
        if( is_null($this->obj) ) {
            $this->obj = (new NetSwitch)->connect($this->ip, $this->snmp[$this->vendor]);
        }
    }

    /**
     * @ignore
     */
    public function compile()
    {
        if( is_null($this->obj) ) {
            $this->obj = (new NetSwitch)->compile( $this->data[0] );
        }
    }
    
}
