<?php

namespace tests\integrity;
use dautkom\netswitch\NetSwitch;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\integrity
 */
class ConnectionTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * Test successful connection
     *
     * @group connection
     */
    public function testConnect()
    {
        foreach ($this->dlinks as $ip => $data) {
            try {
                $obj = (new NetSwitch)->connect($ip, $this->snmp['dlink']);
                $this->assertEquals($data[0], $obj->getModelId());
                break;
            } catch (\Throwable $e) {
                $this->fail("Unable to connect to $ip");
            }
        }
    }


    /**
     * Test unsuccessful connection
     *
     * @group connection
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp #Unable to connect to device.+#
     */
    public function testConnectionExceptionToWrongIP()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $obj = (new NetSwitch)->connect('127.0.0.1');
    }


    /**
     * Test unsuccessful connection
     *
     * @group connection
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp #Unable to connect to device.+#
     */
    public function testConnectionExceptionWithWrongCommunity()
    {
        foreach ($this->dlinks as $ip => $data) {
            /** @noinspection PhpUnusedLocalVariableInspection */
            $obj = (new NetSwitch)->connect($ip, array('foo', 'bar'));
            break;
        }
    }


    /**
     * Test malformed IP-address
     *
     * @group connection
     * @expectedException \Exception
     * @expectedExceptionMessage Wrong IP-address specified
     */
    public function testErroneousIpAddressException()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $obj = (new NetSwitch)->connect('192.168.0.256');
    }

}
