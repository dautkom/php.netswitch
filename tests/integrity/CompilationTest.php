<?php

namespace tests\integrity;
use dautkom\netswitch\NetSwitch;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @property array devices
 */
class CompilationTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * Test if compilation (dry-run) is successful
     *
     * @group compilation
     */
    public function testSupportedCompilation()
    {
        
        echo "Testing compilation for ".count($this->devices)." devices.\n";
        
        foreach($this->devices as $device => $data) {
            $obj = (new NetSwitch())->compile($device);
            $this->assertInternalType('object', $obj);
        }
        
    }


    /**
     * Test exception if trying to compile with unsupported vendor\model
     *
     * @group compilation
     * @expectedException \Error
     * @expectedExceptionMessageRegExp #Class .+ not found#
     */
    public function testUnsupporteVendorCompilation()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $obj = (new NetSwitch())->compile('foo\bar');
    }


    /**
     * Test exception if trying to compile with unsupported vendor\model
     *
     * @group compilation
     * @expectedException \Error
     * @expectedExceptionMessageRegExp #Class .+ not found#
     */
    public function testUnsupportedDeviceCompilation()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $obj = (new NetSwitch())->compile('dlink\DES000');
    }

}
