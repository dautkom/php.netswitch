<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class CableDiagnosticsTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $testset
     */
    public function testInit($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $initResult = $this->obj->CableDiagnostics->init();
            $this->assertEquals($subject[1], $initResult);
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->obj->CableDiagnostics->init();
            $this->assertEquals($subject[1], $this->obj->CableDiagnostics->getState());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetLength($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject);
            $initResult = $this->obj->CableDiagnostics->init();
            $lengths = $this->obj->CableDiagnostics->getLength();

            if( $initResult === false ) {
                $this->assertNull( $lengths );
            }
            else
            {
                foreach ($lengths as $length) {
                    $this->assertGreaterThanOrEqual(-1, $length);
                    $this->assertLessThan(150, $length);
                }
            }

        }
    }

}
