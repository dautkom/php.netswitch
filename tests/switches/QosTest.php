<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class QosTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetQos($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Qos->getQos());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testSetQos($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Qos->setQos($subject[1]));
            $data1 = $this->obj->Qos->getQos();
            $this->assertEquals($subject[1], $data1);
        }
    }

}
