<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class BandwidthTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetRxRate($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Bandwidth->getRxRate());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetRxRate($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Bandwidth->setRxRate($subject[1]));
            $data1 = $this->obj->Bandwidth->getRxRate();
            $this->assertEquals($subject[1], $data1);
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetTxRate($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Bandwidth->getTxRate());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetTxRate($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Bandwidth->setTxRate($subject[1]));
            $data1 = $this->obj->Bandwidth->getTxRate();
            $this->assertEquals($subject[1], $data1);
        }
    }

}
