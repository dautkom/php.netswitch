<?php
namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class VlanTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $testset
     */
    public function testValidateVID($testset)
    {
        $this->connect();
        foreach($testset as $subject) {
            $this->assertEquals($subject[0], $this->obj->Vlan->validateVID($subject[1]));
        }
    }

    /**
     * @group switches\general
     */
    public function testGetAll()
    {
        $this->connect();
        $this->assertNotEmpty($this->obj->Vlan->getAll());
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testCreateVlan($testset)
    {
        $this->connect();

        foreach($testset as $subject) {

            /* Delete vlan if exists */
            $data = $this->obj->Vlan->getAll();
            if (array_key_exists($subject[0], $data)) {
                $this->obj->Vlan->deleteVlan($subject[0]);
            }

            $this->assertEquals(true, $this->obj->Vlan->createVlan($subject[0], $subject[1]));
            $data = $this->obj->Vlan->getAll();
            $this->assertArrayHasKey($subject[0], $data);
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testAddUntaggedVlanOnPort($testset)
    {

        $this->connect();
        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->addUntaggedVlanOnPort($subject[1]));
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testAddTaggedVlanOnPort($testset)
    {

        $this->connect();
        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->addTaggedVlanOnPort($subject[1]));
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testIsTagged($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->isTagged());
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testDeleteVlanOnPort($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->deleteVlanOnPort($subject[1]));
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetTransit($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->setTransit());
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testIsTransit($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->isTransit());
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testClearVlansOnPort($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Vlan->clearVlansOnPort());
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testSetGvrpPvid($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[2], $this->obj->Vlan->setGvrpPvid($subject[1]));
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testDeleteVlan($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals(true, $this->obj->Vlan->deleteVlan($subject[0]));
            $data = $this->obj->Vlan->getAll();
            $this->assertArrayNotHasKey($subject[0], $data);
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testDeleteVlanMe($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            @$this->obj->Vlan->deleteVlan($subject[0]);
        }

        $data = $this->obj->Vlan->getAll();
        foreach($testset as $subject) {
            $this->assertArrayNotHasKey($subject[0], $data);
        }

    }

}
