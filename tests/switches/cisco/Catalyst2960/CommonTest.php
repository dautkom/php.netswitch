<?php

namespace tests\switches\cisco\Catalyst2960;

/**
 * @package tests\switches\cisco\Catalyst2960
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['cisco\Catalyst2960']);
        $this->data   = $this->devices['cisco\Catalyst2960'][$this->ip];
        $this->vendor = 'cisco';

    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param string $expected
     */
    public function testGetModel($expected = 'Catalyst2960')
    {
        parent::testGetModel($expected);
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param string $expected
     */
    public function testGetModelId($expected = 'cisco\Catalyst2960')
    {
        parent::testGetModelId($expected);
    }

    /**
     * @group switches\cisco\Catalyst2960
     */
    public function testGetIni()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param string $expected
     */
    public function testGetVendor($expected = 'Cisco')
    {
        parent::testGetVendor($expected);
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param string $expected
     */
    public function testGetSoftwareRevision($expected = "9-M)")
    {
        parent::testGetSoftwareRevision($expected);
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param string $expected
     */
    public function testGetHardwareRevision($expected = "")
    {
        parent::testGetHardwareRevision($expected);
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param string $expected
     */
    public function testGetSerialNumber($expected = "FOC1006X2H2")
    {
        parent::testGetSerialNumber($expected);
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param $expected
     */
    public function testLocation($expected)
    {
        $this->connect();
        $this->assertEquals($this->data[1], $this->obj->getLocation());
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param $expected
     */
    public function testName($expected)
    {
        $this->connect();
        $this->assertEquals($this->data[2], $this->obj->getName());
    }

    /**
     * @group switches\cisco\Catalyst2960
     * @param $expected
     */
    public function testContact($expected)
    {
        $this->connect();
        $this->assertEquals($this->data[3], $this->obj->getContact());
    }

    /**
     * @group switches\cisco\Catalyst2960
     */
    public function testUptime()
    {
        parent::testUptime();
    }

}
