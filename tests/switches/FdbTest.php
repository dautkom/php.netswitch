<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class FdbTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     */
    public function testShowAllPermanent()
    {
        $this->connect();
        $this->assertNotEmpty($this->obj->Fdb->showAllPermanent());

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testShowPortPermanent($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Fdb->showPortPermanent());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testShowPortAll($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Fdb->showPortAll());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testAddPermanent($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Fdb->addPermanent($subject[1], $subject[2]));
            $data1 = $this->obj->Fdb->showPortPermanent();
            $this->assertEquals($subject[3], $data1);
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testDeletePermanent($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Fdb->deletePermanent($subject[1], $subject[2]));
            $this->assertEmpty($this->obj->Fdb->showPortPermanent());
        }
    }


    /**
     * @group switches\general
     * @param $testset
     */
    public function testDeleteAllPermanent($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Fdb->addPermanent($subject[1], $subject[2]));
            $this->assertEquals(true, $this->obj->Fdb->deleteAllPermanent());
            $this->assertEmpty($this->obj->Fdb->showPortPermanent());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetPortFromFdb($testset)
    {
        $this->connect();
        foreach($testset as $subject) {
            $this->assertSame($subject[2], $this->obj->Port->getPortFromFdb( $subject[0], $subject[1] ));
        }
    }

}
