<?php

namespace tests\switches\dlink\DES320028F;


/**
 * @package tests\switches\dlink\DES320028F
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028F']);
        $this->data   = $this->devices['dlink\DES320028F'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('DES320028F');
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('dlink\DES320028F');
    }

    /**
     * @group switches\dlink\DES320028F\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Dlink');
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision("Build 1.50.B002");
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision("A1");
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber("PVC71A5000028");
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\dlink\DES320028F\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\dlink\DES320028F\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
