<?php

namespace tests\switches\dlink\DES320028F;


/**
 * @package tests\switches\dlink\DES320028F
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028F']);
        $this->data   = $this->devices['dlink\DES320028F'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            32768,
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,32768],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            28,
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            'd8fee3923380',
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            '90ef68c40932',
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetSwitchMacTest()
    {
        parent::testGetSwitchMac(array(
            '1cbdb96dfde0',
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\stp
     *
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

