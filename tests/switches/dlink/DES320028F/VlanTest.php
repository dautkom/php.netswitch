<?php
namespace tests\switches\dlink\DES320028F;


/**
 * @package tests\switches\dlink\DES320028F
 */
class VlanTest extends \tests\switches\VlanTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028F']);
        $this->data   = $this->devices['dlink\DES320028F'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testValidateVIDTest()
    {
        parent::testValidateVID(array(
            [true,  777],
            [false, 7771],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     */
    public function testGetAllTest()
    {
        parent::testGetAll();
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testCreateVlanTest()
    {
        parent::testCreateVlan(array(
            [555, 'TestVlan555'],
            [777, 'TestVlan777'],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testAddUntaggedVlanOnPortTest()
    {
        parent::testAddUntaggedVlanOnPort(array(
            [10, 555],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testAddTaggedVlanOnPortTest()
    {
        parent::testAddTaggedVlanOnPort(array(
            [12, 777],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testIsTaggedTest()
    {
        parent::testIsTagged(array(
            [12],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testDeleteVlanOnPortTest()
    {
        parent::testDeleteVlanOnPort(array(
            [10, 555],
            [12, 777],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testsetTransitTest()
    {
        parent::testsetTransit(array(
            [10],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testIsTransitTest()
    {
        parent::testIsTransit(array(
            [10],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testClearVlansOnPortTest()
    {
        parent::testClearVlansOnPort(array(
            [10],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testSetGvrpPvidTest()
    {
        parent::testSetGvrpPvid(array(
            [10, 555, true],
            [10, 1, true],
        ));
    }

    /**
     * @group switches\dlink\DES320028F\vlan
     *
     */
    public function testDeleteVlanTest()
    {
        parent::testDeleteVlan(array(
            [555],
            [777],
        ));
    }

}
