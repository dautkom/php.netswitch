<?php

namespace tests\switches\dlink\DGS121052ME\B1;

use PHPUnit\Framework\Error\Error;
use RuntimeException;

/**
 * @package tests\switches\dlink\DGS121052ME\B1
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121052ME\B1']);
        $this->data   = $this->devices['dlink\DGS121052ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     *
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testSetPortNumberExceptionTest()
    {
        $this->expectExceptionMessage("Wrong port specified");
        $this->expectException(RuntimeException::class);
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testPortNumberNotSetErrorTest()
    {
        $this->expectException(Error::class);
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription([
            [1, ''],
            [52, '[mon]'],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription([
            [1, ''],
            [52, '[mon]']
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     */
    public function testCountTest()
    {
        parent::testCount(52);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     */
    public function testGetLinkTest()
    {
        parent::testGetLink([
            [1, 2],  // 2: full-10Mbps
            [2, 4],  // 4: full-100Mbps
            [3, 6],  // 6: full-1Gbps
            [4, 0],  // 0: link down
            [47, 6], // 6: full-1Gbps
            [50, 0], // 0: fiber link down
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed([
            [1,  10000000],   // 10Mmbs link
            [2,  100000000],  // 100Mmbs link
            [3,  1000000000], // 1000Mmbs link
            [4,  0],          // no-link
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus([
            [1, 1], // Link up
            [4, 2]  // Link down
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo([
            [1, false],
            [52, false]
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo([
            [1, false],
            [52, false]
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType([
            [1, 0],
            [52, 1],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testCompileGetMediumTypeTest()
    {
        parent::testCompileGetMediumType([
            [1, 0],
            [52, 1],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState([
            [7,  2],
            [16, 2],
            [50, 1]
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState([
            [7, 2],
            [50, 1]
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testsetFlowControlStateTest()
    {
        parent::testsetFlowControlState([
            [1, 2],
            [14, 1],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testgetFlowControlStateTest()
    {
        parent::testgetFlowControlState([
            [1, 2],
            [14, 1]
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState([
            [1, 1, true],
            [16, 2, true],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState([
            [1, 1],
            [16, 2],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     */
    public function testClearStatsTest()
    {
        parent::testClearStats();
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     */
    public function testGetDuplexTest()
    {
        parent::testGetDuplex(array(
            [3, 3],
            [4, 1]
        ));
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\port
     *
     */
    public function testGetPortFromFdbTest()
    {
        $this->connect();
        $this->obj->Port->setPortNumber(15);

        if (array_key_exists(555, $this->obj->Vlan->getAll())) {
            $this->obj->Vlan->deleteVlan(555);
        }

        @$this->obj->Vlan->createVlan(555, 'TestVlan555');
        $this->obj->Vlan->clearVlansOnPort();
        $this->obj->Vlan->addUntaggedVlanOnPort(555);
        $this->obj->Fdb->addPermanent('00-0B-82-39-3F-C4', 555);

        parent::testGetPortFromFdb(array(
            [555, '00-0B-82-39-3F-C4', 15],
        ));

        @$this->obj->Vlan->deleteVlan(555);
    }

}
