<?php

namespace tests\switches\dlink\DGS121052ME\B1;


/**
 * @package tests\switches\dlink\DGS121052ME\B1
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121052ME\B1']);
        $this->data   = $this->devices['dlink\DGS121052ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121052ME\B1\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit([
            [21, true],
            [1, true],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        $no_link = [1 => '9', 2 => '9', 3 => '9', 4 => '9'];
        $optic   = null;

        parent::testGetState([
            [50, $optic],
            [5, $no_link],
        ]);
    }

    /**
     * @group switches\dlink\DGS121052ME\B1\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength([1, 21]);
    }

}
