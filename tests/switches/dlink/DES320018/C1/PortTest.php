<?php

namespace tests\switches\dlink\DES320018\C1;


/**
 * @package tests\switches\dlink\DES320018\C1
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320018\C1']);
        $this->data   = $this->devices['dlink\DES320018\C1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     *
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Wrong port specified
     */
    public function testSetPortNumberExceptionTest()
    {
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testPortNumberNotSetErrorTest()
    {
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription(array(
            [1, ''],
            [18, '[mon] [stp]']
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testCountTest()
    {
        parent::testCount(18);
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetLinkTest()
    {
        parent::testGetLink(array(
            [1, 0], // 0: link down
            [2, 1], // 1: half-10Mbps
            [3, 2], // 2: full-10Mbps
            [4, 3], // 3: half-100Mbps
            [5, 4], // 4: full-100Mbps
            [18, 6] // 6: full-1Gbps
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription(array(
            [1, ''],
            [18, '[mon] [stp]'],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed(array(
            [1, 0],             // link down no speed
            [2, 10000000],      // 10Mmbs link
            [4, 100000000],     // 100Mbs link
            [18, 1000000000],   // 1Gbs link
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus(array(
            [1, 2], // Link down
            [2, 1]  // Link up
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo(array(
            [1, false],
            [17, true]
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo(array(
            [1, false],
            [17, true]
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType(array(
            [1, 0],
            [17, null],
            [18, 1],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testCompileGetMediumTypeTest()
    {
        parent::testCompileGetMediumType(array(
            [1, 0],
            [17, null],
            [18, 1],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState(array(
            [10, 2],
            [18, 1]
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState(array(
            [10, 2],
            [18, 1]
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testgetFlowControlStateTest()
    {
        parent::testgetFlowControlState(array(
            [1, 2],
            [14, 1]
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testsetFlowControlStateTest()
    {
        parent::testsetFlowControlState(array(
            [1, 2],
            [14, 1],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState(array(
            [1, 1],
            [18, 2],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState(array(
            [1, 1, true],
            [18, 2, true],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     */
    public function testClearStatsTest()
    {
        parent::testClearStats();
    }

    /**
     * @group switches\dlink\DES320018\C1\port
     *
     */
    public function testGetPortFromFdbTest()
    {
        parent::testGetPortFromFdb(array(
            [992, '00-0B-82-39-3F-C4' , 15],
        ));
    }

}
