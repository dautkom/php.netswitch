<?php

namespace tests\switches\dlink\DES320018\C1;


/**
 * @package tests\switches\dlink\DES320018\C1
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320018\C1']);
        $this->data   = $this->devices['dlink\DES320018\C1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320018\C1\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [1, true],
            [5, true],
            [17,true],
            [18,false],
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        // Test array
        $link_full_100        = [1 => '8', 2 => '0', 3 => '0', 4 => '8'];
        $no_link_100          = [1 => '8', 2 => '7', 3 => '7', 4 => '8'];
        $no_link_1000_combo   = [1 => '7', 2 => '7', 3 => '7', 4 => '7'];
        $optic                = null;

        parent::testGetState(array(
            [5, $link_full_100],
            [1, $no_link_100],
            [17, $no_link_1000_combo],
            [18, $optic]
        ));
    }

    /**
     * @group switches\dlink\DES320018\C1\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            5,
            17,
            18,
        ));
    }

}
