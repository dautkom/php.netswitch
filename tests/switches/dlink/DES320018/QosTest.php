<?php

namespace tests\switches\dlink\DES320018;


/**
 * @package tests\switches\dlink\DES320018
 */
class QosTest extends \tests\switches\QosTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320018']);
        $this->data   = $this->devices['dlink\DES320018'][$this->ip];
        $this->vendor = 'dlink';

    }


    /**
     * @group switches\dlink\DES320018\qos
     *
     */
    public function testGetQosTest()
    {
        parent::testGetQos(array(
            [1, 0],
            [2, 1],
            [3, 2],
            [4, 7],
            [17, 3],
        ));
    }

    /**
     * @group switches\dlink\DES320018\qos
     *
     */
    public function testSetQosTest()
    {
        parent::testSetQos(array(
            [1, 0],
            [2, 1],
            [3, 2],
            [4, 7],
            [17, 3],
        ));
    }

}
