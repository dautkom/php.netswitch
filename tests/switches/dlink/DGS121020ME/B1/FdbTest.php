<?php

namespace tests\switches\dlink\DGS121020ME\B1;


/**
 * @package tests\switches\dlink\DGS121020ME\B1
 */
class FdbTest extends \tests\switches\FdbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip = key($this->devices['dlink\DGS121020ME\B1']);
        $this->data = $this->devices['dlink\DGS121020ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     */
    public function testCreateEnvironment()
    {
        $this->connect();
        $this->obj->Vlan->createVlan(555, 'TestVlan555');
        $this->obj->Vlan->createVlan(777, 'TestVlan777');

        $this->obj->Port->setPortNumber(10);
        $this->obj->Vlan->addUntaggedVlanOnPort(555);

        $this->obj->Port->setPortNumber(11);
        $this->obj->Vlan->addUntaggedVlanOnPort(777);

        $this->assertTrue(true);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     *
     */
    public function testAddPermanentTest()
    {
        $test_dataset = [
            [0 => '000b82393fc2', 1 => '555'],
            [0 => '000b82393fc3', 1 => '777'],
            [[0 => '000b82393fc3', 1 => '777'], [0 => '000b82393fc4', 1 => '777']],
        ];
        parent::testAddPermanent([
            [10, '00:0b:82:39:3f:c2', 555, [$test_dataset[0]]],
            [11, '00:0b:82:39:3f:c3', 777, [$test_dataset[1]]],
            [11, '00:0b:82:39:3f:c4', 777, $test_dataset[2]],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     */
    public function testShowAllPermanentTest()
    {
        parent::testShowAllPermanent();
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     *
     */
    public function testShowPortPermanentTest()
    {
        $test_dataset = [
            [0 => '000b82393fc2', 1 => '555'],
            [[0 => '000b82393fc3', 1 => '777'], [0 => '000b82393fc4', 1 => '777']],
        ];
        parent::testShowPortPermanent([
            [10, [$test_dataset[0]]],
            [11, $test_dataset[1]],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb_slow
     *
     */
    public function testShowPortAllTest()
    {
        $test_dataset = [
            ['vid' => '555', 'vlan' => 'TestVlan555', 'state' => 'Static', 'mac' => '000b82393fc2'],
        ];
        parent::testShowPortAll([
            [10, $test_dataset]
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     *
     */
    public function testDeletePermanentTest()
    {
        parent::testDeletePermanent([
            [10, '00-0b-82-39-3f-c2', 555]
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     *
     */
    public function testDeleteAllPermanentTest()
    {
        parent::testDeleteAllPermanent([
            [11, '00:0b:82:39:3b:c3', 777],
            [11, '00:0b:82:39:3b:c4', 777]

        ]);
    }


    /**
     * @group switches\dlink\DGS121020ME\B1\fdb
     */
    public function testDestroyEnvironment()
    {
        $this->connect();
        @$this->obj->Vlan->deleteVlan(555);
        @$this->obj->Vlan->deleteVlan(777);
        $this->assertTrue(true);
    }

}
