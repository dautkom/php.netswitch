<?php

namespace tests\switches\dlink\DGS121020ME\B1;


/**
 * @package tests\switches\dlink\DGS121020ME\B1
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121020ME\B1']);
        $this->data   = $this->devices['dlink\DGS121020ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority([
            32768,
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority([
            [true, 32768],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort([15]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus([1]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(['c4e90ab43c10']);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(['bc9911d89a5e']);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetSwitchMacTest()
    {
        parent::testGetSwitchMac(['f0b4d21d6ec0']);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort([
            [1, 2]
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\stp
     *
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort([
            [1, 1, true],
            [1, 2, true]
        ]);
    }

}

