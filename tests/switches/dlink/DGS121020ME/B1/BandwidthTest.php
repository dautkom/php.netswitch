<?php

namespace tests\switches\dlink\DGS121020ME\B1;


/**
 * @package tests\switches\dlink\DGS121020ME\B1
 */
class BandwidthTest extends \tests\switches\BandwidthTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121020ME\B1']);
        $this->data   = $this->devices['dlink\DGS121020ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121020ME\B1\bandwidth
     */
    public function testsetRxRateTest()
    {
        parent::testsetRxRate([
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [20, 49152], # 48 # 1 Mbit/s
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\bandwidth
     *
     */
    public function testgetRxRateTest()
    {
        parent::testgetRxRate([
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [20, 49152], # 48 # 1 Mbit/s
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\bandwidth
     *
     */
    public function testsetTxRateTest()
    {
        parent::testsetTxRate([
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [20, 49152], # 48 # 1 Mbit/s
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\bandwidth
     *
     */
    public function testgetTxRateTest()
    {
        parent::testgetTxRate([
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [20, 49152], # 48 # 1 Mbit/s
        ]);
    }

}
