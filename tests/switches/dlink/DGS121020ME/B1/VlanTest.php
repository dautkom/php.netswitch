<?php
namespace tests\switches\dlink\DGS121020ME\B1;


/**
 * @package tests\switches\dlink\DGS121020ME\B1
 */
class VlanTest extends \tests\switches\VlanTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121020ME\B1']);
        $this->data   = $this->devices['dlink\DGS121020ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testValidateVIDTest()
    {
        parent::testValidateVID([
            [true,  777],
            [false, 7771],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     */
    public function testGetAllTest()
    {
        parent::testGetAll();
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testCreateVlanTest()
    {
        parent::testCreateVlan([
            [555, 'TestVlan555'],
            [777, 'TestVlan777'],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testsetTransitTest()
    {
        parent::testsetTransit([
            [10],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testIsTransitTest()
    {
        parent::testIsTransit([
            [10],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testClearVlansOnPortTest()
    {
        parent::testClearVlansOnPort([
            [10], [11], [12]
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testAddUntaggedVlanOnPortTest()
    {
        parent::testAddUntaggedVlanOnPort([
            [10, 555],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testAddTaggedVlanOnPortTest()
    {
        parent::testAddTaggedVlanOnPort([
            [11, 777],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testIsTaggedTest()
    {
        parent::testIsTagged([
            [11],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testDeleteVlanOnPortTest()
    {
        parent::testDeleteVlanOnPort([
            [10, 555],
            [11, 777]
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testSetGvrpPvidTest()
    {
        parent::testSetGvrpPvid([
            [10, 555, null],
        ]);
    }

    /**
     * @group switches\dlink\DGS121020ME\B1\vlan
     *
     */
    public function testDeleteVlanTest()
    {
        parent::testDeleteVlanMe([
            [555],
            [777],
        ]);
    }

}
