<?php

namespace tests\switches\dlink\DES3026;


/**
 * @package tests\switches\dlink\DES3026
 */
class BandwidthTest extends \tests\switches\BandwidthTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3026']);
        $this->data   = $this->devices['dlink\DES3026'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3026\bandwidth
     *
     */
    public function testgetRxRateTest()
    {
        parent::testgetRxRate(array(
            [1, 1024],
            [2, 10024],
            [17, 0],
        ));
    }

    /**
     * @group switches\dlink\DES3026\bandwidth
     *
     */
    public function testsetRxRateTest()
    {
        parent::testsetRxRate(array(
            [1, 1024],
            [2, 10024],
            [17, 0],
        ));
    }

    /**
     * @group switches\dlink\DES3026\bandwidth
     *
     */
    public function testgetTxRateTest()
    {
        parent::testgetTxRate(array(
            [1, 1024],
            [2, 10024],
            [17, 0],
        ));
    }

    /**
     * @group switches\dlink\DES3026\bandwidth
     *
     */
    public function testsetTxRateTest()
    {
        parent::testsetTxRate(array(
            [1, 1024],
            [2, 10024],
            [17, 0],
        ));
    }

}
