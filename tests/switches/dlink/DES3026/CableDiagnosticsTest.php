<?php

namespace tests\switches\dlink\DES3026;


/**
 * @package tests\switches\dlink\DES3026
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3026']);
        $this->data   = $this->devices['dlink\DES3026'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3026\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [1, null],
            [16, null],
        ));
    }

    /**
     * @group switches\dlink\DES3026\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        parent::testGetState(array(
            [1, null],
            [16, null],
        ));
    }

    /**
     * @group switches\dlink\DES3026\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            16,
        ));
    }

}
