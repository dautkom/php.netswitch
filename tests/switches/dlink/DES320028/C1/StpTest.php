<?php

namespace tests\switches\dlink\DES320028\C1;


/**
 * @package tests\switches\dlink\DES320028\C1
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028\C1']);
        $this->data   = $this->devices['dlink\DES320028\C1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            32768,
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,32768],
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            26,
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            '90ef68c8ecf8',
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            null,
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetSwitchMacTest()
    {
        parent::testGetSwitchMac(array(
            'd8fee3878900',
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\dlink\DES320028\C1\stp
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

