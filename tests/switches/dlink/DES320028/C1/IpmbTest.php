<?php

namespace tests\switches\dlink\DES320028\C1;


/**
 * @package tests\switches\dlink\DES320028\C1
 */
class IpmbTest extends \tests\switches\IpmbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip = key($this->devices['dlink\DES320028\C1']);
        $this->data = $this->devices['dlink\DES320028\C1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028\C1\ipmb
     */
    public function testGetStateTest()
    {
        parent::testGetState(array(
            [6, true],
            [2, false],
        ));
    }
}
