<?php

namespace tests\switches\dlink\DES320028;


/**
 * @package tests\switches\dlink\DES320028
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028']);
        $this->data   = $this->devices['dlink\DES320028'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [1, true],
            [5, true],
        ));
    }

    /**
     * @group switches\dlink\DES320028\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        // Test array
        $link_full_100        = [1 => '0', 2 => '0', 3 => '8', 4 => '8'];
        $no_link_100          = [1 => '7', 2 => '7', 3 => '8', 4 => '8'];
        $no_link_1000_combo   = [1 => '7', 2 => '7', 3 => '7', 4 => '7'];

        parent::testGetState(array(
            [5, $link_full_100],
            [1, $no_link_100],
            [27, $no_link_1000_combo],
        ));
    }

    /**
     * @group switches\dlink\DES320028\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            5,
            27,
        ));
    }

}
