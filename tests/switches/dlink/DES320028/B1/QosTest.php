<?php

namespace tests\switches\dlink\DES320028\B1;


/**
 * @package tests\switches\dlink\DES320028\B1
 */
class QosTest extends \tests\switches\QosTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028\B1']);
        $this->data   = $this->devices['dlink\DES320028\B1'][$this->ip];
        $this->vendor = 'dlink';

    }


    /**
     * @group switches\dlink\DES320028\B1\qos
     *
     */
    public function testGetQosTest()
    {
        parent::testGetQos(array(
            [1, 0],
        ));
    }

    /**
     * @group switches\dlink\DES320028\B1\qos
     *
     */
    public function testSetQosTest()
    {
        parent::testSetQos(array(
            [1, 0],
            [27, 1],
            [3, 2],
            [4, 7],
            [27, 3],
        ));
    }

}
