<?php

namespace tests\switches\dlink\DES320028;


/**
 * @package tests\switches\dlink\DES320028
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES320028']);
        $this->data   = $this->devices['dlink\DES320028'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('DES320028');
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('dlink\DES320028');
    }

    /**
     * @group switches\dlink\DES320028\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Dlink');
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision("Build 1.50.B002");
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision("A1");
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber("PVI41AB004966");
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\dlink\DES320028\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\dlink\DES320028\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
