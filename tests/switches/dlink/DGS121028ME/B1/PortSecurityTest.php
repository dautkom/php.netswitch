<?php

namespace tests\switches\dlink\DGS121028ME\B1;


/**
 * @package tests\switches\dlink\DGS121028ME\B1
 */
class PortSecurityTest extends \tests\switches\PortSecurityTest
{

    /**
     * @ignore
     */
    public function __construct()
    {
        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121028ME\B1']);
        $this->data   = $this->devices['dlink\DGS121028ME\B1'][$this->ip];
        $this->vendor = 'dlink';
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\port_security
     *
     */
    public function testsetStateTest()
    {
        parent::testsetState([
            [1, 1],
            [2, 2],
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\port_security
     *
     */
    public function testgetStateTest()
    {
        parent::testgetState([
            [1, 1],
            [2, 2],
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\port_security
     *
     */
    public function testsetMaxAddressesTest()
    {
        parent::testsetMaxAddresses([
            [1, 0],
            [2, 64],
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\port_security
     *
     */
    public function testgetMaxAddressesTest()
    {
        parent::testgetMaxAddresses([
            [1, 0],
            [2, 64],
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\port_security
     *
     */
    public function testsetModeTest()
    {
        parent::testsetMode([
            [14, true, 2],
            [15, true, 3],
            [16, true, 4],
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\port_security
     *
     */
    public function testgetModeTest()
    {
        parent::testgetMode([
            [14, 2],
            [15, 3],
            [16, 4],
        ]);
    }

}
