<?php

namespace tests\switches\dlink\DGS121028ME\B1;


/**
 * @package tests\switches\dlink\DGS121028ME\B1
 */
class IpmbTest extends \tests\switches\IpmbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip = key($this->devices['dlink\DGS121028ME\B1']);
        $this->data = $this->devices['dlink\DGS121028ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121028ME\B1\ipmb
     *
     */
    public function testGetStateTest()
    {
        parent::testGetState(array(
            [1, false],
            [2, false],
        ));
    }
}
