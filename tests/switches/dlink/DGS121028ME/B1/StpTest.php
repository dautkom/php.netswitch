<?php

namespace tests\switches\dlink\DGS121028ME\B1;


/**
 * @package tests\switches\dlink\DGS121028ME\B1
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS121028ME\B1']);
        $this->data   = $this->devices['dlink\DGS121028ME\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority([
            28672,
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority([
            [true, 28672],
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort([0]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus([1]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac([null]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac([null]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetSwitchMacTest()
    {
        parent::testGetSwitchMac(['c4e90ab43c10']);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort([
            [1, 2]
        ]);
    }

    /**
     * @group switches\dlink\DGS121028ME\B1\stp
     *
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort([
            [1, 1, true],
            [1, 2, true]
        ]);
    }

}

