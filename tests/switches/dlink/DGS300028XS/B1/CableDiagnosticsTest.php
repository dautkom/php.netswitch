<?php

namespace tests\switches\dlink\DGS300028XS\B1;


/**
 * @package tests\switches\dlink\DGS300028XS\B1
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS300028XS\B1']);
        $this->data   = $this->devices['dlink\DGS300028XS\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS300028XS\B1\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit([
            [1, false],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        parent::testGetState([
            [26, null],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength([1, 21]);
    }

}
