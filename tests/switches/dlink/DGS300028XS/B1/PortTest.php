<?php

namespace tests\switches\dlink\DGS300028XS\B1;

use PHPUnit\Framework\Error\Error;
use RuntimeException;

/**
 * @package tests\switches\dlink\DGS300028XS\B1
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS300028XS\B1']);
        $this->data   = $this->devices['dlink\DGS300028XS\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     *
     *
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testSetPortNumberExceptionTest()
    {
        $this->expectExceptionMessage("Wrong port specified");
        $this->expectException(RuntimeException::class);
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testPortNumberNotSetErrorTest()
    {
        $this->expectException(Error::class);
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription([
            [1, ''],
            [25, '[mon]'],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     *
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription([
            [1, ''],
            [25, '[mon]']
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     *
     */
    public function testCountTest()
    {
        parent::testCount(28);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     *
     */
    public function testGetLinkTest()
    {
        parent::testGetLink([
            [4, 0],  // 0: link down
            [27, 6], // 6: full-1Gbps
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed([
            [27, 1000000000], // 1Gbps link
            [4,  0],          // no-link
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus([
            [4, 2], // Link down
            [27, 1]  // Link up
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo([
            [1, false],
            [28, false]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo([
            [1, false],
            [28, false]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType([
            [1, 1],
            [28, 1],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testCompileGetMediumTypeTest()
    {
        parent::testCompileGetMediumType([
            [1, 1],
            [28, 1],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     *
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState([
            [7,  2],
            [16, 2],
            [21, 1]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState([
            [7, 2],
            [21, 1]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testsetFlowControlStateTest()
    {
        parent::testsetFlowControlState([
            [1, 2],
            [14, 1],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testgetFlowControlStateTest()
    {
        parent::testgetFlowControlState([
            [1, 2],
            [14, 1]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState([
            [1, 1, true],
            [16, 2, true],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState([
            [1, 1],
            [16, 2],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     */
    public function testClearStatsTest()
    {
        parent::testClearStats();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\port
     *
     */
    public function testGetDuplexTest()
    {
        parent::testGetDuplex([
            [4, 1],
            [27, 3],
        ]);
    }

}
