<?php
namespace tests\switches\dlink\DGS300028XS\B1;


/**
 * @package tests\switches\dlink\DGS300028XS\B1
 */
class VlanTest extends \tests\switches\VlanTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS300028XS\B1']);
        $this->data   = $this->devices['dlink\DGS300028XS\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testValidateVIDTest()
    {
        parent::testValidateVID([
            [true,  777],
            [false, 7771],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     */
    public function testGetAllTest()
    {
        parent::testGetAll();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testCreateVlanTest()
    {
        parent::testCreateVlan([
            [555, 'TestVlan555'],
            [777, 'TestVlan777'],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testsetTransitTest()
    {
        parent::testsetTransit([
            [16],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testIsTransitTest()
    {
        parent::testIsTransit([
            [16],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testClearVlansOnPortTest()
    {
        parent::testClearVlansOnPort([
            [14], [15], [16]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testAddUntaggedVlanOnPortTest()
    {
        parent::testAddUntaggedVlanOnPort([
            [14, 555],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testAddTaggedVlanOnPortTest()
    {
        parent::testAddTaggedVlanOnPort([
            [15, 777],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testIsTaggedTest()
    {
        parent::testIsTagged([
            [15],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testDeleteVlanOnPortTest()
    {
        parent::testDeleteVlanOnPort([
            [14, 555],
            [15, 777]
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testSetGvrpPvidTest()
    {
        parent::testSetGvrpPvid([
            [16, 555, true],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\vlan
     *
     */
    public function testDeleteVlanTest()
    {
        parent::testDeleteVlanMe([
            [555],
            [777],
        ]);
    }

}
