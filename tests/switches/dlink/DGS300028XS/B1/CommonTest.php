<?php

namespace tests\switches\dlink\DGS300028XS\B1;


/**
 * @package tests\switches\dlink\DGS300028XS\B1
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS300028XS\B1']);
        $this->data   = $this->devices['dlink\DGS300028XS\B1'][$this->ip];
        $this->vendor = 'dlink';
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('DGS300028XS\B1');
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('dlink\DGS300028XS\B1');
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Dlink');
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision("4.12.B007");
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision("B1");
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber("SY34103000208");
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
