<?php

namespace tests\switches\dlink\DGS300028XS\B1;


/**
 * @package tests\switches\dlink\DGS300028XS\B1
 */
class QosTest extends \tests\switches\QosTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS300028XS\B1']);
        $this->data   = $this->devices['dlink\DGS300028XS\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS300028XS\B1\qos
     *
     */
    public function testSetQosTest()
    {
        parent::testSetQos([
            [1, 0],
            [20, 1],
            [3, 2],
            [4, 7],
        ]);
    }

    /**
     * @group switches\dlink\DGS300028XS\B1\qos
     *
     */
    public function testGetQosTest()
    {
        parent::testGetQos([
            [1, 0],
            [20, 1],
            [3, 2],
            [4, 7],
        ]);
    }



}
