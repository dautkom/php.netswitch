<?php

namespace tests\switches\dlink\DES3526;


/**
 * @package tests\switches\dlink\DES3526
 */
class IpmbTest extends \tests\switches\IpmbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip = key($this->devices['dlink\DES3526']);
        $this->data = $this->devices['dlink\DES3526'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3526\ipmb
     *
     */
    public function testGetStateTest()
    {
        parent::testGetState(array(
            [6, true],
            [2, false],
        ));
    }
}
