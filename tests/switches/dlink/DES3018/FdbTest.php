<?php

namespace tests\switches\dlink\DES3018;


/**
 * @package tests\switches\dlink\DES3018
 */
class FdbTest extends \tests\switches\FdbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3018']);
        $this->data   = $this->devices['dlink\DES3018'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3018\fdb
     */
    public function testShowAllPermanentTest()
    {
        parent::testShowAllPermanent();
    }

    /**
     * @group switches\dlink\DES3018\fdb
     *
     */
    public function testShowPortPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc4', 1 => '992' ]];

        parent::testShowPortPermanent(array(
            [15, $testarr]
        ));
    }

    /**
     * @group switches\dlink\DES3018\fdb
     *
     */
    public function testshowPortAllTest()
    {
        // Test array
        $testarr = [0 => [ 'vid' => '992', 'vlan' => 'Metro', 'state' => 'Permanent', 'mac' => '000b82393fc4' ]];

        parent::testShowPortAll(array(
            [15, $testarr, '', '']
        ));
    }

    /**
     * @group switches\dlink\DES3018\fdb
     *
     */
    public function testAddPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc3', 1 => '992' ]];

        parent::testAddPermanent(array(
            [14, '00-0b-82-39-3f-c3', 992, $testarr]
        ));
    }

    /**
     * @group switches\dlink\DES3018\fdb
     *
     */
    public function testDeletePermanentTest()
    {
        parent::testDeletePermanent(array(
            [14, '00:0b:82:39:3f:c3', 992]
        ));
    }

    /**
     * @group switches\dlink\DES3018\fdb
     *
     */
    public function testDeleteAllPermanentTest()
    {
        parent::testDeleteAllPermanent(array(
            [11, '00:0b:82:39:3b:c1', 992],
            [11, '00:0b:82:39:3b:c2', 992]

        ));
    }

}
