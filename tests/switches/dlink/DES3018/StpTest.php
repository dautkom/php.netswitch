<?php

namespace tests\switches\dlink\DES3018;


/**
 * @package tests\switches\dlink\DES3018
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3018']);
        $this->data   = $this->devices['dlink\DES3018'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            32768,
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,32768],
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            17,
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            'd8fee3923380',
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            null,
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetSwitchMacTest()
    {
        parent::testGetSwitchMac(array(
            '0013466a836d',
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\dlink\DES3018\stp
     *
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

