<?php

namespace tests\switches\dlink\DES3018;


/**
 * @package tests\switches\dlink\DES3018
 */
class PortSecurityTest extends \tests\switches\PortSecurityTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3018']);
        $this->data   = $this->devices['dlink\DES3018'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3018\port_security
     *
     */
    public function testgetStateTest()
    {
        parent::testgetState(array(
            [1, 1],
            [2, 2],
        ));
    }

    /**
     * @group switches\dlink\DES3018\port_security
     *
     */
    public function testsetStateTest()
    {
        parent::testsetState(array(
            [1, 1],
            [2, 2],
        ));
    }

    /**
     * @group switches\dlink\DES3018\port_security
     *
     */
    public function testgetMaxAddressesTest()
    {
        parent::testgetMaxAddresses(array(
            [1, 10],
            [15, 0],
        ));
    }

    /**
     * @group switches\dlink\DES3018\port_security
     *
     */
    public function testsetMaxAddressesTest()
    {
        parent::testsetMaxAddresses(array(
            [1, 10],
            [15, 0],
        ));
    }

    /**
     * @group switches\dlink\DES3018\port_security
     *
     */
    public function testgetModeTest()
    {
        parent::testgetMode(array(
            [14, 2],
            [15, 3],
            [16, 4],
        ));
    }

    /**
     * @group switches\dlink\DES3018\port_security
     *
     */
    public function testsetModeTest()
    {
        parent::testsetMode(array(
            [14, true, 2],
            [15, true, 3],
            [16, true, 4],
        ));
    }

}
