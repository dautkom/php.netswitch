<?php

namespace tests\switches\dlink\DES3018;


/**
 * @package tests\switches\dlink\DES3018
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3018']);
        $this->data   = $this->devices['dlink\DES3018'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3018\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [1, null],
            [16, null],
        ));
    }

    /**
     * @group switches\dlink\DES3018\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {

        parent::testGetState(array(
            [1, null],
            [16, null],
        ));
    }

    /**
     * @group switches\dlink\DES3018\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            16,
        ));
    }

}
