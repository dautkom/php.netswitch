<?php

namespace tests\switches\dlink\DES3550;


/**
 * @package tests\switches\dlink\DES3550
 */
class IpmbTest extends \tests\switches\IpmbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip = key($this->devices['dlink\DES3550']);
        $this->data = $this->devices['dlink\DES3550'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3550\ipmb
     *
     */
    public function testGetStateTest()
    {
        parent::testGetState(array(
            [6, true],
            [2, false],
        ));
    }
}
