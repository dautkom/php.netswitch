<?php

namespace tests\switches\dlink\DES3550;


/**
 * @package tests\switches\dlink\DES3550
 */
class QosTest extends \tests\switches\QosTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3550']);
        $this->data   = $this->devices['dlink\DES3550'][$this->ip];
        $this->vendor = 'dlink';

    }


    /**
     * @group switches\dlink\DES3550\qos
     *
     */
    public function testGetQosTest()
    {
        parent::testGetQos(array(
            [1, 0],
        ));
    }

    /**
     * @group switches\dlink\DES3550\qos
     *
     */
    public function testSetQosTest()
    {
        parent::testSetQos(array(
            [1, 0],
            [49, 1],
            [3, 2],
            [4, 7],
            [49, 3],
        ));
    }

}
