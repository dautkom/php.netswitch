<?php

namespace tests\switches\dlink\DES3550;


/**
 * @package tests\switches\dlink\DES3550
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DES3550']);
        $this->data   = $this->devices['dlink\DES3550'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('DES3550');
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('dlink\DES3550');
    }

    /**
     * @group switches\dlink\DES3550\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Dlink');
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision("Build 6.00.B51");
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision("A5G");
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber(null);
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\dlink\DES3550\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\dlink\DES3550\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
