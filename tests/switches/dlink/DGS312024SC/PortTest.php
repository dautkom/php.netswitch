<?php

namespace tests\switches\dlink\DGS312024SC;


/**
 * @package tests\switches\dlink\DGS312024SC
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC']);
        $this->data   = $this->devices['dlink\DGS312024SC'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     *
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Wrong port specified
     */
    public function testSetPortNumberExceptionTest()
    {
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testPortNumberNotSetErrorTest()
    {
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription(array(
            [1, ''],
            [24, '[mon] [stp]']
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testCountTest()
    {
        parent::testCount(26);
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetLinkTest()
    {
        parent::testGetLink(array(
            [1, 0],  // 0: link down
            [2, 1],  // 1: half-10Mbps
            [3, 2],  // 2: full-10Mbps
            [4, 3],  // 3: half-100Mbps
            [5, 4],  // 4: full-100Mbps
            [16, 0], // 0: fiber link down
            [24, 6]  // 6: full-1Gbps
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription(array(
            [1, ''],
            [24, '[mon] [stp]'],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed(array(
            [1,  0],            // link down no speed
            [2,  10000000],     // 10Mmbs link
            [4,  100000000],    // 100Mbs link
            [16, 0],            // fiber link down no speed
            [24, 1000000000],   // 1Gbs link
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus(array(
            [1, 2], // Link down
            [2, 1]  // Link up
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo(array(
            [1, true],
            [9, false]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo(array(
            [1, true],
            [9, false]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType(array(
            [1, 0],
            [9, 1],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testCompileGetMediumTypeTest()
    {
        parent::testCompileGetMediumType(array(
            [1, 0],
            [9, 1],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState(array(
            [7, 2],
            [21, 1]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState(array(
            [7,  2],
            [16, 2],
            [21, 1]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testgetFlowControlStateTest()
    {
        parent::testgetFlowControlState(array(
            [1, 2],
            [14, 1]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testsetFlowControlStateTest()
    {
        parent::testsetFlowControlState(array(
            [1, 2],
            [14, 1],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState(array(
            [1, 1],
            [16, 2],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState(array(
            [1, 1, true],
            [16, 2, true],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     */
    public function testClearStatsTest()
    {
        parent::testClearStats();
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetDuplexTest()
    {
        parent::testGetDuplex(array(
            [2, 2],
            [3, 3]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\port
     *
     */
    public function testGetPortFromFdbTest()
    {
        parent::testGetPortFromFdb(array(
            [992, '00-0B-82-39-3F-C4' , 15],
        ));
    }

}
