<?php

namespace tests\switches\dlink\DGS312024SC;


/**
 * @package tests\switches\dlink\DGS312024SC
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC']);
        $this->data   = $this->devices['dlink\DGS312024SC'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('DGS312024SC');
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('dlink\DGS312024SC');
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Dlink');
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision("3.00.B026");
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision("A2");
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber("R3143CC000046");
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\dlink\DGS312024SC\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
