<?php

namespace tests\switches\dlink\DGS312024SC;


/**
 * @package tests\switches\dlink\DGS312024SC
 */
class FdbTest extends \tests\switches\FdbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC']);
        $this->data   = $this->devices['dlink\DGS312024SC'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS312024SC\fdb
     */
    public function testShowAllPermanentTest()
    {
        parent::testShowAllPermanent();
    }

    /**
     * @group switches\dlink\DGS312024SC\fdb
     *
     */
    public function testShowPortPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc4', 1 => '992' ]];

        parent::testShowPortPermanent(array(
            [15, $testarr]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\fdb
     *
     */
    public function testshowPortAllTest()
    {
        // Test array
        $testarr = [0 => [ 'vid' => '992', 'vlan' => 'Metro', 'state' => 'Static', 'mac' => '000b82393fc4' ]];

        parent::testShowPortAll(array(
            [15, $testarr, '', '']
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\fdb
     *
     */
    public function testAddPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc2', 1 => '992' ]];

        parent::testAddPermanent(array(
            [14, '00:0b:82:39:3f:c2', 992, $testarr]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\fdb
     *
     */
    public function testDeletePermanentTest()
    {
        parent::testDeletePermanent(array(
            [14, '00-0b-82-39-3f-c2', 992]
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\fdb
     *
     */
    public function testDeleteAllPermanentTest()
    {
        parent::testDeleteAllPermanent(array(
            [11, '00:0b:82:39:3b:c1', 992],
            [11, '00:0b:82:39:3b:c2', 992]

        ));
    }

}
