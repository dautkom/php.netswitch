<?php

namespace tests\switches\dlink\DGS312024SC;


/**
 * @package tests\switches\dlink\DGS312024SC
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC']);
        $this->data   = $this->devices['dlink\DGS312024SC'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS312024SC\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [21, false],
            [1, true],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        // Test array
        $no_link_1000_combo   = [1 => '1', 2 => '1', 3 => '1', 4 => '1'];
        $optic                = null;

        parent::testGetState(array(
            [21, $optic],
            [1, $no_link_1000_combo],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            21,
        ));
    }

}
