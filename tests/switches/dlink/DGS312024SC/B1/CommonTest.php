<?php

namespace tests\switches\dlink\DGS312024SC\B1;


/**
 * @package tests\switches\dlink\DGS312024SC\B1
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC\B1']);
        $this->data   = $this->devices['dlink\DGS312024SC\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('DGS312024SC\B1');
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('dlink\DGS312024SC\B1');
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Dlink');
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision("3.00.B538");
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision("B1");
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber("R3144D7002120");
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
