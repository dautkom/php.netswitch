<?php

namespace tests\switches\dlink\DGS312024SC\B1;


/**
 * @package tests\switches\dlink\DGS312024SC\B1
 */
class QosTest extends \tests\switches\QosTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC\B1']);
        $this->data   = $this->devices['dlink\DGS312024SC\B1'][$this->ip];
        $this->vendor = 'dlink';

    }


    /**
     * @group switches\dlink\DGS312024SC\B1\qos
     *
     */
    public function testGetQosTest()
    {
        parent::testGetQos(array(
            [1, 0],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\qos
     *
     */
    public function testSetQosTest()
    {
        parent::testSetQos(array(
            [1, 0],
            [23, 1],
            [3, 2],
            [4, 7],
            [23, 3],
        ));
    }

}
