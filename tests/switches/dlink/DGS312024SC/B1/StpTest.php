<?php

namespace tests\switches\dlink\DGS312024SC\B1;


/**
 * @package tests\switches\dlink\DGS312024SC\B1
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['dlink\DGS312024SC\B1']);
        $this->data   = $this->devices['dlink\DGS312024SC\B1'][$this->ip];
        $this->vendor = 'dlink';

    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            28672,
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,28672],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            0,
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            null,
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            null,
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetSwitchMacTest()
    {
        parent::testGetSwitchMac(array(
            'd8fee3923380',
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\dlink\DGS312024SC\B1\stp
     *
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

