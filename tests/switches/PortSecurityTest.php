<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class PortSecurityTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->PortSecurity->getState());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->PortSecurity->setState($subject[1]));
            $data1 = $this->obj->PortSecurity->getState();
            $this->assertEquals($subject[1], $data1);
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetMaxAddresses($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->PortSecurity->getMaxAddresses());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetMaxAddresses($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->PortSecurity->setMaxAddresses($subject[1]));
            $data1 = $this->obj->PortSecurity->getMaxAddresses();
            $this->assertEquals($subject[1], $data1);
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetMode($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->PortSecurity->getMode());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetMode($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->PortSecurity->setMode($subject[2]));
            $data1 = $this->obj->PortSecurity->getMode();
            $this->assertEquals($subject[2], $data1);
        }
    }

}
