<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class CommonTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $expected
     */
    public function testGetModel($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->getModel());
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testGetModelId($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->getModelId());
    }

    /**
     * @group switches\general
     */
    public function testGetIni()
    {
        $this->connect();
        $this->assertEquals(true, is_array($this->obj->getIni()));
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testGetVendor($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->getVendor());
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testGetSoftwareRevision($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->getSoftwareRevision());
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testGetHardwareRevision($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->getHardwareRevision());
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testGetSerialNumber($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->getSerialNumber());
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testLocation($expected)
    {
        $this->connect();
        $data1 = $this->obj->getLocation();
        $this->assertEquals($expected, $data1);
        $this->assertEquals(true, $this->obj->setLocation($expected));
        $data2 = $this->obj->getLocation();
        $this->assertEquals($expected, $data2);
        $this->assertEquals($data1, $data2);
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testName($expected)
    {
        $this->connect();
        $data1 = $this->obj->getName();
        $this->assertEquals($expected, $data1);
        $this->assertEquals(true, $this->obj->setName($expected));
        $data2 = $this->obj->getName();
        $this->assertEquals($expected, $data2);
        $this->assertEquals($data1, $data2);
    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testContact($expected)
    {
        $this->connect();
        $data1 = $this->obj->getContact();
        $this->assertEquals($expected, $data1);
        $this->assertEquals(true, $this->obj->setContact($expected));
        $data2 = $this->obj->getContact();
        $this->assertEquals($expected, $data2);
        $this->assertEquals($data1, $data2);
    }

    /**
     * @group switches\general
     */
    public function testUptime()
    {
        $this->connect();

        $u1 = $this->obj->getUptime();
        $u2 = $this->obj->getUptime(true);

        $this->assertEquals(true, is_string($u1));
        $this->assertEquals(true, is_string($u2));
        $this->assertEquals(true, (strlen($u1)>1));
        $this->assertEquals(true, (strlen($u2)>1));
    }

}
