<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class StpTest extends NetSwitch_PHPUnit_Framework_TestCase
{

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetPriority($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals($subject, $this->obj->Stp->getPriority());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testSetPriority($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals($subject[0], $this->obj->Stp->setPriority($subject[1]));
            $this->assertEquals($subject[1], $this->obj->Stp->getPriority());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetRootPort($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals($subject, $this->obj->Stp->getRootPort());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetStpStatus($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertSame($subject, $this->obj->Stp->getStpStatus());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetRootMac($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals($subject, $this->obj->Stp->getRootMac());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetDesignatedBridgeMac($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals($subject, $this->obj->Stp->getDesignatedBridgeMac());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetSwitchMac($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->assertEquals($subject, $this->obj->Stp->getSwitchMac());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetStateOnPort($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertSame($subject[1], $this->obj->Stp->getStateOnPort());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testSetStateOnPort($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertSame($subject[2], $this->obj->Stp->setStateOnPort($subject[1]));
            $this->assertSame($subject[1], $this->obj->Stp->getStateOnPort());
        }
    }

}
