<?php

namespace tests\switches;
use tests\NetSwitch_PHPUnit_Framework_TestCase;


/**
 * @package tests\switches
 */
class PortTest extends NetSwitch_PHPUnit_Framework_TestCase
{


    /**
     * @group switches\general
     * @param $port
     * @param $expected
     */
    public function testSetAndGetPortNumber($port, $expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->Port->setPortNumber($port));
        $this->assertEquals($expected, $this->obj->Port->getPortNumber());
    }

    /**
     * Test exception if trying to compile with unsupported vendor\model
     *
     * @group switches\general
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Wrong port specified
     */
    public function testSetPortNumberException()
    {
        $this->connect();
        $this->obj->Port->setPortNumber(999);
    }

    /**
     * @group switches\general
     */
    public function testGetDataFlow()
    {
        $this->connect();
        $this->obj->Port->setPortNumber(1);
        $data = $this->obj->Port->getDataFlow();

        $this->assertArrayHasKey('in', $data);
        $this->assertArrayHasKey('out', $data);
        $this->assertArrayHasKey('unicast', $data['in']);
        $this->assertArrayHasKey('nonunicast', $data['in']);
        $this->assertArrayHasKey('unicast', $data['out']);
        $this->assertArrayHasKey('nonunicast', $data['out']);
    }

    /**
     * @group switches\general
     */
    public function testGetErrors()
    {
        $this->connect();
        $this->obj->Port->setPortNumber(1);
        $data = $this->obj->Port->getErrors();

        $this->assertArrayHasKey('in', $data);
        $this->assertArrayHasKey('out', $data);
        $this->assertArrayHasKey('errors', $data['in']);
        $this->assertArrayHasKey('discards', $data['in']);
        $this->assertArrayHasKey('errors', $data['out']);
        $this->assertArrayHasKey('discards', $data['out']);
    }

    /**
     * @group switches\general
     * @expectedException \PHPUnit\Framework\Exception
     */
    public function testPortNumberNotSetError()
    {
        $this->connect();
        $this->obj->Port->getErrors();
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetDescription($testset)
    {

        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getDescription());
        }

    }

    /**
     * @group switches\general
     * @param $expected
     */
    public function testCount($expected)
    {
        $this->connect();
        $this->assertEquals($expected, $this->obj->Port->count());
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetLink($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getLink());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testSetDescription($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Port->setDescription($subject[1]));
            $data1 = $this->obj->Port->getDescription();
            $this->assertEquals($subject[1], $data1);
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetOperatingSpeed($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getOperatingSpeed());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetOperatingStatus($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getOperatingStatus());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testConnectedIsCombo($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->isCombo());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testCompileIsCombo($testset)
    {
        $this->compile();

        foreach($testset as $subject) {
            $this->assertEquals($subject[1], $this->obj->Port->isCombo($subject[0]));
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetMediumType($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getMediumType());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testCompileGetMediumType($testset)
    {
        $this->compile();

        foreach($testset as $subject) {
            $this->assertEquals($subject[1], $this->obj->Port->getMediumType($subject[0]));
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetAdminState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getAdminState());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testSetAdminState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Port->setAdminState($subject[1]));
            $data1 = $this->obj->Port->getAdminState();
            $this->assertEquals($subject[1], $data1);
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetFlowControlState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getFlowControlState());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetFlowControlState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Port->setFlowControlState($subject[1]));
            $data1 = $this->obj->Port->getFlowControlState();
            $this->assertEquals($subject[1], $data1);
        }

    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testgetLearningState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getLearningState());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testsetLearningState($testset)
    {
        $this->connect();

        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[2], $this->obj->Port->setLearningState($subject[1]));
            $data1 = $this->obj->Port->getLearningState();
            $this->assertEquals($subject[1], $data1);
        }

    }

    /**
     * @group switches\general
     */
    public function testClearStats()
    {
        $this->connect();
        $this->assertEquals(true, $this->obj->Port->clearStats());
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testClearStatsOnPort($testset)
    {
        $this->connect();
        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals(true, $this->obj->Port->clearStats());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetDuplex($testset)
    {
        $this->connect();
        foreach($testset as $subject) {
            $this->obj->Port->setPortNumber($subject[0]);
            $this->assertEquals($subject[1], $this->obj->Port->getDuplex());
        }
    }

    /**
     * @group switches\general
     * @param $testset
     */
    public function testGetPortFromFdb($testset)
    {
        $this->connect();
        foreach($testset as $subject) {
            $this->assertSame($subject[2], $this->obj->Port->getPortFromFdb( $subject[0], $subject[1] ));
        }
    }

}
