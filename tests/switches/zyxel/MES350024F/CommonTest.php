<?php

namespace tests\switches\zyxel\MES350024F;

/**
 * @package tests\switches\zyxel\MES350024F
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MES350024F']);
        $this->data   = $this->devices['zyxel\MES350024F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('MES350024F');
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('zyxel\MES350024F');
    }

    /**
     * @group switches\zyxel\MES350024F\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Zyxel');
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision('V4.0(AABG.3)');
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision('1.0');
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber('S140H18000297');
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\zyxel\MES350024F\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\zyxel\MES350024F\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
