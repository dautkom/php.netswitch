<?php

namespace tests\switches\zyxel\MES350024F;


/**
 * @package tests\switches\zyxel\MES350024F
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MES350024F']);
        $this->data   = $this->devices['zyxel\MES350024F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            32768,
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,32768],
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            28,
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            'ccb2557d7080',
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            'NULL',
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\stp
     *
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

