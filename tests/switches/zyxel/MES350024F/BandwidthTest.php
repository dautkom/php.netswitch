<?php

namespace tests\switches\zyxel\MES350024F;


/**
 * @package tests\switches\zyxel\MES350024F
 */
class BandwidthTest extends \tests\switches\BandwidthTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MES350024F']);
        $this->data   = $this->devices['zyxel\MES350024F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MES350024F\bandwidth
     *
     */
    public function testgetRxRateTest()
    {
        parent::testgetRxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\bandwidth
     *
     */
    public function testsetRxRateTest()
    {
        parent::testsetRxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\bandwidth
     *
     */
    public function testgetTxRateTest()
    {
        parent::testgetTxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\MES350024F\bandwidth
     *
     */
    public function testsetTxRateTest()
    {
        parent::testsetTxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

}
