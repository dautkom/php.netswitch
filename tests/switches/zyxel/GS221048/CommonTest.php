<?php

namespace tests\switches\zyxel\GS221048;

/**
 * @package tests\switches\zyxel\GS221048
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS221048']);
        $this->data   = $this->devices['zyxel\GS221048'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('GS221048');
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('zyxel\GS221048');
    }

    /**
     * @group switches\zyxel\GS221048\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Zyxel');
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision('V4.30(AAHV.0) | 09/07/2015');
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision('1.3');
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber('S142L46001087');
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\zyxel\GS221048\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\zyxel\GS221048\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
