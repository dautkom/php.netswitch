<?php

namespace tests\switches\zyxel\GS221024;


/**
 * @package tests\switches\zyxel\GS221024
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS221024']);
        $this->data   = $this->devices['zyxel\GS221024'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            32768,
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,32768],
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            28,
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            'd8fee3878900',
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            null,
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\zyxel\GS221024\stp
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

