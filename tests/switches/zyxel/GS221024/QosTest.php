<?php

namespace tests\switches\zyxel\GS221024;


/**
 * @package tests\switches\zyxel\GS221024
 */
class QosTest extends \tests\switches\QosTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS221024']);
        $this->data   = $this->devices['zyxel\GS221024'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS221024\qos
     */
    public function testGetQosTest()
    {
        parent::testGetQos(array(
            [1, 0],
        ));
    }

    /**
     * @group switches\zyxel\GS221024\qos
     */
    public function testSetQosTest()
    {
        parent::testSetQos(array(
            [1, 0],
            [2, 1],
            [3, 2],
            [4, 7],
            [24, 3],
        ));
    }

}
