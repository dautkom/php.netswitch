<?php

namespace tests\switches\zyxel\XGS221028;


/**
 * @package tests\switches\zyxel\XGS221028
 */
class FdbTest extends \tests\switches\FdbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\XGS221028']);
        $this->data   = $this->devices['zyxel\XGS221028'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\XGS221028\fdb
     */
    public function testShowAllPermanentTest()
    {
        parent::testShowAllPermanent();
    }

    /**
     * @group switches\zyxel\XGS221028\fdb
     */
    public function testShowPortPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc1', 1 => '999' ]];

        parent::testShowPortPermanent(array(
            [15, $testarr]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\fdb
     */
    public function testshowPortAllTest()
    {
        // Test array
        $testarr = [0 => [ 'vid' => '999', 'vlan' => 'Test', 'state' => 'Static', 'mac' => '000b82393fc1' ]];

        parent::testShowPortAll(array(
            [15, $testarr, 'admin', '1234']
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\fdb
     */
    public function testAddPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc2', 1 => '999' ]];

        parent::testAddPermanent(array(
            [16, '00:0b:82:39:3f:c2', 999, $testarr]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\fdb
     */
    public function testDeletePermanentTest()
    {
        parent::testDeletePermanent(array(
            [16, '00-0b-82-39-3f-c2', 999]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\fdb
     */
    public function testDeleteAllPermanentTest()
    {
        parent::testDeleteAllPermanent(array(
            [11, '00:0b:82:39:3b:c1', 999],
            [11, '00:0b:82:39:3b:c2', 999]

        ));
    }

}
