<?php

namespace tests\switches\zyxel\XGS221028;


/**
 * @package tests\switches\zyxel\XGS221028
 */
class PortSecurityTest extends \tests\switches\PortSecurityTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\XGS221028']);
        $this->data   = $this->devices['zyxel\XGS221028'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\XGS221028\port_security
     */
    public function testgetStateTest()
    {
        parent::testgetState(array(
            [1,  2],
            [2,  2],
            [20, 1],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port_security
     */
    public function testsetStateTest()
    {
        parent::testsetState(array(
            [1,  2],
            [20, 1],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port_security
     */
    public function testgetMaxAddressesTest()
    {
        parent::testgetMaxAddresses(array(
            [1,  0],
            [21, 24],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port_security
     */
    public function testsetMaxAddressesTest()
    {
        parent::testsetMaxAddresses(array(
            [1,  0],
            [21, 24],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port_security
     */
    public function testgetModeTest()
    {
        parent::testgetMode(array(
            [14, null]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port_security
     */
    public function testsetModeTest()
    {
        parent::testsetMode(array(
            [14, null, null]
        ));
    }

}
