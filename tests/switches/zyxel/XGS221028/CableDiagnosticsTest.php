<?php

namespace tests\switches\zyxel\XGS221028;


/**
 * @package tests\switches\zyxel\XGS221028
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\XGS221028']);
        $this->data   = $this->devices['zyxel\XGS221028'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\dlink\XGS221028\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [21, false],
            [27, true],
        ));
    }

    /**
     * @group switches\dlink\XGS221028\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        // Test array
        $no_link_1000_combo   = [1 => '7', 2 => '7', 3 => '7', 4 => '7'];
        $optic                = null;

        parent::testGetState(array(
            [21, $optic],
            [27, $no_link_1000_combo],
        ));
    }

    /**
     * @group switches\dlink\XGS221028\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            5,
            27,
        ));
    }

}
