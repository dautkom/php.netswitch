<?php

namespace tests\switches\zyxel\XGS221028;

/**
 * @package tests\switches\zyxel\XGS221028
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\XGS221028']);
        $this->data   = $this->devices['zyxel\XGS221028'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('XGS221028');
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('zyxel\XGS221028');
    }

    /**
     * @group switches\zyxel\XGS221028\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Zyxel');
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision('V4.30(AAZJ.0) | 03/15/2016');
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision('1.1');
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber('S162L49000395');
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\zyxel\XGS221028\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\zyxel\XGS221028\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
