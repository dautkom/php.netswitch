<?php

namespace tests\switches\zyxel\XGS221028;


/**
 * @package tests\switches\zyxel\XGS221028
 */
class BandwidthTest extends \tests\switches\BandwidthTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\XGS221028']);
        $this->data   = $this->devices['zyxel\XGS221028'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\XGS221028\bandwidth
     */
    public function testsetRxRateTest()
    {
        parent::testsetRxRate(array(
            [1,  0],     # no-limit
            [22, 1024],  # 1 Mbit/s
            [25, 49152], # 48 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\bandwidth
     */
    public function testgetRxRateTest()
    {
        parent::testgetRxRate(array(
            [1,  0],     # no-limit
            [22, 1024],  # 1 Mbit/s
            [25, 49152], # 48 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\bandwidth
     */
    public function testsetTxRateTest()
    {
        parent::testsetTxRate(array(
            [1,  0],     # no-limit
            [22, 1024],  # 1 Mbit/s
            [25, 49152], # 48 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\bandwidth
     */
    public function testgetTxRateTest()
    {
        parent::testgetTxRate(array(
            [1,  0],     # no-limit
            [22, 1024],  # 1 Mbit/s
            [25, 49152], # 48 Mbit/s
        ));
    }

}
