<?php

namespace tests\switches\zyxel\XGS221028;


/**
 * @package tests\switches\zyxel\XGS221028
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\XGS221028']);
        $this->data   = $this->devices['zyxel\XGS221028'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\zyxel\XGS221028\port
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Wrong port specified
     */
    public function testSetPortNumberExceptionTest()
    {
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\zyxel\XGS221028\port
     * @expectedException \PHPUnit\Framework\Error\Warning
     */
    public function testPortNumberNotSetErrorTest()
    {
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription(array(
            [1, ''],
            [28, '[mon] [stp]']
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testCountTest()
    {
        parent::testCount(28);
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetLinkTest()
    {
        parent::testGetLink(array(
            [1, 0], // link down
            [2, 6]  // 1 gbps
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription(array(
            [1, ''],
            [28, '[mon] [stp]'],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed(array(
            [1, 0],            // link down no speed
            [2, 1000000000],   // 1Gbs link
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus(array(
            [1, 2],  // Link down
            [2, 1]   // Link up
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo(array(
            [1, false],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo(array(
            [1, false],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType(array(
            [1, null],
            [2, 0],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState(array(
            [2, 1], // enabled
            [3, 2]  // off
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState(array(
            [3, 2],
            [2, 1]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetFlowControlStateTest()
    {
        parent::testgetFlowControlState(array(
            [1,  1],
            [14, 2]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testSetFlowControlStateTest()
    {
        parent::testsetFlowControlState(array(
            [1,  1],
            [14, 2],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState(array(
            [1, 1],
            [20, 2],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState(array(
            [1, 1, true],
            [20, 2, true],
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testClearStatsOnPortTest()
    {
        parent::testClearStatsOnPort(array(
            [15]
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetDuplexTest()
    {
        parent::testGetDuplex(array(
            [2, '3'],
            [3, '1']
        ));
    }

    /**
     * @group switches\zyxel\XGS221028\port
     */
    public function testGetPortFromFdbTest()
    {
        parent::testGetPortFromFdb(array(
            [999, '00:0b:82:39:3f:c1' , 15],
        ));
    }

}
