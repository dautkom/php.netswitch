<?php

namespace tests\switches\zyxel\MGS352028F;


/**
 * @package tests\switches\zyxel\MGS352028F
 */
class StpTest extends \tests\switches\StpTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MGS352028F']);
        $this->data   = $this->devices['zyxel\MGS352028F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testGetPriorityTest()
    {
        parent::testGetPriority(array(
            32768,
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testSetPriorityTest()
    {
        parent::testSetPriority(array(
            [true,32768],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testGetRootPortTest()
    {
        parent::testGetRootPort(array(
            28,
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testGetStpStatusTest()
    {
        parent::testGetStpStatus(array(
            1,
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testGetRootMacTest()
    {
        parent::testGetRootMac(array(
            'd8fee3878900',
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testGetDesignatedBridgeMacTest()
    {
        parent::testGetDesignatedBridgeMac(array(
            null,
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testGetStateOnPortTest()
    {
        parent::testGetStateOnPort(array(
            [1,2],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\stp
     */
    public function testSetStateOnPortTest()
    {
        parent::testSetStateOnPort(array(
            [1,1,true],
            [1,2,true],
        ));
    }

}

