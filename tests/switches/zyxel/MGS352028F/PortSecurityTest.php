<?php

namespace tests\switches\zyxel\MGS352028F;


/**
 * @package tests\switches\zyxel\MGS352028F
 */
class PortSecurityTest extends \tests\switches\PortSecurityTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MGS352028F']);
        $this->data   = $this->devices['zyxel\MGS352028F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MGS352028F\port_security
     */
    public function testgetStateTest()
    {
        parent::testgetState(array(
            [1, 1],
            [25, 2],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port_security
     */
    public function testsetStateTest()
    {
        parent::testsetState(array(
            [1, 1],
            [25, 2],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port_security
     */
    public function testgetMaxAddressesTest()
    {
        parent::testgetMaxAddresses(array(
            [1, 0],
            [25, 64],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port_security
     */
    public function testsetMaxAddressesTest()
    {
        parent::testsetMaxAddresses(array(
            [1, 0],
            [25, 64],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port_security
     */
    public function testgetModeTest()
    {
        parent::testgetMode(array(
            [14, null]
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port_security
     */
    public function testsetModeTest()
    {
        parent::testsetMode(array(
            [14, null, null]
        ));
    }

}
