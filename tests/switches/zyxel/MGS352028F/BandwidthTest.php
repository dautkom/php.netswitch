<?php

namespace tests\switches\zyxel\MGS352028F;


/**
 * @package tests\switches\zyxel\MGS352028F
 */
class BandwidthTest extends \tests\switches\BandwidthTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MGS352028F']);
        $this->data   = $this->devices['zyxel\MGS352028F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MGS352028F\bandwidth
     */
    public function testgetRxRateTest()
    {
        parent::testgetRxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\bandwidth
     */
    public function testsetRxRateTest()
    {
        parent::testsetRxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\bandwidth
     */
    public function testgetTxRateTest()
    {
        parent::testgetTxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\bandwidth
     */
    public function testsetTxRateTest()
    {
        parent::testsetTxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
            [25, 49152], # 48 # 1 Mbit/s
        ));
    }

}
