<?php

namespace tests\switches\zyxel\MGS352028F;

/**
 * @package tests\switches\zyxel\MGS352028F
 */
class CommonTest extends \tests\switches\CommonTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MGS352028F']);
        $this->data   = $this->devices['zyxel\MGS352028F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testGetModelTest()
    {
        parent::testGetModel('MGS352028F');
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testGetModelIdTest()
    {
        parent::testGetModelId('zyxel\MGS352028F');
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     */
    public function testGetIniTest()
    {
        parent::testGetIni();
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testGetVendorTest()
    {
        parent::testGetVendor('Zyxel');
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testGetSoftwareRevisionTest()
    {
        parent::testGetSoftwareRevision('V4.10(AATM.0) | 07/13/2016');
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testGetHardwareRevisionTest()
    {
        parent::testGetHardwareRevision('1.0');
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testGetSerialNumberTest()
    {
        parent::testGetSerialNumber('S154325005122');
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testLocationTest()
    {
        parent::testLocation($this->data[1]);
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testNameTest()
    {
        parent::testName($this->data[2]);
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     *
     */
    public function testContactTest()
    {
        parent::testContact($this->data[3]);
    }

    /**
     * @group switches\zyxel\MGS352028F\common
     */
    public function testUptimeTest()
    {
        parent::testUptime();
    }

}
