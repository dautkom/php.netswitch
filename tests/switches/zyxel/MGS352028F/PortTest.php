<?php

namespace tests\switches\zyxel\MGS352028F;


/**
 * @package tests\switches\zyxel\MGS352028F
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\MGS352028F']);
        $this->data   = $this->devices['zyxel\MGS352028F'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Wrong port specified
     */
    public function testSetPortNumberExceptionTest()
    {
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testPortNumberNotSetErrorTest()
    {
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription(array(
            [1, ''],
            [28, '[mon] [stp]']
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testCountTest()
    {
        parent::testCount(28);
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetLinkTest()
    {
        parent::testGetLink(array(
            [1, 0], // 0: link down
            [28, 6] // 6: full-1Gbps
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription(array(
            [1, ''],
            [28, '[mon] [stp]'],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed(array(
            [1, 0],             // link down no speed
            [28, 1000000000],   // 1Gbs link
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus(array(
            [1, 2], // Link down
            [28, 1]  // Link up
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo(array(
            [1, false],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo(array(
            [1, false],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType(array(
            [1, 0],
            [28, 0],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testCompileGetMediumTypeTest()
    {
        parent::testCompileGetMediumType(array(
            [1, 0],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState(array(
            [10, 2],
            [25, 1]
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState(array(
            [10, 2],
            [25, 1]
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testgetFlowControlStateTest()
    {
        parent::testgetFlowControlState(array(
            [1, 2],
            [14, 1]
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testsetFlowControlStateTest()
    {
        parent::testsetFlowControlState(array(
            [1, 2],
            [14, 1],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState(array(
            [1, 1],
            [25, 2],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState(array(
            [1, 1, true],
            [25, 2, true],
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testClearStatsOnPortTest()
    {
        parent::testClearStatsOnPort(array(
            [15]
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetDuplexTest()
    {
        parent::testGetDuplex(array(
            [2, 2],
            [3, 3]
        ));
    }

    /**
     * @group switches\zyxel\MGS352028F\port
     */
    public function testGetPortFromFdbTest()
    {
        parent::testGetPortFromFdb(array(
            [999, '00:0b:82:39:3f:c1' , 15],
        ));
    }


}
