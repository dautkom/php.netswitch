<?php

namespace tests\switches\zyxel\GS22108;


/**
 * @package tests\switches\zyxel\GS22108
 */
class BandwidthTest extends \tests\switches\BandwidthTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS22108']);
        $this->data   = $this->devices['zyxel\GS22108'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS22108\bandwidth
     *
     */
    public function testgetRxRateTest()
    {
        parent::testgetRxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\GS22108\bandwidth
     *
     */
    public function testsetRxRateTest()
    {
        parent::testsetRxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\GS22108\bandwidth
     *
     */
    public function testgetTxRateTest()
    {
        parent::testgetTxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
        ));
    }

    /**
     * @group switches\zyxel\GS22108\bandwidth
     *
     */
    public function testsetTxRateTest()
    {
        parent::testsetTxRate(array(
            [1, 0], # no-limit
            [2, 1024], # 1 Mbit/s
        ));
    }

}
