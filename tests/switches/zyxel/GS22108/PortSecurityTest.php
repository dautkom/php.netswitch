<?php

namespace tests\switches\zyxel\GS22108;


/**
 * @package tests\switches\zyxel\GS22108
 */
class PortSecurityTest extends \tests\switches\PortSecurityTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS22108']);
        $this->data   = $this->devices['zyxel\GS22108'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS22108\port_security
     *
     */
    public function testgetStateTest()
    {
        parent::testgetState(array(
            [1, 1],
            [9, 2],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port_security
     *
     */
    public function testsetStateTest()
    {
        parent::testsetState(array(
            [1, 1],
            [9, 2],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port_security
     *
     */
    public function testgetMaxAddressesTest()
    {
        parent::testgetMaxAddresses(array(
            [1, 0],
            [9, 64],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port_security
     *
     */
    public function testsetMaxAddressesTest()
    {
        parent::testsetMaxAddresses(array(
            [1, 0],
            [9, 64],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port_security
     *
     */
    public function testgetModeTest()
    {
        parent::testgetMode(array(
            [7, null]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port_security
     *
     */
    public function testsetModeTest()
    {
        parent::testsetMode(array(
            [7, null, null]
        ));
    }

}
