<?php

namespace tests\switches\zyxel\GS22108;


/**
 * @package tests\switches\zyxel\GS22108
 */
class PortTest extends \tests\switches\PortTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS22108']);
        $this->data   = $this->devices['zyxel\GS22108'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     *
     */
    public function testSetAndGetPortNumberTest()
    {
        parent::testSetAndGetPortNumber(1, true);
    }

    /**
     * @group switches\zyxel\GS22108\port
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Wrong port specified
     */
    public function testSetPortNumberExceptionTest()
    {
        parent::testSetPortNumberException();
    }

    /**
     * @group switches\zyxel\GS22108\port
     */
    public function testGetDataFlowTest()
    {
        parent::testGetDataFlow();
    }

    /**
     * @group switches\zyxel\GS22108\port
     */
    public function testGetErrorsTest()
    {
        parent::testGetErrors();
    }

    /**
     * @group switches\zyxel\GS22108\port
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testPortNumberNotSetErrorTest()
    {
        parent::testPortNumberNotSetError();
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetDescriptionTest()
    {
        parent::testGetDescription(array(
            [1, ''],
            [10, '[mon] [stp]']
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testCountTest()
    {
        parent::testCount(10);
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetLinkTest()
    {
        parent::testGetLink(array(
            [1, 0], // 0: link down
            [2, 1], // 1: half-10Mbps
            [3, 2], // 2: full-10Mbps
            [4, 3], // 3: half-100Mbps
            [5, 4], // 4: full-100Mbps
            [10, 6] // 6: full-1Gbps
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testSetDescriptionTest()
    {
        parent::testSetDescription(array(
            [1, ''],
            [10, '[mon] [stp]'],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetOperatingSpeedTest()
    {
        parent::testGetOperatingSpeed(array(
            [1, 0],             // link down no speed
            [2, 10000000],      // 10Mmbs link
            [4, 100000000],     // 100Mbs link
            [10, 1000000000],   // 1Gbs link
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetOperatingStatusTest()
    {
        parent::testGetOperatingStatus(array(
            [1, 2], // Link down
            [2, 1]  // Link up
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testConnectedIsComboTest()
    {
        parent::testConnectedIsCombo(array(
            [1, false],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testCompileIsComboTest()
    {
        parent::testCompileIsCombo(array(
            [1, false],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetMediumTypeTest()
    {
        parent::testGetMediumType(array(
            [1, 0],
            [10, 0],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testCompileGetMediumTypeTest()
    {
        parent::testCompileGetMediumType(array(
            [1, 0],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetAdminStateTest()
    {
        parent::testGetAdminState(array(
            [7, 2],
            [9, 1]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testSetAdminStateTest()
    {
        parent::testSetAdminState(array(
            [7, 2],
            [9, 1]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testgetFlowControlStateTest()
    {
        parent::testgetFlowControlState(array(
            [1, 2],
            [7, 1]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testsetFlowControlStateTest()
    {
        parent::testsetFlowControlState(array(
            [1, 2],
            [7, 1],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testgetLearningStateTest()
    {
        parent::testgetLearningState(array(
            [1, 1],
            [9, 2],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testsetLearningStateTest()
    {
        parent::testsetLearningState(array(
            [1, 1, true],
            [9, 2, true],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testClearStatsOnPortTest()
    {
        parent::testClearStatsOnPort(array(
            [1]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetDuplexTest()
    {
        parent::testGetDuplex(array(
            [2, 2],
            [3, 3]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\port
     *
     */
    public function testGetPortFromFdbTest()
    {
        parent::testGetPortFromFdb(array(
            [999, '00:0b:82:39:3f:c4' , 8],
        ));
    }


}
