<?php

namespace tests\switches\zyxel\GS22108;


/**
 * @package tests\switches\zyxel\GS22108
 */
class CableDiagnosticsTest extends \tests\switches\CableDiagnosticsTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS22108']);
        $this->data   = $this->devices['zyxel\GS22108'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS22108\cable_diagnostics
     *
     */
    public function testInitTest()
    {
        parent::testInit(array(
            [1, true],
            [5, true],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\cable_diagnostics
     *
     */
    public function testGetStateTest()
    {
        // Test array
        $no_link            = [1 => '1', 2 => '1', 3 => '1', 4 => '1'];
        $link_100           = [1 => '0', 2 => '0', 3 => '0', 4 => '0'];

        parent::testGetState(array(
            [1, $no_link],
            [5, $link_100],
        ));
    }

    /**
     * @group switches\zyxel\GS22108\cable_diagnostics
     *
     */
    public function testGetLengthTest()
    {
        parent::testGetLength(array(
            1,
            5,
        ));
    }


}
