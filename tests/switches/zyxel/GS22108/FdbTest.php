<?php

namespace tests\switches\zyxel\GS22108;


/**
 * @package tests\switches\zyxel\GS22108
 */
class FdbTest extends \tests\switches\FdbTest
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->ip     = key($this->devices['zyxel\GS22108']);
        $this->data   = $this->devices['zyxel\GS22108'][$this->ip];
        $this->vendor = 'zyxel';

    }

    /**
     * @group switches\zyxel\GS22108\fdb
     */
    public function testShowAllPermanentTest()
    {
        parent::testShowAllPermanent();
    }

    /**
     * @group switches\zyxel\GS22108\fdb
     *
     */
    public function testShowPortPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc4', 1 => '999' ]];

        parent::testShowPortPermanent(array(
            [8, $testarr]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\fdb
     *
     */
    public function testshowPortAllTest()
    {
        // Test array
        $testarr = [0 => [ 'vid' => '999', 'vlan' => 'Metro', 'state' => 'Static', 'mac' => '000b82393fc4' ]];

        parent::testShowPortAll(array(
            [8, $testarr, 'admin', '1234']
        ));
    }

    /**
     * @group switches\zyxel\GS22108\fdb
     *
     */
    public function testAddPermanentTest()
    {
        // Test array
        $testarr = [0 => [ 0 => '000b82393fc2', 1 => '999' ]];

        parent::testAddPermanent(array(
            [7, '00:0b:82:39:3f:c2', 999, $testarr]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\fdb
     *
     */
    public function testDeletePermanentTest()
    {
        parent::testDeletePermanent(array(
            [7, '00-0b-82-39-3f-c2', 999]
        ));
    }

    /**
     * @group switches\zyxel\GS22108\fdb
     *
     */
    public function testDeleteAllPermanentTest()
    {
        parent::testDeleteAllPermanent(array(
            [7, '00:0b:82:39:3b:c1', 999],
            [7, '00:0b:82:39:3b:c2', 999]

        ));
    }

}
