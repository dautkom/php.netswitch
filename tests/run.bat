@echo off

title PHP.NetSwitch Unit Tests
cd %~dp0

SET phpunit=..\vendor\bin\phpunit

:home
cls
echo.
echo  -------------------------------------------
echo ^|  Command line: %phpunit%      ^|
echo ^|  Select a task:                           ^|
echo ^|-------------------------------------------^|
echo ^| Q) Exit                                   ^|
echo ^| L) List groups                            ^|
echo ^|-------------------------------------------^|
echo ^| 1) Test dry-run compilation               ^|
echo ^| 2) Run connection tests                   ^|
echo ^| 3) Run all tests                          ^|
echo ^|                                           ^|
echo ^| 4) Test Cisco Catalyst2960                ^|
echo ^|                                           ^|
echo ^| 5) Test D-Link DES-3018                   ^|
echo ^| 6) Test D-Link DES-3026                   ^|
echo ^| 7) Test D-Link DES-3526                   ^|
echo ^| 8) Test D-Link DES-3526                   ^|
echo ^| 9) Test D-Link DES-320018 B1              ^|
echo ^| 10) Test D-Link DES-320018 C1             ^|
echo ^| 11) Test D-Link DES-320028                ^|
echo ^| 12) Test D-Link DES-320028 B1             ^|
echo ^| 13) Test D-Link DES-320028 C1             ^|
echo ^| 14) Test D-Link DES-320028F               ^|
echo ^| 15) Test D-Link DES-320028F B1            ^|
echo ^| 16) Test D-Link DES-320028F C1            ^|
echo ^|                                           ^|
echo ^| 17) Test D-Link DGS-312024SC A2           ^|
echo ^| 18) Test D-Link DGS-312024SC B1           ^|
echo ^| 19) Test D-Link DGS-1210-20/ME B1         ^|
echo ^| 20) Test D-Link DGS-1210-28/ME B1         ^|
echo ^| 21) Test D-Link DGS-1210-52/ME B1         ^|
echo ^| 22) Test D-Link DGS-3000-28/XS            ^|
echo ^|                                           ^|
echo ^| 23) Test Zyxel GS-2210-8                  ^|
echo ^| 24) Test Zyxel GS-2210-24                 ^|
echo ^| 25) Test Zyxel GS-2210-48                 ^|
echo ^| 26) Test Zyxel MES-3500-24F               ^|
echo ^| 27) Test Zyxel MGS-3520-28F               ^|
echo ^| 28) Test Zyxel XGS-2210-28                ^|
echo  -------------------------------------------
echo.
set /p opt=Type option:
if "%opt%"=="q" exit /B
if "%opt%"=="Q" exit /B
if "%opt%"=="l" goto list
if "%opt%"=="L" goto list
if "%opt%"=="1" goto compile
if "%opt%"=="2" goto connect
if "%opt%"=="3" goto runall
if "%opt%"=="4" goto Catalyst2960

if "%opt%"=="5" goto DES3018
if "%opt%"=="6" goto DES3026
if "%opt%"=="7" goto DES3526
if "%opt%"=="8" goto DES3550
if "%opt%"=="9" goto DES320018_B1
if "%opt%"=="10" goto DES320018_C1
if "%opt%"=="11" goto DES320028
if "%opt%"=="12" goto DES320028_B1
if "%opt%"=="13" goto DES320028_C1
if "%opt%"=="14" goto DES320028F
if "%opt%"=="15" goto DES320028F_B1
if "%opt%"=="16" goto DES320028F_C1

if "%opt%"=="17" goto DGS312024SC_A2
if "%opt%"=="18" goto DGS312024SC_B1
if "%opt%"=="19" goto DGS121020ME_B1
if "%opt%"=="20" goto DGS121028ME_B1
if "%opt%"=="21" goto DGS121052ME_B1
if "%opt%"=="22" goto DGS300028XS_B1

if "%opt%"=="23" goto GS22108
if "%opt%"=="24" goto GS221024
if "%opt%"=="25" goto GS221048
if "%opt%"=="26" goto MES350024F
if "%opt%"=="27" goto MGS352028F
if "%opt%"=="28" goto XGS221028

goto home

:list
%phpunit% --list-groups .
Pause
goto home

:runall
%phpunit% ./
Pause
goto home

:compile
%phpunit% --group="compilation" ./
Pause
goto home

:connect
%phpunit% --group="connection" ./
Pause
goto home

:Catalyst2960
%phpunit% --group="switches\cisco\Catalyst2960" ./
Pause
goto home

:DES3018
%phpunit% --group="switches\dlink\DES3018\common,switches\dlink\DES3018\port,switches\dlink\DES3018\port_security,switches\dlink\DES3018\bandwidth,switches\dlink\DES3018\qos,switches\dlink\DES3018\cable_diagnostics,switches\dlink\DES3018\fdb,switches\dlink\DES3018\ipmb,switches\dlink\DES3018\vlan,switches\dlink\DES3018\stp" ./
Pause
goto home

:DES3026
%phpunit% --group="switches\dlink\DES3026\common,switches\dlink\DES3026\port,switches\dlink\DES3026\port_security,switches\dlink\DES3026\bandwidth,switches\dlink\DES3026\qos,switches\dlink\DES3026\cable_diagnostics,switches\dlink\DES3026\fdb,switches\dlink\DES3026\ipmb,switches\dlink\DES3026\vlan,switches\dlink\DES3026\stp" ./
Pause
goto home

:DES3526
%phpunit% --group="switches\dlink\DES3526\common,switches\dlink\DES3526\port,switches\dlink\DES3526\port_security,switches\dlink\DES3526\bandwidth,switches\dlink\DES3526\qos,switches\dlink\DES3526\cable_diagnostics,switches\dlink\DES3526\fdb,switches\dlink\DES3526\ipmb,switches\dlink\DES3526\vlan,switches\dlink\DES3526\stp" ./
Pause
goto home

:DES3550
%phpunit% --group="switches\dlink\DES3550\common,switches\dlink\DES3550\port,switches\dlink\DES3550\port_security,switches\dlink\DES3550\bandwidth,switches\dlink\DES3550\qos,switches\dlink\DES3550\cable_diagnostics,switches\dlink\DES3550\fdb,switches\dlink\DES3550\ipmb,switches\dlink\DES3550\vlan,switches\dlink\DES3550\stp" ./
Pause
goto home

:DES320018_B1
%phpunit% --group="switches\dlink\DES320018\common,switches\dlink\DES320018\port,switches\dlink\DES320018\port_security,switches\dlink\DES320018\bandwidth,switches\dlink\DES320018\qos,switches\dlink\DES320018\cable_diagnostics,switches\dlink\DES320018\fdb,switches\dlink\DES320018\ipmb,switches\dlink\DES320018\vlan,switches\dlink\DES320018\stp" ./
Pause
goto home

:DES320018_C1
%phpunit% --group="switches\dlink\DES320018\C1\common,switches\dlink\DES320018\C1\port,switches\dlink\DES320018\C1\port_security,switches\dlink\DES320018\C1\bandwidth,switches\dlink\DES320018\C1\qos,switches\dlink\DES320018\C1\cable_diagnostics,switches\dlink\DES320018\C1\fdb,switches\dlink\DES320018\C1\ipmb,switches\dlink\DES320018\C1\vlan,switches\dlink\DES320018\C1\stp" ./
Pause
goto home

:DES320028
%phpunit% --group="switches\dlink\DES320028\common,switches\dlink\DES320028\port,switches\dlink\DES320028\port_security,switches\dlink\DES320028\bandwidth,switches\dlink\DES320028\qos,switches\dlink\DES320028\cable_diagnostics,switches\dlink\DES320028\fdb,switches\dlink\DES320028\ipmb,switches\dlink\DES320028\vlan,switches\dlink\DES320028\stp" ./
Pause
goto home

:DES320028_B1
%phpunit% --group="switches\dlink\DES320028\B1\common,switches\dlink\DES320028\B1\port,switches\dlink\DES320028\B1\port_security,switches\dlink\DES320028\B1\bandwidth,switches\dlink\DES320028\B1\qos,switches\dlink\DES320028\B1\cable_diagnostics,switches\dlink\DES320028\B1\fdb,switches\dlink\DES320028\B1\ipmb,switches\dlink\DES320028\B1\vlan,switches\dlink\DES320028\B1\stp" ./
Pause
goto home

:DES320028_C1
%phpunit% --group="switches\dlink\DES320028\C1\common,switches\dlink\DES320028\C1\port,switches\dlink\DES320028\C1\port_security,switches\dlink\DES320028\C1\bandwidth,switches\dlink\DES320028\C1\qos,switches\dlink\DES320028\C1\cable_diagnostics,switches\dlink\DES320028\C1\fdb,switches\dlink\DES320028\C1\ipmb,switches\dlink\DES320028\C1\vlan,switches\dlink\DES320028\C1\stp" ./
Pause
goto home

:DES320028F
%phpunit% --group="switches\dlink\DES320028F\common,switches\dlink\DES320028F\port,switches\dlink\DES320028F\port_security,switches\dlink\DES320028F\bandwidth,switches\dlink\DES320028F\qos,switches\dlink\DES320028F\cable_diagnostics,switches\dlink\DES320028F\fdb,switches\dlink\DES320028F\ipmb,switches\dlink\DES320028F\vlan,switches\dlink\DES320028F\stp" ./
Pause
goto home

:DES320028F_C1
%phpunit% --group="switches\dlink\DES320028F\C1\common,switches\dlink\DES320028F\C1\port,switches\dlink\DES320028F\C1\port_security,switches\dlink\DES320028F\C1\bandwidth,switches\dlink\DES320028F\C1\qos,switches\dlink\DES320028F\C1\cable_diagnostics,switches\dlink\DES320028F\C1\fdb,switches\dlink\DES320028F\C1\ipmb,switches\dlink\DES320028F\C1\vlan,switches\dlink\DES320028F\C1\stp" ./
Pause
goto home

:DGS312024SC_A2
%phpunit% --group="switches\dlink\DGS312024SC\common,switches\dlink\DGS312024SC\port,switches\dlink\DGS312024SC\port_security,switches\dlink\DGS312024SC\bandwidth,switches\dlink\DGS312024SC\qos,switches\dlink\DGS312024SC\cable_diagnostics,switches\dlink\DGS312024SC\fdb,switches\dlink\DGS312024SC\ipmb,switches\dlink\DGS312024SC\vlan,switches\dlink\DGS312024SC\stp" ./
Pause
goto home

:DGS312024SC_B1
%phpunit% --group="switches\dlink\DGS312024SC\B1\common,switches\dlink\DGS312024SC\B1\port,switches\dlink\DGS312024SC\B1\port_security,switches\dlink\DGS312024SC\B1\bandwidth,switches\dlink\DGS312024SC\B1\qos,switches\dlink\DGS312024SC\B1\cable_diagnostics,switches\dlink\DGS312024SC\B1\fdb,switches\dlink\DGS312024SC\B1\ipmb,switches\dlink\DGS312024SC\B1\vlan,switches\dlink\DGS312024SC\B1\stp" ./
Pause
goto home

:DGS121020ME_B1
%phpunit% --testdox --group="switches\dlink\DGS121020ME\B1\common,switches\dlink\DGS121020ME\B1\port,switches\dlink\DGS121020ME\B1\port_security,switches\dlink\DGS121020ME\B1\bandwidth,switches\dlink\DGS121020ME\B1\qos,switches\dlink\DGS121020ME\B1\cable_diagnostics,switches\dlink\DGS121020ME\B1\fdb,switches\dlink\DGS121020ME\B1\ipmb,switches\dlink\DGS121020ME\B1\vlan,switches\dlink\DGS121020ME\B1\stp" ./
Pause
goto home

:DGS121028ME_B1
%phpunit% --testdox --group="switches\dlink\DGS121028ME\B1\common,switches\dlink\DGS121028ME\B1\port,switches\dlink\DGS121028ME\B1\port_security,switches\dlink\DGS121028ME\B1\bandwidth,switches\dlink\DGS121028ME\B1\qos,switches\dlink\DGS121028ME\B1\cable_diagnostics,switches\dlink\DGS121028ME\B1\fdb,switches\dlink\DGS121028ME\B1\ipmb,switches\dlink\DGS121028ME\B1\vlan,switches\dlink\DGS121028ME\B1\stp" ./
Pause
goto home

:DGS121052ME_B1
%phpunit% --testdox --group="switches\dlink\DGS121052ME\B1\common,switches\dlink\DGS121052ME\B1\port,switches\dlink\DGS121052ME\B1\port_security,switches\dlink\DGS121052ME\B1\bandwidth,switches\dlink\DGS121052ME\B1\qos,switches\dlink\DGS121052ME\B1\cable_diagnostics,switches\dlink\DGS121052ME\B1\fdb,switches\dlink\DGS121052ME\B1\ipmb,switches\dlink\DGS121052ME\B1\vlan,switches\dlink\DGS121052ME\B1\stp" ./
Pause
goto home

:DGS300028XS_B1
%phpunit% --testdox --group="switches\dlink\DGS300028XS\B1\common,switches\dlink\DGS300028XS\B1\port,switches\dlink\DGS300028XS\B1\port_security,switches\dlink\DGS300028XS\B1\bandwidth,switches\dlink\DGS300028XS\B1\qos,switches\dlink\DGS300028XS\B1\cable_diagnostics,switches\dlink\DGS300028XS\B1\fdb,switches\dlink\DGS300028XS\B1\ipmb,switches\dlink\DGS300028XS\B1\vlan,switches\dlink\DGS300028XS\B1\stp" ./
Pause
goto home

:GS22108
%phpunit% --group="switches\zyxel\GS22108\common,switches\zyxel\GS22108\port,switches\zyxel\GS22108\port_security,switches\zyxel\GS22108\bandwidth,switches\zyxel\GS22108\fdb,switches\zyxel\GS22108\qos,switches\zyxel\GS22108\vlan,switches\zyxel\GS22108\cable_diagnostics,switches\zyxel\GS22108\stp" ./
Pause
goto home

:GS221024
%phpunit% --group="switches\zyxel\GS221024\common,switches\zyxel\GS221024\port,switches\zyxel\GS221024\port_security,switches\zyxel\GS221024\bandwidth,switches\zyxel\GS221024\fdb,switches\zyxel\GS221024\qos,switches\zyxel\GS221024\vlan,switches\zyxel\GS221024\cable_diagnostics,switches\zyxel\GS221024\stp" ./
Pause
goto home

:GS221048
%phpunit% --group="switches\zyxel\GS221048\common,switches\zyxel\GS221048\port,switches\zyxel\GS221048\port_security,switches\zyxel\GS221048\bandwidth,switches\zyxel\GS221048\fdb,switches\zyxel\GS221048\qos,switches\zyxel\GS221048\vlan,switches\zyxel\GS221048\cable_diagnostics,switches\zyxel\GS221048\stp" ./
Pause
goto home

:MES350024F
%phpunit% --group="switches\zyxel\MES350024F\common,switches\zyxel\MES350024F\port,switches\zyxel\MES350024F\port_security,switches\zyxel\MES350024F\bandwidth,switches\zyxel\MES350024F\fdb,switches\zyxel\MES350024F\qos,switches\zyxel\MES350024F\vlan,switches\zyxel\MES350024F\cable_diagnostics,switches\zyxel\MES350024F\stp" ./
Pause
goto home

:MGS352028F
%phpunit% --group="switches\zyxel\MGS352028F\common,switches\zyxel\MGS352028F\port,switches\zyxel\MGS352028F\port_security,switches\zyxel\MGS352028F\bandwidth,switches\zyxel\MGS352028F\fdb,switches\zyxel\MGS352028F\qos,switches\zyxel\MGS352028F\vlan,switches\zyxel\MGS352028F\cable_diagnostics,switches\zyxel\MGS352028F\stp" ./
Pause
goto home

:XGS221028
%phpunit% --group="switches\zyxel\XGS221028\common,switches\zyxel\XGS221028\port,switches\zyxel\XGS221028\port_security,switches\zyxel\XGS221028\bandwidth,switches\zyxel\XGS221028\fdb,switches\zyxel\XGS221028\qos,switches\zyxel\XGS221028\vlan,switches\zyxel\XGS221028\cable_diagnostics,switches\zyxel\XGS221028\stp" ./
Pause
goto home
