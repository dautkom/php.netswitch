<?php

namespace dautkom\netswitch;

use dautkom\netsnmp;
use dautkom\netswitch\library\Fdb;
use dautkom\netswitch\library\Vlan;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use TypeError;

/**
 * Interface iConnect for warning mitigation:
 * Strict Standards: Declaration of Telnet::connect() should be compatible with that of NetSwitch::connect()
 *
 * @package dautkom\netswitch
 */
interface iConnect
{
    /**
     * @param  string $arg1 IP-address
     * @param  mixed  $arg2 telnet port or SNMP settings array
     * @param  mixed  $arg3 telnet settings array
     * @return object
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function connect($arg1, $arg2, $arg3);
}


/**
 * @package dautkom\netswitch
 */
class NetSwitch implements iConnect
{

    /**
     * Should E_USER_NOTICE be triggered if method
     * is not implemented in particular device model
     */
    const NOT_IMPLEMENTED_NOTICE = false;

    /**
     * Device information from ini file.
     * Public access due to trait overriding
     * @var array
     */
    public static $device = [];

    /**
     * Port number set via $obj->Port->setPortNumber.
     * Public access due to trait overriding
     * @var int
     */
    public static $port;

    /**
     * Saved object instance
     * @var Vlan
     */
    public static $_vlan;

    /**
     * Saved object instance
     * @var Fdb
     */
    public static $_fdb;

    /**
     * Default telnet settings as array[login, password]
     * @var array
     */
    protected static $telnet = ['admin', '1234'];

    /**
     * Device IP-address
     * @var string
     */
    protected static $ip;

    /**
     * SNMP instance
     * @var netsnmp\NetSNMP
     */
    protected static $snmp;

    /**
     * Device ID, similar to namespace
     * @var string
     */
    protected static $model_id;

    /**
     * Connection status flag
     * @var bool
     */
    protected static $connected = false;

    /**
     * Vendor name and sysObjectID match
     * @var array
     */
    private static $vendors = [
        // Cisco
        9   => [
            '1.716'      => 'cisco\\Catalyst2960',
        ],
        // D-Link
        171 => [
            '10.63.2'    => 'dlink\\DES3018',
            '10.63.3'    => 'dlink\\DES3026',
            '10.64.1'    => 'dlink\\DES3526',
            '10.64.2'    => 'dlink\\DES3550',
            '10.113.1.2' => 'dlink\\DES320018',
            '10.113.3.1' => 'dlink\\DES320018\\C1',
            '10.113.1.5' => 'dlink\\DES320026',
            '10.113.1.3' => [
                'dlink\\DES320028'     => ['hw' => 'A1'],
                'dlink\\DES320028\\B1' => ['hw' => 'B1'],
            ],
            '10.113.5.1'  => 'dlink\\DES320028\\C1',
            '10.113.9.1'  => 'dlink\\DES320052\\C1',
            '10.113.1.4'  => 'dlink\\DES320028F',
            '10.113.6.1'  => 'dlink\\DES320028F\\C1',
            '10.117.1.3'  => 'dlink\\DGS312024SC',
            '10.117.4.1'  => 'dlink\\DGS312024SC\\B1',
            '10.76.31.2'  => 'dlink\\DGS121020ME\\B1',
            '10.76.28.2'  => 'dlink\\DGS121028ME\\B1',
            '10.76.29.2'  => 'dlink\\DGS121052ME\\B1',
            '10.133.12.2' => 'dlink\\DGS300028XS\\B1',
        ],
        // Zyxel
        890 => [
            '1.15'       => [
                'zyxel\\GS221024'   => ['descr' => 'GS2210-24'],
                'zyxel\\GS221048'   => ['descr' => 'GS2210-48'],
                'zyxel\\GS22108'    => ['descr' => 'GS2210-8'],
                'zyxel\\MGS352028'  => ['descr' => 'MGS3520-28'],
                'zyxel\\MGS352028F' => ['descr' => 'MGS3520-28F'],
                'zyxel\\XGS221028'  => ['descr' => 'XGS2210-28'],
            ],
            '1.5.8.57'   => 'zyxel\\MES350024F'
        ],
    ];

    /**
     * SNMP attributes
     * Add any attribute and use any combination (or all scope) to determine device model namespace by given OID
     * @var array
     */
    private static $snmpAttributes = [
        // Cisco
        9 => [],
        // D-Link
        171 => [
            'hw'    => '.1.3.6.1.2.1.16.19.3.0',
            'descr' => '.1.3.6.1.2.1.1.1.0',
        ],
        // Zyxel
        890 => [
            'descr' => '.1.3.6.1.2.1.1.1.0',
        ],
    ];


    /**
     * Get all supported models and revisions
     *
     * @return array
     */
    public function getSupportedModels(): array
    {

        $dir     = __DIR__ . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR;
        $rdi     = new RecursiveDirectoryIterator( $dir, FilesystemIterator::SKIP_DOTS );
        $it      = new RecursiveIteratorIterator( $rdi, RecursiveIteratorIterator::SELF_FIRST );
        $result  = [];

        foreach ( $it as $splFileInfo ) {

            $path = [];

            for ( $depth = $it->getDepth(); $depth >= 0; $depth-- ) {

                if( $splFileInfo->isDir() ) {
                    $path = [$it->getSubIterator($depth)->current()->getFileName() => $path];
                    if( $depth == 2 ) {
                        $path = $splFileInfo->getFileName();
                    }
                }

            }

            $result = array_merge_recursive($result, $path);

        }

        return $result;

    }


    /**
     * Dry-run factory method
     *
     * @param  string $model
     * @return mixed
     */
    public function compile(string $model)
    {
        $class          = "dautkom\\netswitch\\library\\$model\\Common";
        self::$model_id = $model;
        return new $class;
    }


    /**
     * Factory method
     *
     *
     * @param  string $ip     IP-address
     * @param  array  $snmp   SNMP settings
     * @param  array  $telnet Telnet credentials
     * @return mixed
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function connect($ip, $snmp=[], $telnet=[])
    {

        self::$ip     = trim($ip);
        self::$telnet = array_replace(self::$telnet, $telnet);

        if( !filter_var(self::$ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            throw new RuntimeException( 'Wrong IP-address specified' );
        }

        // Instantiate SNMP engine and get sysObjectID
        self::$snmp = (new netsnmp\NetSNMP())->init($ip, $snmp);

        // Error suppression is causing TypeError in prepareResult() method
        try {
            $device     = @self::$snmp->get(".1.3.6.1.2.1.1.2.0");
        }
        catch (TypeError $e) {
            $device     = false;
        }

        if( !$device ) {
            throw new RuntimeException("Unable to connect to device $ip. Check if device is offline or has misconfigured SNMP settings.");
        }

        // Split sysObjectID to vendor id and object
        $device          = preg_split('/\./', $device, 2);
        self::$connected = true;

        // Check if device is supported by PHP.NetSwitch
        if( !array_key_exists($device[0], self::$vendors) ) {
            throw new RuntimeException("Device vendor for sysObjectID.".implode('.', $device)." is not supported by PHP.NetSwitch");
        }
        elseif( !array_key_exists($device[1], self::$vendors[$device[0]]) ) {
            throw new RuntimeException("Device with ID $device[1] is not supported by PHP.NetSwitch, while vendor is found for sysObjectID.$device[0]");
        }
        else {
            if( !is_array(self::$vendors[$device[0]][$device[1]]) ) {
                // Default behavior, when vendor object id points to namespace
                $device = self::$vendors[$device[0]][$device[1]];
            }
            else {
                // If there's an array in particular vendor configuration
                // try to find out required namespace by snmp attributes
                $device = $this->getNamespaceByAttributes($device[0], $device[1]);
            }
        }

        // Get full class name for device
        $class = "dautkom\\netswitch\\library\\$device\\Common";
        self::$model_id = $device;

        return new $class;

    }


    /**
     * Retrieves array parsed from ini file
     *
     * @return array
     */
    public function getIni(): array
    {
        return self::$device;
    }


    /**
     * Wrapper for snmpset(). Set the value of an SNMP object. Accepts both strings and arrays as arguments
     *
     * @param  array|string $oid   SNMP object identifier
     * @param  array|string $type  content type literal
     * @param  array|string $value writable data
     * @return bool|null
     */
    public function set($oid, $type, $value): bool
    {

        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
        }

        return self::$snmp->set($oid, $type, $value);

    }


    /**
     * Wrapper SNMP::get() to a self::$port
     * This method is a wrapper for port-specific queries.
     *
     * @param  string|array $oid SNMP object identifier
     * @return mixed
     */
    public function getFromPort( $oid )
    {

        if( !self::$port ) {
            trigger_error("Port number is not set", E_USER_WARNING);
            return null;
        }

        return self::$snmp->get( $this->addPortNumberToOID($oid) );

    }


    /**
     * Wrapper for snmpget(). Fetch an SNMP object. Accepts both strings and arrays as OIDs.
     * When $oid is an array and keys in results will be taken exactly as in object_id.
     *
     * @param  array|string $oid SNMP object identifier
     * @return mixed
     */
    protected function get($oid)
    {

        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
        }

        // Error suppression is causing TypeError in prepareResult() method
        try {
            $snmp_get = self::$snmp->get($oid);
        }
        catch (TypeError $e) {
            $snmp_get = false;
        }

        return $snmp_get;

    }


    /**
     * Wrapper for snmpwalk() and snmprealwalk(). Fetch SNMP object subtree
     *
     * @param  string $oid  SNMP object identifier representing root of subtree to be fetched
     * @param  bool   $real [optional] By default full OID notation is used for keys in output array. If set to <b>TRUE</b> subtree prefix will be removed from keys leaving only suffix of object_id.
     * @return array
     */
    protected function walk(string $oid, bool $real = false): array
    {

        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
        }

        // Error suppression is causing TypeError in prepareResult() method
        try {
            $snmp_walk = self::$snmp->walk($oid, $real);
        }
        catch (TypeError $e) {
            $snmp_walk = [];
        }

        return $snmp_walk;

    }


    /**
     * Wrapper for snmpset(). Set the value of an SNMP object. Accepts both strings and arrays as arguments
     *
     * @param  array|string $oid   SNMP object identifier
     * @param  array|string $type  content type literal
     * @param  array|string $value writable data
     * @return bool
     */
    protected function setToPort($oid, $type, $value): bool
    {

        if( !self::$port ) {
            trigger_error("Port number is not set", E_USER_WARNING);
        }

        return self::$snmp->set( $this->addPortNumberToOID($oid), $type, $value );

    }


    /**
     * Wrapper for methods not supported by device model
     * Replaces "OID is missing"
     *
     * @return null
     */
    protected function notImplemented()
    {

        if ( self::NOT_IMPLEMENTED_NOTICE ) {
            $invoker = debug_backtrace();
            $invoker = $invoker[1]['function'];
            trigger_error("Method $invoker() is not implemented for " . self::$model_id, E_USER_NOTICE);
        }

        return null;

    }


    /**
     * Load ini file with device configuration
     *
     * @throws RuntimeException
     * @throws ReflectionException
     * @return void
     */
    protected function loadIni()
    {

        // Who called me?
        $invoker = new ReflectionClass($this);
        $invoker = $invoker->getNamespaceName();

        // Create path to ini file
        $ini = __DIR__ . str_replace([ __NAMESPACE__, '\\'], ['', DIRECTORY_SEPARATOR], $invoker) . DIRECTORY_SEPARATOR . 'model.ini';

        if( !file_exists( $ini ) ) {
            throw new RuntimeException( "$ini not found" );
        }

        self::$device = $this->parseIni($ini);

    }


    /**
     * Adds port number to OID
     *
     * @param  array|string $oid SNMP object identifier
     * @return array|string
     */
    protected function addPortNumberToOID($oid)
    {

        if( is_array($oid) ) {
            $oid = array_map(function( $value ){
                return trim($value) .'.'.self::$port;
            }, $oid);
        }
        else {
            $oid = trim($oid) .'.'.self::$port;
        }

        return $oid;

    }


    /**
     * Parse and validate ini file
     *
     * @param   $ini string path to ini file
     * @return  array
     * @throws RuntimeException
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    private function parseIni($ini): array
    {

        $ini = parse_ini_file( $ini, true );

        if( empty($ini) ) {
            throw new RuntimeException('Error while loading '.self::$model_id.' ini-file, probably syntax error found');
        }

        if( !array_key_exists('ports', $ini) || empty($ini['ports']) ) {
            throw new RuntimeException('No ports defined in '.self::$model_id.' ini-file');
        }

        return $ini;

    }


    /**
     * Namespace detection by snmp attributes
     *
     * @throws RuntimeException
     * @param  string $vendor
     * @param  string $id
     * @return string
     */
    private function getNamespaceByAttributes( string $vendor, string $id ): string
    {

        // Obtain the necessary initialization parameters
        $options = array_keys( reset( self::$vendors[$vendor][$id] ) );
        $find    = array_flip( $options );  // values <--> keys

        array_walk( $find, function( &$value, $index, $vendor ) {

            if( !isset( self::$snmpAttributes[$vendor][$index] ) ) {
                self::$connected = false;
                throw new RuntimeException("Unable to connect to device ".self::$ip.". Unknown attribute - $index.");
            }
            // Get attribute value
            $value = self::$snmp->get( self::$snmpAttributes[$vendor][$index] );

            if( !$value ) {
                self::$connected = false;
                throw new RuntimeException("Unable to connect to device ".self::$ip.". Check if device is offline or has misconfigured SNMP settings.");
            }
        }
        , $vendor );

        $result = array_search( $find, self::$vendors[$vendor][$id] );

        // Check if device is supported by PHP.NetSwitch
        if( !$result ) {
            self::$connected = false;
            throw new RuntimeException("Device sysObjectID.$vendor.$id is not supported by PHP.NetSwitch");
        }

        return $result;

    }

}
